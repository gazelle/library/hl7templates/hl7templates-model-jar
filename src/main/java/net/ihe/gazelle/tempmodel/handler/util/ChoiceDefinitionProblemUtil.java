package net.ihe.gazelle.tempmodel.handler.util;

import java.util.List;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionProblemUtil;

/**
 *
 * @author Abderrazek Boufahja
 *
 */
public final class ChoiceDefinitionProblemUtil {
	
	private ChoiceDefinitionProblemUtil() {}
	
	public static boolean choiceContainsValidsubElements(ChoiceDefinition choiceDefinition) {
		if (choiceDefinition != null) {
			List<RuleDefinition> lrd = ChoiceDefinitionUtil.getElements(choiceDefinition);
			for (RuleDefinition ruleDefinition : lrd) {
				if (!RuleDefinitionProblemUtil.ruleDefinitionHasKnownType(ruleDefinition)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

}
