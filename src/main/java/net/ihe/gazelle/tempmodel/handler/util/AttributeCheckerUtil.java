package net.ihe.gazelle.tempmodel.handler.util;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.goc.uml.utils.UMLPrimitiveType;
import net.ihe.gazelle.tempmodel.decor.dt.model.AttDatatype;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AttributeUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 * the class has not private constructor because it is extended by a testing class
 *
 */
public class AttributeCheckerUtil {
	
	public static boolean isUseValueAcceptableForAttribute(Attribute selectedAttribute, String use) {
		if (selectedAttribute == null) {
			return false;
		}
		if (selectedAttribute.getParentObject() instanceof RuleDefinition) {
			RuleDefinition rd = (RuleDefinition)selectedAttribute.getParentObject();
			String type = RuleDefinitionUtil.getConstrainedUMLTypeName(rd);
			return isUseValueAcceptable(use, type);
		}
		return false;
	}

	protected static boolean isUseValueAcceptable(String use, String type) {
		if (use == null || use.trim().equals("")) {
			return false;
		}
		if (type == null) {
			return false;
		}
		String[] listUse = use.split("\\s+");
		if (UMLLoader.isEnumeration(type + ".use")) {
			String typeUseAttr = UMLLoader.getPropertyTypeName(type + ".use");
			boolean ok = true;
			for (String string : listUse) {
				ok = ok && UMLLoader.checkIfEnumerationContainsValue(typeUseAttr, string);
			}
			return ok;
		}
		else {
			return true;
		}
	}
	
	/**
	 * pre : the attr.name is not null
	 * @param selectedAttribute Attribute
	 * @param dt AttDatatype
	 * @return boolean
	 */
	public static boolean verifyThatDatatypeIsSupported(Attribute selectedAttribute, AttDatatype dt) {
		String type = AttributeUtil.getUMLTypeName(selectedAttribute, selectedAttribute.getName());
		if (type.equals(UMLPrimitiveType.STRING.getValue())){
			return true;
		}
		else if (type.equals(UMLPrimitiveType.BOOLEAN.getValue())){
			return dt == AttDatatype.BL || dt == AttDatatype.BN;
		}
		else if (type.equals(UMLPrimitiveType.INTEGER.getValue())){
			return dt == AttDatatype.INT || dt == AttDatatype.TS;
		}
		else if (type.equals(UMLPrimitiveType.DOUBLE.getValue())){
			return dt == AttDatatype.INT || dt == AttDatatype.RUID || dt == AttDatatype.TS;
		}
		// the attribute is of type Enumeration
		return dt == AttDatatype.CS || dt == AttDatatype.ST;
	}

}
