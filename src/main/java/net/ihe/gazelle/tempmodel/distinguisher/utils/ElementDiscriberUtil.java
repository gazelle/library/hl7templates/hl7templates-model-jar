package net.ihe.gazelle.tempmodel.distinguisher.utils;

import net.ihe.gazelle.tempmodel.distinguisher.CDAElementsDistinguisher;
import net.ihe.gazelle.tempmodel.distinguisher.ElementDiscriber;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class ElementDiscriberUtil {
	
	private ElementDiscriberUtil() {}
	
	public static ElementDiscriber extractElementDiscriberById(String identifier, CDAElementsDistinguisher cdaDistinguisers){
		if (cdaDistinguisers == null || identifier == null){
			return null;
		}
		for (ElementDiscriber ed : cdaDistinguisers.getElementDiscriber()) {
			if (ed.getIdentifier() != null && ed.getIdentifier().equals(identifier)){
				return ed;
			}
		}
		return null;
	}

}
