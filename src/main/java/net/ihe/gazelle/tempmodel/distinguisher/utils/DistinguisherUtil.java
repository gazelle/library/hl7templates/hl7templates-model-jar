package net.ihe.gazelle.tempmodel.distinguisher.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.goc.uml.utils.PathWithDT;
import net.ihe.gazelle.goc.uml.utils.PathWithDTUtil;
import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.tempmodel.distinguisher.CDAElementsDistinguisher;
import net.ihe.gazelle.tempmodel.distinguisher.Distinguisher;
import net.ihe.gazelle.tempmodel.distinguisher.ElementDiscriber;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 * the class has not private constructor because it is extended by a testing class
 *
 */
public class DistinguisherUtil {
	
	
	private static Logger log = LoggerFactory.getLogger(DistinguisherUtil.class);
	
	private static CDAElementsDistinguisher cdaDistinguisers = null;
	
	static {
		try {
			String pathDistinguishers = System.getProperty("HL7TEMP_RESOURCES_PATH",
					"/home/aboufahj/workspaceTopcased/hl7templates-resources/") + File.separator +
					System.getProperty("HL7TEMP_CDACONFFOLDERNAME", "cdabasic") +
					"/distinguishers.xml";
			cdaDistinguisers = loadCDAElementsDistinguisher(new FileInputStream(pathDistinguishers));
		} catch (FileNotFoundException e) {
			log.error("distinguisher file not found", e);
		} catch (JAXBException e) {
			log.error("distinguisher file not well formatted", e);
		}
	}
	
	public static CDAElementsDistinguisher loadCDAElementsDistinguisher(InputStream is) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(CDAElementsDistinguisher.class);
		Unmarshaller u = jc.createUnmarshaller();
		return (CDAElementsDistinguisher) u.unmarshal(is);
	}
	
	public static ElementDiscriber findElementDiscriber(RuleDefinition ruleDefinition){
		if (ruleDefinition != null) {
			List<PathWithDT> lpwdt = RuleDefinitionUtil.getJavaPath(ruleDefinition);
			String javaPath = PathWithDTUtil.extractPathFromListPathWithDT(lpwdt);
			if (javaPath != null) {
				return findElementDiscriber(javaPath);
			}
		}
		return null;
	}
	
	protected static ElementDiscriber findElementDiscriber(String javaPath){
		String name = javaPath.split("\\.")[javaPath.split("\\.").length-1];
		String type = UMLLoader.getPropertyTypeName(javaPath);
		for (ElementDiscriber el : cdaDistinguisers.getElementDiscriber()) {
			boolean nameMatch = el.getName() != null && el.getName().equals(name);
			boolean typeMatch = el.getType() != null && el.getType().value().equals(type);
			if ((nameMatch && el.getType()==null) || (typeMatch && (el.getName()==null || nameMatch))){
				return el;
			}
		}
		return null;
	}
	
	public static List<Distinguisher> getRealDistinguisher(ElementDiscriber ed){
		if (ed == null) {
			return new ArrayList<>();
		}
		if (ed.getListRealDistinguisher() != null) {
			return ed.getListRealDistinguisher();
		}
		List<Distinguisher> aa = getRealDistinguisher(ed, 5);
		ed.setListRealDistinguisher(aa);
		return aa;
	}
	
	private static List<Distinguisher> getRealDistinguisher(ElementDiscriber ed, Integer level){
		if (ed == null || level == null) {
			return new ArrayList<>();
		}
		List<Distinguisher> res = new ArrayList<>();
		for (Distinguisher distinguisher : ed.getDistinguisher()) {
			if (distinguisher.getRef() == null){
				res.add(distinguisher);
			}
			else {
				if (level == 0) {
					continue;
				}
				ElementDiscriber edChild = ElementDiscriberUtil.extractElementDiscriberById(distinguisher.getRef(), cdaDistinguisers);
				if (edChild != null){
					List<Distinguisher> listSub = getRealDistinguisher(edChild, level - 1);
					for (Distinguisher distinguisher2 : listSub) {
						Distinguisher dist = new Distinguisher();
						dist.setXpath(distinguisher.getXpath() + distinguisher2.getXpath());
						res.add(dist);
					}
				}
				else {
					log.error("Error to process Distinguisher ref : " + distinguisher.getRef());
				}
			}
		}
		return res;
	}

}
