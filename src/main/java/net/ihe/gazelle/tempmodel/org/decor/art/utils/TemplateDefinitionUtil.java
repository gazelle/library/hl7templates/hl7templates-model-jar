package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.ihe.gazelle.tempmodel.gov.model.TemplateRepository;
import net.ihe.gazelle.tempmodel.gov.util.TemplateRepositoryUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefineVariable;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RelationshipTypes;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Report;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateProperties;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateRelationships;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateStatusCodeLifeCycle;
import net.ihe.gazelle.tempmodel.temp.mapping.utils.TemplatesTypeMappingUtil;
import net.ihe.gazelle.tempmodel.templates.util.TNoeudUtil;

/**
 * 
 * @author Abderrazek Boufahja
 * the class is not final because it is extended for unit tests
 *
 */
public class TemplateDefinitionUtil {
	
	private static final String ROOT = "root";

	public static List<RuleDefinition> getElements(TemplateDefinition templateDefinition) {
		List<RuleDefinition> templates = new ArrayList<>();
		if (templateDefinition != null){
			for(Object obj : templateDefinition.getAttributeOrChoiceOrElement()) {
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition) {
						templates.add( (RuleDefinition) obj );
				}
			} 
		}
		return templates;
	}
	
	public static List<ChoiceDefinition> getChoices(TemplateDefinition templateDefinition) {
		List<ChoiceDefinition> templates = new ArrayList<>();
		if (templateDefinition != null){
			for(Object obj : templateDefinition.getAttributeOrChoiceOrElement()) {
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition) {
						templates.add( (ChoiceDefinition) obj );
				}
			} 
		}
		return templates;
	}
	
	public static List<IncludeDefinition> getIncludes(TemplateDefinition templateDefinition) {
		List<IncludeDefinition> templates = new ArrayList<>();
		if (templateDefinition != null){
			for(Object obj : templateDefinition.getAttributeOrChoiceOrElement()) {
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition) {
						templates.add( (IncludeDefinition) obj );
				}
			} 
		}
		return templates;
	}
	
	public static RuleDefinition getFirstElement(TemplateDefinition templateDefinition){
		if (templateDefinition != null){
			for(Object obj : templateDefinition.getAttributeOrChoiceOrElement()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition){
						return (RuleDefinition) obj;
				}
			} 
		}
		return null;
	}
	
	public static List<Attribute> getAttributes(TemplateDefinition templateDefinition){
		List<Attribute> templates = new ArrayList<>();
		if (templateDefinition != null){
			for(Object obj : templateDefinition.getAttributeOrChoiceOrElement()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute){
						templates.add( (Attribute) obj );
				}
			} 
		}
		return templates;
	}
	
	public static List<Let> getLets(TemplateDefinition templateDefinition){
		List<Let> elements = new ArrayList<>();
		if (templateDefinition != null){
			for(Object obj : templateDefinition.getAttributeOrChoiceOrElement()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.Let){
					elements.add( (Let) obj );
				}
			} 
		}
		return elements;
	}
	
	public static List<Assert> getAsserts(TemplateDefinition templateDefinition){
		List<Assert> elements = new ArrayList<>();
		if (templateDefinition != null){
			for(Object obj : templateDefinition.getAttributeOrChoiceOrElement()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.Assert){
					elements.add( (Assert) obj );
				}
			} 
		}
		return elements;
	}
	
	public static List<Report> getReports(TemplateDefinition templateDefinition){
		List<Report> elements = new ArrayList<>();
		if (templateDefinition != null){
			for(Object obj : templateDefinition.getAttributeOrChoiceOrElement()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.Report){
					elements.add( (Report) obj );
				}
			} 
		}
		return elements;
	}
	
	public static List<DefineVariable> getDefineVariables(TemplateDefinition templateDefinition){
		List<DefineVariable> elements = new ArrayList<>();
		if (templateDefinition != null){
			for(Object obj : templateDefinition.getAttributeOrChoiceOrElement()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.DefineVariable){
					elements.add( (DefineVariable) obj );
				}
			} 
		}
		return elements;
	}
	
	public static List<FreeFormMarkupWithLanguage> getConstraints(TemplateDefinition templateDefinition){
		List<FreeFormMarkupWithLanguage> elements = new ArrayList<>();
		if (templateDefinition != null){
			for(Object obj : templateDefinition.getAttributeOrChoiceOrElement()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage){
					elements.add( (FreeFormMarkupWithLanguage) obj );
				}
			} 
		}
		return elements;
	}
	
	/**
	 * return only a sub list of templates which are CDA templates
	 * ie : the template/@id is equal to an element templateId/@root
	 * @param ltd
	 * @return
	 */
	public static List<TemplateDefinition> extractListCDATemplate(List<TemplateDefinition> ltd){
		List<TemplateDefinition> res = new ArrayList<>();
		if (ltd != null) {
			for (TemplateDefinition templateDefinition : ltd) {
				if (templateDefinitionIsCDATemplate(templateDefinition)){
					res.add(templateDefinition);
				}
			}
		}
		return res;
	}
	
	public static List<TemplateDefinition> extractListCDATemplateFromDecorTemplateRepository(Decor dec){
		if (dec != null) {
			TemplateRepository tr = TemplateRepositoryUtil.generateTemplateRepository(dec);
			return extractListCDATemplateFromTemplateRepository(tr);
		}	
		return new ArrayList<>();
	}
	
	private static List<TemplateDefinition> extractListCDATemplateFromTemplateRepository(TemplateRepository tr) {
		return extractListCDATemplateFromTemplateRepository(tr, null);
	}
		
	private static List<TemplateDefinition> extractListCDATemplateFromTemplateRepository(TemplateRepository tr, List<TemplateRepository> listTreatedTRParam) {
		List<TemplateRepository> listTreatedTR = listTreatedTRParam;
		if (listTreatedTR == null) {
			listTreatedTR = new ArrayList<>();
		}
		List<TemplateDefinition> res = new ArrayList<>();
		if (tr != null) {
			if (tr.getCurrentDecor() != null && tr.getCurrentDecor().getRules() != null) {
				res.addAll(extractListCDATemplate(RulesUtil.getTemplates(tr.getCurrentDecor().getRules()))); 
			}
			listTreatedTR.add(tr);
			for (TemplateRepository trParent : tr.getListParentRepository()) {
				if (!listTreatedTR.contains(trParent)) {
					res.addAll(extractListCDATemplateFromTemplateRepository(trParent, listTreatedTR));
				}
			}
		}
		return res;
	}

	/**
	 * check if a templateDefinition is a CDA Template
	 * ie : verify that the template has only one root element from the UML elements,
	 * and verify that the first element has an element templateId with the right root
	 * @param templateDefinition
	 * @return
	 */
	public static boolean templateDefinitionIsCDATemplate(TemplateDefinition templateDefinition) {
		if (TemplateDefinitionUtil.templateDefinitionContainsUniqueElement(templateDefinition) && 
				!TemplateDefinitionUtil.templateDefinitionContainsChoices(templateDefinition) &&
				!TemplateDefinitionUtil.templateDefinitionContainsIncludes(templateDefinition)){
			if (templateDefinition == null || templateDefinition.getContext() == null) {
				return false;
			}
			if (TemplatesTypeMappingUtil.templateTypeCanBeHL7Template(getTypeNameOfTemplateDefinition(templateDefinition))) {
				return templateIdentifierEqualTemplateId(templateDefinition) ||
						templateTemplateIdIsUnique(templateDefinition) ||
						processedTemplateIdsHasAUnique(templateDefinition);
			}
		}
		return false;
	}
	
	public static boolean templateDefinitionContainsUniqueElement(TemplateDefinition templateDefinition) {
		return TemplateDefinitionUtil.getElements(templateDefinition).size()==1;
	}
	
	public static boolean templateDefinitionContainsIncludes(TemplateDefinition templateDefinition) {
		return (templateDefinition != null) && TemplateDefinitionUtil.getIncludes(templateDefinition)!=null && 
				!TemplateDefinitionUtil.getIncludes(templateDefinition).isEmpty();
	}
	
	public static boolean templateDefinitionContainsChoices(TemplateDefinition templateDefinition) {
		return (templateDefinition != null) && TemplateDefinitionUtil.getChoices(templateDefinition)!=null && 
				!TemplateDefinitionUtil.getChoices(templateDefinition).isEmpty();
	}
	
	public static String getTypeNameOfTemplateDefinition(TemplateDefinition templateDefinition){
		RuleDefinition first = TemplateDefinitionUtil.getFirstElement(templateDefinition);
		if (first == null) {
			return null;
		}
		String realName = RuleDefinitionUtil.getRealNameOfRuleDefinition(first);
		return TemplatesTypeMappingUtil.extractTemplateTypeFromElementNameIfExists(realName, 
				TemplateDefinitionUtil.getListSupportedClassificationLevel(templateDefinition));
	}
	
	protected static Boolean templateIdentifierEqualTemplateId(TemplateDefinition templateDefinition) {
		RuleDefinition firstRuleDefinition = TemplateDefinitionUtil.getFirstElement(templateDefinition);
		List<RuleDefinition> listRules = RuleDefinitionUtil.getElementsByName(firstRuleDefinition, "(.*:)?templateId");
		for (RuleDefinition ruleDefinition : listRules) {
			Attribute attr = RuleDefinitionUtil.getAttributeByName(ruleDefinition, ROOT);
			String val = AttributeUtil.getAttributeValue(attr, ROOT);
			if (val != null && templateDefinition != null && val.equals(templateDefinition.getId())){
				return true;
			}
		}
		return false;
	}
	
	private static Boolean templateTemplateIdIsUnique(TemplateDefinition templateDefinition) {
		RuleDefinition firstRuleDefinition = TemplateDefinitionUtil.getFirstElement(templateDefinition);
		List<RuleDefinition> listRules = RuleDefinitionUtil.getElementsByName(firstRuleDefinition, "(.*:)?templateId");
		List<RuleDefinition> listRulesres = new ArrayList<>();
		for (RuleDefinition ruleDefinition : listRules) {
			Attribute attr = RuleDefinitionUtil.getAttributeByName(ruleDefinition, ROOT);
			String val = AttributeUtil.getAttributeValue(attr, ROOT);
			if (val != null){
				listRulesres.add(ruleDefinition);
			}
		}
		return listRulesres.size()==1;
	}
	
	/**
	 * This method explore the referenced templates and extract the unique id
	 * used by this template
	 * 
	 * @param templateDefinition
	 * @return
	 */
	private static Boolean processedTemplateIdsHasAUnique(TemplateDefinition templateDefinition) {
		Set<String> listCurrentTemplateIds = TemplateDefinitionUtil.extractListTemplateIdForSpecialization(templateDefinition);
		return listCurrentTemplateIds!= null && listCurrentTemplateIds.size()==1;
	}
	
	protected static Set<String> extractListTemplateIdForSpecialization(TemplateDefinition templateDefinition) {
		return TNoeudUtil.extractListTemplateIdForIdentificationFromTD(templateDefinition);
	}
	
	public static Set<String> extractListCurrentTemplateId(TemplateDefinition templateDefinition) {
		Set<String> setTemplateIds = new HashSet<>();
		RuleDefinition firstRuleDefinition = TemplateDefinitionUtil.getFirstElement(templateDefinition);
		List<RuleDefinition> listRules = RuleDefinitionUtil.getElementsByName(firstRuleDefinition, "templateId");
		for (RuleDefinition ruleDefinition : listRules) {
			Attribute attr = RuleDefinitionUtil.getAttributeByName(ruleDefinition, ROOT);
			if (attr != null){
				String val = AttributeUtil.getAttributeValue(attr, ROOT);
				if (val != null && !val.trim().isEmpty()){
					setTemplateIds.add(val);
				}
			}
		}
		return setTemplateIds;
	}

	public static List<TemplateDefinition> extractListSpecialization(TemplateDefinition templateDefinition, TemplateRepository templateRepository) {
		List<TemplateDefinition> res = new ArrayList<>();
		List<TemplateRelationships> ltr = templateDefinition.getRelationship();
		for (TemplateRelationships templateRelationships : ltr) {
			if (templateRelationships.getType() != null &&
					Arrays.asList(RelationshipTypes.GEN, RelationshipTypes.SPEC, RelationshipTypes.COPY, RelationshipTypes.VERSION, 
						RelationshipTypes.REPL)
						.contains(templateRelationships.getType())){
				TemplateDefinition parent = TemplateRepositoryUtil.findTemplateDefinitionById(templateRepository, templateRelationships.getTemplate());
				if (parent != null && (
						templateRelationships.getType() != RelationshipTypes.VERSION || 
								(parent.getId() != templateDefinition.getId())
						)
					){
					res.add(parent);
				}
			}
		}
		res.remove(templateDefinition);
		return res;
	}

	public static String extractTemplateId(TemplateDefinition templateDefinition) {
		if (templateIdentifierEqualTemplateId(templateDefinition)) {
			return templateDefinition.getId();
		}
		else if (templateTemplateIdIsUnique(templateDefinition)){
			RuleDefinition firstRuleDefinition = TemplateDefinitionUtil.getFirstElement(templateDefinition);
			RuleDefinition ruleDefinition = RuleDefinitionUtil.getElementByName(firstRuleDefinition, "templateId");
			Attribute attr = RuleDefinitionUtil.getAttributeByName(ruleDefinition, ROOT);
			return AttributeUtil.getAttributeValue(attr, ROOT);
		}
		else if (processedTemplateIdsHasAUnique(templateDefinition)){
			Set<String> listCurrentTemplateIds = TemplateDefinitionUtil.extractListTemplateIdForSpecialization(templateDefinition);
			return listCurrentTemplateIds.iterator().next();
		}
		return null;
	}
	
	/**
	 * return the all elements that have the same name of the param name
	 * 
	 * @param templateDefinition template
	 * @param name -&gt; example : {hl7:templateId, hl7:templateId}
	 * @return object list
	 */
	public static List<RuleDefinition> getElementsByName(TemplateDefinition templateDefinition, String name){
		List<RuleDefinition> res = new ArrayList<>();
		List<RuleDefinition> la = TemplateDefinitionUtil.getElements(templateDefinition);
		for (RuleDefinition rul : la) {
			if (rul.getName().matches("(.*:)?" + name)){
				res.add(rul);
			}
		}
		return res;
	}
	
	public static boolean referencedTemplateIsCDA(String identifier, Decor decor) {
		if (decor != null) {
			TemplateRepository tr = TemplateRepositoryUtil.generateTemplateRepository(decor);
			return referencedTemplatedIsCDA(identifier, tr);
		}
		return false;
	}
	
	private static boolean referencedTemplatedIsCDA(String identifier, TemplateRepository tr) {
		return referencedTemplatedIsCDA(identifier, tr, null);
	}

	private static boolean referencedTemplatedIsCDA(String identifier, TemplateRepository tr, List<TemplateRepository> listProcessedTRParam) {
		List<TemplateRepository> listProcessedTR = (listProcessedTRParam == null)?new ArrayList<TemplateRepository>():listProcessedTRParam;
		if (tr != null && !listProcessedTR.contains(tr) && tr.getCurrentDecor() != null && tr.getCurrentDecor().getRules() != null && 
				RulesUtil.getTemplates(tr.getCurrentDecor().getRules()) != null) {
			boolean isOk = referencedTemplateIsCDA(identifier, RulesUtil.getTemplates(tr.getCurrentDecor().getRules()));
			if (!isOk) {
				listProcessedTR.add(tr);
				for (TemplateRepository trParent : tr.getListParentRepository()) {
					isOk = referencedTemplatedIsCDA(identifier, trParent, listProcessedTR);
				}
			}
			return isOk;
		}
		return false;
	}
	
	public static boolean referencedTemplateIsCDA(String identifier, List<TemplateDefinition> listHL7Templates) {
		List<TemplateDefinition> listCDATemplate = TemplateDefinitionUtil.extractListCDATemplate(listHL7Templates);
		if (identifier != null) {
			for (TemplateDefinition td : listCDATemplate) {
				if (td.getId() != null && td.getId().equals(identifier)){
					return true;
				}
			}
		}
		return false;
	}

	public static List<ContainDefinition> getContains(TemplateDefinition templateDefinition) {
		List<ContainDefinition> templates = new ArrayList<>();
		if (templateDefinition != null){
			for(Object obj : templateDefinition.getAttributeOrChoiceOrElement()){
				if (obj instanceof net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition){
						templates.add( (ContainDefinition) obj );
				}
			} 
		}
		return templates;
	}

	public static TemplateStatusCodeLifeCycle getRealStatusCode(TemplateDefinition templateDefinition) {
		if (templateDefinition != null) {
			TemplateStatusCodeLifeCycle stat = templateDefinition.getStatusCode();
			if (stat == null) {
				stat = TemplateStatusCodeLifeCycle.ACTIVE;
			}
			return stat;
		}
		return null;
	}

	public static List<TemplateDefinition> extractListTemplateDefinitionById(List<TemplateDefinition> ltdcleaned, String id) {
		List<TemplateDefinition> res = new ArrayList<>();
		if (ltdcleaned != null && id != null) {
			for (TemplateDefinition templateDefinition : ltdcleaned) {
				if (templateDefinition.getId() != null && templateDefinition.getId().equals(id)) {
					res.add(templateDefinition);
				}
			}
		}
		return res;
	}
	
	public static List<String> getListSupportedClassificationLevel(TemplateDefinition td) {
		List<String> res = new ArrayList<>();
		if (td != null) {
			for (TemplateProperties tp : td.getClassification()) {
				if (tp.getType() != null) {
					res.add(tp.getType().value());
				}
			}
		}
		return res;
	}
	
}
