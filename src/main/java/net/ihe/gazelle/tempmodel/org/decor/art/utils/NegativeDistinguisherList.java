package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class NegativeDistinguisherList {
	
	
	private static Logger log = LoggerFactory.getLogger(NegativeDistinguisherList.class);
	
	private NegativeDistinguisherList() {}
	
	private static final Map<RuleDefinition, Boolean> mapNegativeDistinguisher = new HashMap<>();
	
	private static Boolean treatNegativeDistinguisher = true;
	
	private static NegativeDistinguisherVerifier verifier = null;
	
	public static void updateNegativeDistinguisherVerifier(NegativeDistinguisherVerifier negver) {
		NegativeDistinguisherList.verifier = negver;
	}
	
	public static Boolean getTreatNegativeDistinguisher() {
		return treatNegativeDistinguisher;
	}

	public static void setTreatNegativeDistinguisher(
			Boolean treatNegativeDistinguisher) {
		NegativeDistinguisherList.treatNegativeDistinguisher = treatNegativeDistinguisher;
	}

	// TODO use location of RD instead of dpath (not unique)
	public static void defineRuleDefinitionAsNegativeDistinguisher(RuleDefinition ruleDefinition, boolean hasNegativeDistinguisher) {
		boolean hasNegativeDistinguisherGlobally = hasNegativeDistinguisher;
		if (!treatNegativeDistinguisher) {
			hasNegativeDistinguisherGlobally = false;
		}
		if (verifier != null) {
			hasNegativeDistinguisherGlobally = hasNegativeDistinguisherGlobally && verifier.acceptNegativeDistinguisher(ruleDefinition);
		}
		if (hasNegativeDistinguisherGlobally) {
			DParent dpath = RuleDefinitionUtil.getDParentOfRuleDefinition(ruleDefinition);
			String path = DPathExtractor.createPathFromDParent(dpath);
			log.info("treated as negative distinguisher : {}", path);
		}
		mapNegativeDistinguisher.put(ruleDefinition, hasNegativeDistinguisherGlobally);
	}
	
	public static Boolean ruleDefinitionHasNegativeDistinguisher(RuleDefinition ruleDefinition) {
		if (ruleDefinition != null) {
			return mapNegativeDistinguisher.get(ruleDefinition);
		}
		return null;
	}
	
	/**
	 * used only for testing purpose
	 */
	public static void cleanMapNegativeDistinguisher() {
		mapNegativeDistinguisher.clear();
	}
	
}
