package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.goc.uml.utils.PathWithDT;
import net.ihe.gazelle.goc.uml.utils.PathWithDTUtil;
import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.tempmodel.decor.dt.model.AttDatatype;
import net.ihe.gazelle.tempmodel.dpath.model.DAttribute;
import net.ihe.gazelle.tempmodel.dpath.model.DElement;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dtdefault.utils.AttrDTUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;

/**
 * 
 * @author Abderrazek Boufahja
 * 
 */
public final class AttributeUtil {
	
	
	private static Logger log = LoggerFactory.getLogger(AttributeUtil.class);

	private AttributeUtil() {
	}

	public static String getAttributeValue(Attribute attribute, String attrName) {
		if (attribute == null || attrName == null) {
			return null;
		}
		if (attribute.getName() != null && attribute.getName().equals(attrName)) {
			return attribute.getValue();
		} else if (attrName.equals("classCode") && attribute.getClassCode() != null) {
			return attribute.getClassCode();
		} else if (attrName.equals("contextConductionInd") && attribute.isContextConductionInd() != null) {
			return String.valueOf(attribute.isContextConductionInd());
		} else if (attrName.equals("contextControlCode") && attribute.getContextControlCode() != null) {
			return attribute.getContextControlCode();
		} else if (attrName.equals("determinerCode") && attribute.getDeterminerCode() != null) {
			return attribute.getDeterminerCode();
		} else if (attrName.equals("extension") && attribute.getExtension() != null) {
			return attribute.getExtension();
		} else if (attrName.equals("independentInd") && attribute.isIndependentInd() != null) {
			return String.valueOf(attribute.isIndependentInd());
		} else if (attrName.equals("institutionSpecified") && attribute.isInstitutionSpecified() != null) {
			return String.valueOf(attribute.isInstitutionSpecified());
		} else if (attrName.equals("inversionInd") && attribute.isInversionInd() != null) {
			return String.valueOf(attribute.isInversionInd());
		} else if (attrName.equals("mediaType") && attribute.getMediaType() != null) {
			return attribute.getMediaType();
		} else if (attrName.equals("moodCode") && attribute.getMoodCode() != null) {
			return attribute.getMoodCode();
		} else if (attrName.equals("negationInd") && attribute.getNegationInd() != null) {
			return attribute.getNegationInd();
		} else if (attrName.equals("nullFlavor") && attribute.getNullFlavor() != null) {
			return attribute.getNullFlavor();
		} else if (attrName.equals("operator") && attribute.getOperator() != null) {
			return attribute.getOperator();
		} else if (attrName.equals("qualifier") && attribute.getQualifier() != null) {
			return attribute.getQualifier();
		} else if (attrName.equals("representation") && attribute.getRepresentation() != null) {
			return attribute.getRepresentation();
		} else if (attrName.equals("root") && attribute.getRoot() != null) {
			return attribute.getRoot();
		} else if (attrName.equals("typeCode") && attribute.getTypeCode() != null) {
			return attribute.getTypeCode();
		} else if (attrName.equals("unit") && attribute.getUnit() != null) {
			return attribute.getUnit();
		} else if (attrName.equals("use") && attribute.getUse() != null) {
			return attribute.getUse();
		}
		return null;

	}

	public static List<String> getListAttributesNames(Attribute attr) {
		if (attr == null) {
			return new ArrayList<>();
		}
		List<String> listNames = new ArrayList<>();
		updateListAttributesName(attr.getName(), attr.getName(), listNames);
		updateListAttributesName(attr.getClassCode(), "classCode", listNames);
		updateListAttributesName(attr.isContextConductionInd(), "contextConductionInd", listNames);
		updateListAttributesName(attr.getContextControlCode(), "contextControlCode", listNames);
		updateListAttributesName(attr.getDeterminerCode(), "determinerCode", listNames);
		updateListAttributesName(attr.getExtension(), "extension", listNames);
		updateListAttributesName(attr.isIndependentInd(), "independentInd", listNames);
		updateListAttributesName(attr.isInstitutionSpecified(), "institutionSpecified", listNames);
		updateListAttributesName(attr.isInversionInd(), "inversionInd", listNames);
		updateListAttributesName(attr.getMediaType(), "mediaType", listNames);
		updateListAttributesName(attr.getMoodCode(), "moodCode", listNames);
		updateListAttributesName(attr.getNegationInd(), "negationInd", listNames);
		updateListAttributesName(attr.getNullFlavor(), "nullFlavor", listNames);
		updateListAttributesName(attr.getOperator(), "operator", listNames);
		updateListAttributesName(attr.getQualifier(), "qualifier", listNames);
		updateListAttributesName(attr.getRepresentation(), "representation", listNames);
		updateListAttributesName(attr.getRoot(), "root", listNames);
		updateListAttributesName(attr.getTypeCode(), "typeCode", listNames);
		updateListAttributesName(attr.getUnit(), "unit", listNames);
		updateListAttributesName(attr.getUse(), "use", listNames);
		return listNames;
	}
	
	private static void updateListAttributesName(Object val, String name, List<String> listNames) {
		if (val != null) {
			listNames.add(name);
		}
	}

	public static DParent getDParentOfTheParent(Attribute attribute) {
		DParent parentPath = null;
		if (attribute.getParentObject() instanceof RuleDefinition) {
			parentPath = RuleDefinitionUtil.getDParentOfRuleDefinition((RuleDefinition) attribute.getParentObject());
		} else {
			log.error("This case is not treatable yet : getDParentOfTheParent");
		}
		return parentPath;
	}

	public static TemplateDefinition getParentTemplateDefinition(Attribute attribute) {
		if (attribute.getParentObject() instanceof RuleDefinition) {
			return RuleDefinitionUtil.getParentTemplateDefinition((RuleDefinition) attribute.getParentObject());
		} else if (attribute.getParentObject() instanceof TemplateDefinition) {
			return (TemplateDefinition) attribute.getParentObject();
		} else {
			log.error("This case is not treatable yet : getParentTemplateDefinition");
		}
		return null;
	}

	public static DParent getDParentOfAttibute(Attribute attribute) {
		if (attribute != null) {
			String name = AttributeUtil.getBetterMatchingName(attribute);
			String value = AttributeUtil.getBetterMatchingValue(attribute);
			return getDParentOfAttibute(attribute, name, value);
		}
		return null;
	}

	public static DParent getDParentOfAttibute(Attribute attribute, String attrName, String attrValue) {
		if (attribute.getParentObject() instanceof RuleDefinition) {
			DParent parent = RuleDefinitionUtil.getDParentOfRuleDefinition((RuleDefinition) attribute.getParentObject());
			if (attrValue != null) {
				DAttribute attr = new DAttribute();
				attr.setName(attrName);
				attr.setValue(attrValue);
				((DElement) parent.getTail()).setDistinguisherAttributeOrElement(attr);

			}
			DAttribute attrF = new DAttribute();
			attrF.setName(attrName);
			((DElement) parent.getTail()).setFollowingAttributeOrElement(attrF);
			return parent;
		}
		return null;
	}

	// TODO here we should use the complete class name instead of only the name of the class type
	public static String getUMLTypeName(Attribute attribute, String attrNameParam) {
		String attrName = attrNameParam;
		if (attribute.getParentObject() instanceof RuleDefinition) {
			if (attrName == null) {
				attrName = AttributeUtil.getBetterMatchingName(attribute);
			}
			List<PathWithDT> lpwdt = RuleDefinitionUtil.getJavaPath((RuleDefinition) attribute.getParentObject());
			String path = PathWithDTUtil.extractTypedPathFromListPathWithDT(lpwdt);
			String pathAttr = path + "." + attrName;
			return UMLLoader.getPropertyTypeName(pathAttr);
		}
		return null;
	}

	/**
	 * assert that the datatype specified for the attribute is different than the default datatype specified
	 * 
	 * @param selectedAttribute
	 * @param dt
	 * @return
	 */
	public static boolean assertDatatypeDifferent(Attribute selectedAttribute, String attrName, AttDatatype dt) {
		if (selectedAttribute.getParentObject() instanceof RuleDefinition) {
			String attrType = RuleDefinitionUtil.getConstrainedUMLTypeName((RuleDefinition) selectedAttribute
					.getParentObject());
			String bl = AttrDTUtil.getOriginalDTOfType(attrType, attrName);
			return bl == null || !bl.equals(dt.getName());
		}
		return false;
	}

	public static boolean assertDatatypeIsForValidAttribute(Attribute selectedAttribute, String attrName) {
		if (selectedAttribute.getParentObject() instanceof RuleDefinition) {
			String attrType = RuleDefinitionUtil.getConstrainedUMLTypeName((RuleDefinition) selectedAttribute
					.getParentObject());
			String bl = AttrDTUtil.getOriginalDTOfType(attrType, attrName);
			if (bl != null) {
				return true;
			}
		}
		return false;
	}

	public static boolean assertAttributeIsNotRequiredByDefault(Attribute selectedAttribute, String attrName) {
		if (selectedAttribute.getParentObject() instanceof RuleDefinition) {
			String attrType = RuleDefinitionUtil.getConstrainedUMLTypeName((RuleDefinition) selectedAttribute
					.getParentObject());
			int min = UMLLoader.getMinAttribute(attrType, attrName);
			if (min == 0) {
				return true;
			}
		}
		return false;
	}

	public static String getBetterMatchingName(Attribute selectedAttribute) {
		if (selectedAttribute != null && selectedAttribute.getName() != null) {
			return selectedAttribute.getName();
		}
		List<String> ln = getListAttributesNames(selectedAttribute);
		if (ln != null && !ln.isEmpty()) {
			return ln.get(0);
		}
		return null;
	}

	public static String getBetterMatchingValue(Attribute selectedAttribute) {
		String name = getBetterMatchingName(selectedAttribute);
		return getAttributeValue(selectedAttribute, name);
	}

	public static Integer getUMLMaxAttribute(Attribute attribute) {
		return getUMLMaxAttribute(attribute, null);
	}

	public static Integer getUMLMaxAttribute(Attribute attribute, String attributeNameParam) {
		String attributeName = attributeNameParam;
		if (attribute != null && attribute.getParentObject() != null && attribute.getParentObject() instanceof RuleDefinition) {
			if (attributeName == null) {
				attributeName = AttributeUtil.getBetterMatchingName(attribute);
			}
			if (attributeName != null) {
				List<PathWithDT> lpwdt = RuleDefinitionUtil.getJavaPath((RuleDefinition) attribute.getParentObject());
				String path = PathWithDTUtil.extractTypedPathFromListPathWithDT(lpwdt);
				return UMLLoader.getMaxAttribute(path, attributeName);
			}
		}
		return null;
	}

	public static String getJavaPath(Attribute attribute, String attributeNameParam) {
		String attributeName = attributeNameParam;
		if (attribute != null && attribute.getParentObject() instanceof RuleDefinition) {
			if (attributeName == null) {
				attributeName = AttributeUtil.getBetterMatchingName(attribute);
			}
			return PathWithDTUtil.extractPathFromListPathWithDT(RuleDefinitionUtil
					.getJavaPath((RuleDefinition) attribute.getParentObject())) + "." + attributeName;
		}
		return null;
	}

}
