package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.tempmodel.distinguisher.ElementDiscriber;
import net.ihe.gazelle.tempmodel.distinguisher.utils.DistinguisherUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author Abderrazek Boufahja
 * the class has not private constructor because it is extended by a testing class
 */
public class RuleDefinitionProblemUtil {


    private static Logger log = LoggerFactory.getLogger(RuleDefinitionProblemUtil.class);

    public static boolean isStringDatatype(RuleDefinition ruleDefinition) {
        String type = RuleDefinitionUtil.getConstrainedUMLTypeName(ruleDefinition);
        return UMLLoader.datatypeHasMixedElements(type);
    }

    //TODO process contain attribute !!
    public static boolean verifyIfRuleDefinitionIsTreatable(RuleDefinition ruleDefinition) {
        return ruleDefinitionNameIsTreatable(ruleDefinition) && ruleDefinitionHasKnownType(ruleDefinition) &&
                (
                        verifyThatRuleDefinitionIsUnique(ruleDefinition) ||
                                verifyThatRuleDefinitionHasUniqueDistinguisher(ruleDefinition) ||
                                verifyThatRuleDefinitionCanHaveNegativeDistinguisher(ruleDefinition)
                );
    }

    public static boolean ruleDefinitionHasKnownType(RuleDefinition ruleDefinition) {
        String umlType = RuleDefinitionUtil.getConstrainedUMLTypeName(ruleDefinition);
        return umlType != null && !umlType.equals("--") && !umlType.trim().equals("");
    }

    public static boolean ruleDefinitionNameIsTreatable(RuleDefinition ruleDefinition) {
        return ruleDefinition != null &&
                ruleDefinition.getName() != null &&
                !ruleDefinition.getName().matches(".*?[\\[\\]@/].*");
    }

    public static boolean verifyThatRuleDefinitionCanHaveNegativeDistinguisher(RuleDefinition ruleDefinition) {
        Boolean isNeg = NegativeDistinguisherList.ruleDefinitionHasNegativeDistinguisher(ruleDefinition);
        if (isNeg != null) {
            return isNeg;
        }
        Map<RuleDefinition, Pair<String, String>> ldis = RuleDefinitionDistinguisherUtil.extractListDistinguishersForBrodhers(ruleDefinition);
        if (ldis != null && !ldis.isEmpty()) {
            if (!ldis.containsKey(ruleDefinition)) {
                log.error("this case is not supposed to happen : ruleDefinition is not a key !");
                NegativeDistinguisherList.defineRuleDefinitionAsNegativeDistinguisher(ruleDefinition, false);
                return false;
            }
            if (ldis.get(ruleDefinition) != null) {
                NegativeDistinguisherList.defineRuleDefinitionAsNegativeDistinguisher(ruleDefinition, false);
                return false;
            }
            int numberNull = 0;
            for (Entry<RuleDefinition, Pair<String, String>> entry : ldis.entrySet()) {
                if (entry.getValue() == null) {
                    numberNull++;
                }
            }
            boolean hasNeg = (numberNull == 1) && (ldis.size() > 1);
            NegativeDistinguisherList.defineRuleDefinitionAsNegativeDistinguisher(ruleDefinition, hasNeg);
            // we do not return directly hasNeg because it is sometimes overrided by NegativeDistinguisherList
            return NegativeDistinguisherList.ruleDefinitionHasNegativeDistinguisher(ruleDefinition);
        }
        NegativeDistinguisherList.defineRuleDefinitionAsNegativeDistinguisher(ruleDefinition, false);
        return false;
    }

    public static boolean verifyThatRuleDefinitionHasUniqueDistinguisher(RuleDefinition ruleDefinition) {
        Pair<String, String> uniqueDistinguisher = RuleDefinitionDistinguisherUtil.extractUniqueDistinguisher(ruleDefinition);
        return uniqueDistinguisher != null;
    }

    public static boolean verifyThatRuleDefinitionIsUnique(RuleDefinition ruleDefinition) {
        Object parent = ruleDefinition.getParentObject();
        if (parent instanceof TemplateDefinition) {
            List<RuleDefinition> lrd = TemplateDefinitionUtil.getElementsByName((TemplateDefinition) parent,
                    RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition));
            return lrd.size() == 1;
        } else if (parent instanceof RuleDefinition) {
            RuleDefinition parentRD = (RuleDefinition) parent;
            List<RuleDefinition> lrd = RuleDefinitionUtil.getElementsByName(parentRD,
                    RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition));
            return lrd.size() == 1;
        } else if (parent instanceof ChoiceDefinition) {
            ChoiceDefinition choiceDefinition = (ChoiceDefinition) parent;
            if (choiceDefinition.getParentObject() instanceof RuleDefinition) {
                RuleDefinition parentOfChoice = (RuleDefinition) choiceDefinition.getParentObject();
                List<RuleDefinition> lrd = RuleDefinitionUtil.getElementsByName(parentOfChoice,
                        RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition));
                return lrd.size() == 1;
            }
        }
        return false;
    }

    /**
     * the ruleDefinistions with negative distinguisher are not count
     *
     * @param ruleDefinition rule
     * @return boolean
     */
    // TODO generalize the "templateId" because it is a CDA specification
    public static boolean ruleDefinitionNeedsDistinguisher(RuleDefinition ruleDefinition) {
        if (ruleDefinition != null) {
            String nameRD = RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition);
            boolean rdIsUniqueAndDoesNotNeedDistinguisher = (nameRD != null) &&
                    !RuleDefinitionProblemUtil.verifyThatRuleDefinitionNeedAlwaysADistinguisher(ruleDefinition) &&
                    RuleDefinitionProblemUtil.verifyThatRuleDefinitionIsUnique(ruleDefinition)
                    && !nameRD.equals("templateId");
            return !rdIsUniqueAndDoesNotNeedDistinguisher
                    && RuleDefinitionProblemUtil.verifyThatRuleDefinitionHasUniqueDistinguisher(ruleDefinition);
        }
        return false;
    }

    protected static boolean verifyThatRuleDefinitionNeedAlwaysADistinguisher(RuleDefinition ruleDefinition) {
        ElementDiscriber eldisc = DistinguisherUtil.findElementDiscriber(ruleDefinition);
        return eldisc != null && eldisc.getNeedDistinguisher() != null && eldisc.getNeedDistinguisher();
    }

    public static boolean ruleDefinitionNeedDistinguisherOrCanHaveNegatifDistinguisher(RuleDefinition ruleDefinition) {
        return RuleDefinitionProblemUtil.ruleDefinitionNeedsDistinguisher(ruleDefinition) ||
                RuleDefinitionProblemUtil.verifyThatRuleDefinitionCanHaveNegativeDistinguisher(ruleDefinition);
    }

}
