package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.Comparator;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;

public class ValueSetConceptComparator implements Comparator<ValueSetConcept> {

	@Override
	public int compare(ValueSetConcept arg0, ValueSetConcept arg1) {
		if (arg0 == null) {
			return -1;
		}
		if (arg1 == null) {
			return 1;
		}
		if (arg0.getCode() == null) {
			return -1;
		}
		if (arg1.getCode() == null) {
			return 1;
		}
		return arg0.getCode().compareTo(arg1.getCode());
	}

}
