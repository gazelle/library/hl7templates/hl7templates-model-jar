package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class CloneUtil {
	
	private static Logger log = LoggerFactory.getLogger(CloneUtil.class);
	
	private CloneUtil() {}
	
	@SuppressWarnings("unchecked")
	public static <T> T cloneHL7TemplateEl(T el) {
		if (el != null) {
			try {
				JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.tempmodel.org.decor.art.model");
				Marshaller mar = jc.createMarshaller();
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				mar.marshal(el, baos);
				ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
				Unmarshaller um = jc.createUnmarshaller();
				return (T) um.unmarshal(bais);
			}
			catch(Exception e){
				log.error("problem to clone element : " + el.getClass().getSimpleName(), e);
				return null;
			}
		}
		return null;
	}

}
