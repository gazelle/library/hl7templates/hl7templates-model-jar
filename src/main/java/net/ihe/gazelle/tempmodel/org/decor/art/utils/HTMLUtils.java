package net.ihe.gazelle.tempmodel.org.decor.art.utils;
import org.jsoup.Jsoup;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class HTMLUtils {
	
	private HTMLUtils() {}

	public static String extractText(byte[] content){
		return Jsoup.parse(new String(content)).text();
	}

}