//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.09 at 11:11:31 AM CEST 
//


package net.ihe.gazelle.tempmodel.org.decor.art.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TemplateTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TemplateTypes"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="cdadocumentlevel"/&gt;
 *     &lt;enumeration value="messagelevel"/&gt;
 *     &lt;enumeration value="cdaheaderlevel"/&gt;
 *     &lt;enumeration value="cdasectionlevel"/&gt;
 *     &lt;enumeration value="cdaentrylevel"/&gt;
 *     &lt;enumeration value="segmentlevel"/&gt;
 *     &lt;enumeration value="clinicalstatementlevel"/&gt;
 *     &lt;enumeration value="controlactlevel"/&gt;
 *     &lt;enumeration value="payloadlevel"/&gt;
 *     &lt;enumeration value="datatypelevel"/&gt;
 *     &lt;enumeration value="notype"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TemplateTypes")
@XmlEnum
public enum TemplateTypes {


    /**
     * CDA document level template
     * 
     */
    @XmlEnumValue("cdadocumentlevel")
    CDADOCUMENTLEVEL("cdadocumentlevel"),

    /**
     * HL7 V2 or V3 message level template
     * 
     */
    @XmlEnumValue("messagelevel")
    MESSAGELEVEL("messagelevel"),

    /**
     * CDA header level template
     * 
     */
    @XmlEnumValue("cdaheaderlevel")
    CDAHEADERLEVEL("cdaheaderlevel"),

    /**
     * CDA section level template
     * 
     */
    @XmlEnumValue("cdasectionlevel")
    CDASECTIONLEVEL("cdasectionlevel"),

    /**
     * CDA entry level template
     * 
     */
    @XmlEnumValue("cdaentrylevel")
    CDAENTRYLEVEL("cdaentrylevel"),

    /**
     * HL7 V2 segment level template
     * 
     */
    @XmlEnumValue("segmentlevel")
    SEGMENTLEVEL("segmentlevel"),

    /**
     * HL7 V3 clinical statement level template
     * 
     */
    @XmlEnumValue("clinicalstatementlevel")
    CLINICALSTATEMENTLEVEL("clinicalstatementlevel"),

    /**
     * HL7 V3 Control Act level template
     * 
     */
    @XmlEnumValue("controlactlevel")
    CONTROLACTLEVEL("controlactlevel"),

    /**
     * HL7 V3 Payload level template
     * 
     */
    @XmlEnumValue("payloadlevel")
    PAYLOADLEVEL("payloadlevel"),

    /**
     * HL7 V2 or V3 data type level template
     * 
     */
    @XmlEnumValue("datatypelevel")
    DATATYPELEVEL("datatypelevel"),

    /**
     * Template type not specified
     * 
     */
    @XmlEnumValue("notype")
    NOTYPE("notype");
    private final String value;

    TemplateTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TemplateTypes fromValue(String v) {
        for (TemplateTypes c: TemplateTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
