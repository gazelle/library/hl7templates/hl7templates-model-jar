package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Project;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProjectHistory;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProjectRelease;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class ProjectTypeUtil {
	
	private ProjectTypeUtil() {}

	/**
	 * method not needed by goc
	 * @param t
	 * @return
	 */
	public static List<ProjectRelease> getReleases(Project t) {
		return new ArrayList<>();
	}

	/**
	 * method not needed by goc
	 * @param t
	 * @return
	 */
	public static List<ProjectHistory> getVersions(Project t) {
		return new ArrayList<>();
	}

}
