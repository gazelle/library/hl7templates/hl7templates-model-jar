package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.Collection;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VocabType;

public final class ValueSetConceptUtil {
	
	private ValueSetConceptUtil() {}
	
	public static boolean listValueSetConceptContainCode(String code, Collection<ValueSetConcept> lvsc) {
		if (code != null && lvsc != null) {
			for (ValueSetConcept valueSetConcept : lvsc) {
				if (valueSetConcept.getCode() != null && valueSetConcept.getCode().equals(code)) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean valueSetConceptIsNotAbstract(ValueSetConcept valueSetConcept) {
		if (valueSetConcept != null) {
			return valueSetConcept.getType() == null || valueSetConcept.getType() != VocabType.A;
		}
		return false;
	}
	
}
