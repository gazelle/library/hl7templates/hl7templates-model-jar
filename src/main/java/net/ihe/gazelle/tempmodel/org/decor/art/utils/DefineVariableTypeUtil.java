package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import net.ihe.gazelle.tempmodel.org.decor.art.model.DefineVariable;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VarCode;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VarUse;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class DefineVariableTypeUtil {
	
	private DefineVariableTypeUtil() {}

	/**
	 * 
	 * @param t
	 * @return this method is not needed
	 */
	public static VarCode getCodes(DefineVariable t) {
		return null;
	}

	/**
	 * 
	 * @param t
	 * @return this method is not needed
	 */
	public static VarUse getUses(DefineVariable t) {
		return null;
	}

}
