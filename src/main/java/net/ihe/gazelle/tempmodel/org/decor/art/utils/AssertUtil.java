package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;


/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class AssertUtil {
	
	
	private static Logger log = LoggerFactory.getLogger(AssertUtil.class);
	
	private AssertUtil() {}
	
	public static TemplateDefinition getParentTemplateDefinition(Assert assert1){
		if (assert1 != null && assert1.getParentObject() != null){
			if (assert1.getParentObject() instanceof TemplateDefinition){
				return (TemplateDefinition) assert1.getParentObject();
			}
			else if (assert1.getParentObject() instanceof RuleDefinition){
				return RuleDefinitionUtil.getParentTemplateDefinition((RuleDefinition)assert1.getParentObject());
			}
			else {
				log.error("problem processing assert1 : " + assert1.getTest() + " (AssertUtil::getParentTemplateDefinition");
			}
		}
		return null;
	}
	
	public static String getStringValue(Assert assert1){
		StringBuilder res = new StringBuilder();
		int i = 0;
		for(Object obj : assert1.getContent()){
			if (obj instanceof String){
				if (i++>0) {
					res.append(", ");
				}
				res.append((String)obj);
			}
		} 
		return res.toString();
	}

}
