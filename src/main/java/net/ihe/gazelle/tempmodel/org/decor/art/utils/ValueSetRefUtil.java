package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.List;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class ValueSetRefUtil {
	
	private ValueSetRefUtil() {}
	
	public static ValueSet extractReferencedValueSet(ValueSetRef vsr, List<ValueSet> lvs) {
		if (vsr != null) {
			return extractReferencedValueSet(vsr.getRef(), lvs);
		}
		return null;
	}
	
	public static ValueSet extractReferencedValueSet(String ref, List<ValueSet> lvs) {
		if (ref != null && lvs != null) {
			for (ValueSet valueSet : lvs) {
				if (valueSet.getId()!= null && valueSet.getId().equals(ref)) {
					return valueSet;
				}
			}
		}
		return null;
	}

}
