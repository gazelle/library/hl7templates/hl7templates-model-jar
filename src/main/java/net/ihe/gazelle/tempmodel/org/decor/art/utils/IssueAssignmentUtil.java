package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Author;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueAssignment;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class IssueAssignmentUtil {
	
	private IssueAssignmentUtil() {}

	/**
	 * method not needed for goc
	 * @param t
	 * @return
	 */
	public static Author getAuthors(IssueAssignment t) {
		return null;
	}

	/**
	 * method not needed for goc
	 * @param t
	 * @return
	 */
	public static List<FreeFormMarkupWithLanguage> getDescs(IssueAssignment t) {
		return new ArrayList<>();
	}

}
