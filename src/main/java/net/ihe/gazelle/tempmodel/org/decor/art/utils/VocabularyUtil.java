package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class VocabularyUtil {
	
	
	private static Logger log = LoggerFactory.getLogger(VocabularyUtil.class);
	
	private VocabularyUtil() {}
	
	public static boolean isVocabularyListUseful(List<Vocabulary> lv, Decor decor){
		if (lv == null || lv.isEmpty()){
			return false;
		}
		for (Vocabulary vocabulary : lv) {
			if (isVocabularyListUseful(vocabulary, decor)){
				return true;
			}
		}
		return false;
	}
	
	public static boolean isVocabularyListUseful(Vocabulary vocabulary, Decor decor){
		if (vocabulary.getCode()!= null || vocabulary.getCodeSystem() != null){
			return true;
		}
		return vocabulary.getValueSet()!=null &&
				(StringUtil.isOID(vocabulary.getValueSet()) || DecorUtil.containValueSet(vocabulary.getValueSet(), decor));
	}

	public static String getRealValueSetOid(String valueSet, Decor decor) {
		if (valueSet != null) {
			if (StringUtil.isOID(valueSet)){
				return valueSet;
			}
			else {
				String oid = DecorUtil.getValueSetOid(valueSet, decor);
				if (oid != null){
					return oid;
				}
			}
		}
		return null;
	}

	public static boolean hasExceptionValues(List<Vocabulary> vocabulary, Decor decor) {
		Set<ValueSetConcept> svsc = extractListRelatedException(vocabulary, decor);
		return svsc != null && !svsc.isEmpty();
	}
	
	public static Set<ValueSetConcept> extractListRelatedException(List<Vocabulary> lv, Decor dec) {
		Set<ValueSetConcept> res = new TreeSet<>(new ValueSetConceptComparator());
		if (lv != null && dec != null) {
			for (Vocabulary vocabulary : lv) {
				if (vocabulary.getValueSet() != null) {
					String vsid = getRealValueSetOid(vocabulary.getValueSet(), dec);
					ValueSet vsc = TerminologyUtil.getValueSetById(dec.getTerminology(), vsid);
					if (vsc != null && vsc.getConceptList() != null) {
						for (ValueSetConcept excep : vsc.getConceptList().getException()) {
							if (UMLLoader.checkIfEnumerationContainsValue("NullFlavor", excep.getCode())) {
								if (!ValueSetConceptUtil.listValueSetConceptContainCode(excep.getCode(), res)) {
									res.add(excep);
								}
							}
							else {
								log.error("there are a problem with the use of an exception :" + excep.getCode() + 
										" : the NullFlavor enumeration does not contain such value");
							}
						}
					}
				}
			}
		}
		return res;
	}
	
	public static boolean vocabularyTrimmedCodeIsNotEmpty(Vocabulary voc) {
		return voc != null && voc.getCode() != null && !"".equals(voc.getCode().trim());
	}
	
}
