package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class IncludeDefinitionUtil {
	
	private IncludeDefinitionUtil() {}

	public static String getReferenceTemplateIdentifier(IncludeDefinition includeDefinition, Rules rules){
		if (includeDefinition != null && includeDefinition.getRef() != null && rules != null){
			for (TemplateDefinition td : RulesUtil.getTemplates(rules)) {
				if (td.getId().equals(includeDefinition.getRef()) || td.getName().equals(includeDefinition.getRef())){
					return td.getId();
				}
			}
		}
		return null;
	}

}
