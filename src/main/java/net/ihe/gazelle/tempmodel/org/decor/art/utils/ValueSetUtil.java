package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;

public final class ValueSetUtil {
	
	private ValueSetUtil() {}
	
	public static List<ValueSet> extractListCompatibleValueSet(String id, List<ValueSet> listOriginal) {
		List<ValueSet> res = new ArrayList<>();
		if (listOriginal != null) {
			for (ValueSet valueSet : listOriginal) {
				if (valueSet.getId().equals(id)) {
					res.add(valueSet);
				}
			}
		}
		return res;
	}

}
