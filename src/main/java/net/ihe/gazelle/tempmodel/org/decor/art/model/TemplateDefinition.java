//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.09 at 11:11:31 AM CEST 
//


package net.ihe.gazelle.tempmodel.org.decor.art.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempmodel.org.decor.art.behavior.HasItem;
import net.ihe.gazelle.tempmodel.org.decor.art.behavior.HasParent;


/**
 * <p>Java class for TemplateDefinition complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TemplateDefinition"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="desc" type="{}FreeFormMarkupWithLanguage" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="classification" type="{}TemplateProperties" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="relationship" type="{}TemplateRelationships" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{}context" minOccurs="0"/&gt;
 *         &lt;element ref="{}item" minOccurs="0"/&gt;
 *         &lt;element ref="{}example" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="inherit" type="{}InheritDefinition" maxOccurs="0" minOccurs="0"/&gt;
 *         &lt;element name="publishingAuthority" type="{}AuthorityType" maxOccurs="0" minOccurs="0"/&gt;
 *         &lt;element name="endorsingAuthority" type="{}AuthorityType" maxOccurs="0" minOccurs="0"/&gt;
 *         &lt;element name="revisionHistory" type="{}ObjectHistory" maxOccurs="0" minOccurs="0"/&gt;
 *         &lt;choice maxOccurs="unbounded"&gt;
 *           &lt;element ref="{}attribute" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element name="choice" type="{}ChoiceDefinition" maxOccurs="unbounded"/&gt;
 *           &lt;element name="element" type="{}RuleDefinition" maxOccurs="unbounded"/&gt;
 *           &lt;element name="include" type="{}IncludeDefinition" maxOccurs="unbounded"/&gt;
 *           &lt;element ref="{}let" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element ref="{}assert" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element ref="{}report" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element ref="{}defineVariable" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element name="constraint" type="{}FreeFormMarkupWithLanguage" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attGroup ref="{}VersionHandlingEffectiveDateOptional"/&gt;
 *       &lt;attGroup ref="{}TemplateCompilationAttributes"/&gt;
 *       &lt;attribute name="id" type="{}Oid" /&gt;
 *       &lt;attribute name="ref" type="{}Oid" /&gt;
 *       &lt;attribute name="name" use="required" type="{}ShortFormalName" /&gt;
 *       &lt;attribute name="statusCode" type="{}TemplateStatusCodeLifeCycle" /&gt;
 *       &lt;attribute ref="{}displayName"/&gt;
 *       &lt;attribute name="isClosed" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TemplateDefinition", propOrder = {
    "desc",
    "classification",
    "relationship",
    "context",
    "item",
    "example",
    "attributeOrChoiceOrElement"
})
public class TemplateDefinition implements HasItem,HasParent {

    private List<FreeFormMarkupWithLanguage> desc;
    private List<TemplateProperties> classification;
    private List<TemplateRelationships> relationship;
    private Context context;
    private Item item;
    private List<Example> example;
    @XmlElements({
        @XmlElement(name = "attribute", type = Attribute.class),
        @XmlElement(name = "choice", type = ChoiceDefinition.class),
        @XmlElement(name = "element", type = RuleDefinition.class),
        @XmlElement(name = "include", type = IncludeDefinition.class),
        @XmlElement(name = "let", type = Let.class),
        @XmlElement(name = "assert", type = Assert.class),
        @XmlElement(name = "report", type = Report.class),
        @XmlElement(name = "defineVariable", type = DefineVariable.class),
        @XmlElement(name = "constraint", type = FreeFormMarkupWithLanguage.class),
        @XmlElement(name = "contain", type = ContainDefinition.class)
    })
    private List<Object> attributeOrChoiceOrElement;
    @XmlAttribute(name = "id")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    private String id;
    @XmlAttribute(name = "ref")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    private String ref;
    @XmlAttribute(name = "name", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    private String name;
    @XmlAttribute(name = "statusCode")
    private TemplateStatusCodeLifeCycle statusCode;
    @XmlAttribute(name = "displayName")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    private String displayName;
    @XmlAttribute(name = "isClosed")
    private Boolean isClosed;
    @XmlAttribute(name = "effectiveDate")
    private XMLGregorianCalendar effectiveDate;
    @XmlAttribute(name = "expirationDate")
    private XMLGregorianCalendar expirationDate;
    @XmlAttribute(name = "officialReleaseDate")
    private XMLGregorianCalendar officialReleaseDate;
    @XmlAttribute(name = "versionLabel")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    private String versionLabel;
    @XmlAttribute(name = "url")
    @XmlSchemaType(name = "anyURI")
    private String url;
    @XmlAttribute(name = "ident")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    private String ident;
    @XmlAttribute(name = "referencedFrom")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    private String referencedFrom;
    
    @XmlTransient
	private Rules parentObject;

    @SuppressWarnings("unchecked")
	public Rules getParentObject() {
		return parentObject;
	}

	public void setParentObject(Rules parentObject) {
		this.parentObject = parentObject;
	}
	
	void afterUnmarshal( javax.xml.bind.Unmarshaller u, Object parent ){
		this.parentObject = (Rules)parent;
	}

	/**
     * Gets the value of the desc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the desc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDesc().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FreeFormMarkupWithLanguage }
     * 
     * 
     */
    public List<FreeFormMarkupWithLanguage> getDesc() {
        if (desc == null) {
            desc = new ArrayList<>();
        }
        return this.desc;
    }

    /**
     * Gets the value of the classification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the classification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TemplateProperties }
     * 
     * 
     */
    public List<TemplateProperties> getClassification() {
        if (classification == null) {
            classification = new ArrayList<>();
        }
        return this.classification;
    }

    /**
     * Gets the value of the relationship property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relationship property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelationship().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TemplateRelationships }
     * 
     * 
     */
    public List<TemplateRelationships> getRelationship() {
        if (relationship == null) {
            relationship = new ArrayList<>();
        }
        return this.relationship;
    }

    /**
     * Gets the value of the context property.
     * 
     * @return
     *     possible object is
     *     {@link Context }
     *     
     */
    public Context getContext() {
        return context;
    }

    /**
     * Sets the value of the context property.
     * 
     * @param value
     *     allowed object is
     *     {@link Context }
     *     
     */
    public void setContext(Context value) {
        this.context = value;
    }

    /**
     * Gets the value of the item property.
     * 
     * @return
     *     possible object is
     *     {@link Item }
     *     
     */
    public Item getItem() {
        return item;
    }

    /**
     * Sets the value of the item property.
     * 
     * @param value
     *     allowed object is
     *     {@link Item }
     *     
     */
    public void setItem(Item value) {
        this.item = value;
    }

    /**
     * Gets the value of the example property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the example property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExample().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Example }
     * 
     * 
     */
    public List<Example> getExample() {
        if (example == null) {
            example = new ArrayList<>();
        }
        return this.example;
    }

    /**
     * Gets the value of the attributeOrChoiceOrElement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributeOrChoiceOrElement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributeOrChoiceOrElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Attribute }
     * {@link ChoiceDefinition }
     * {@link RuleDefinition }
     * {@link IncludeDefinition }
     * {@link Let }
     * {@link Assert }
     * {@link Report }
     * {@link DefineVariable }
     * {@link FreeFormMarkupWithLanguage }
     * 
     * 
     */
    public List<Object> getAttributeOrChoiceOrElement() {
        if (attributeOrChoiceOrElement == null) {
            attributeOrChoiceOrElement = new ArrayList<>();
        }
        return this.attributeOrChoiceOrElement;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the ref property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRef() {
        return ref;
    }

    /**
     * Sets the value of the ref property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRef(String value) {
        this.ref = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link TemplateStatusCodeLifeCycle }
     *     
     */
    public TemplateStatusCodeLifeCycle getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TemplateStatusCodeLifeCycle }
     *     
     */
    public void setStatusCode(TemplateStatusCodeLifeCycle value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the displayName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Sets the value of the displayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayName(String value) {
        this.displayName = value;
    }

    /**
     * Gets the value of the isClosed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public boolean isIsClosed() {
        if (isClosed == null) {
            return false;
        } else {
            return isClosed;
        }
    }

    /**
     * Sets the value of the isClosed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsClosed(Boolean value) {
        this.isClosed = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the officialReleaseDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOfficialReleaseDate() {
        return officialReleaseDate;
    }

    /**
     * Sets the value of the officialReleaseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOfficialReleaseDate(XMLGregorianCalendar value) {
        this.officialReleaseDate = value;
    }

    /**
     * Gets the value of the versionLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersionLabel() {
        return versionLabel;
    }

    /**
     * Sets the value of the versionLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersionLabel(String value) {
        this.versionLabel = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrl(String value) {
        this.url = value;
    }

    /**
     * Gets the value of the ident property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdent() {
        return ident;
    }

    /**
     * Sets the value of the ident property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdent(String value) {
        this.ident = value;
    }

    /**
     * Gets the value of the referencedFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferencedFrom() {
        return referencedFrom;
    }

    /**
     * Sets the value of the referencedFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferencedFrom(String value) {
        this.referencedFrom = value;
    }

}
