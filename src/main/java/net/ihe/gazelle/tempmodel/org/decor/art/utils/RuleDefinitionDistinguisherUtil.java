package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.behavior.HasParent;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author Abderrazek Boufahja
 * the class has not private constructor because it is extended by a testing class
 * Brodhers = Brothers = siblings
 */
public class RuleDefinitionDistinguisherUtil {

    private static final Logger LOG = LoggerFactory.getLogger(RuleDefinitionDistinguisherUtil.class);

    public static Pair<String, String> extractUniqueDistinguisher(RuleDefinition ruleDefinition) {
        if (ruleDefinition != null) {
            if (ruleDefinition.getHasDistinguisher() != null && !ruleDefinition.getHasDistinguisher()) {
                return null;
            }
            if (ruleDefinition.getUniqueDistinguisher() != null) {
                return ruleDefinition.getUniqueDistinguisher();
            }
            List<Pair<String, String>> distinguisherValues = RuleDefinitionUtil.extractValuesDistinguishersFromRuleDefinition(ruleDefinition);
            if (distinguisherValues == null || distinguisherValues.isEmpty()) {
                ruleDefinition.setHasDistinguisher(false);
                return null;
            }
            List<RuleDefinition> listBrodhers = extractListBrodhers(ruleDefinition, RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition));
            Map<Pair<String, String>, Integer> mapExistingBroDistinguishers = new LinkedHashMap<>();
            for (Pair<String, String> distinguisherValue : distinguisherValues) {
                mapExistingBroDistinguishers.put(distinguisherValue, 0);
            }
            if (listBrodhers != null) {
                for (RuleDefinition ruleDefinition2 : listBrodhers) {
                    List<Pair<String, String>> distinguisherValues2 =
                            RuleDefinitionUtil.extractValuesDistinguishersFromRuleDefinition(ruleDefinition2);
                    if (distinguisherValues2 != null) {
                        for (Pair<String, String> pair : distinguisherValues2) {
                            if (mapExistingBroDistinguishers.containsKey(pair)) {
                                mapExistingBroDistinguishers.put(pair, mapExistingBroDistinguishers.get(pair) + 1);
                            }
                        }
                    }
                }
            }
            Pair<String, String> dist = extractAUniqueDistinguisherFromMap(mapExistingBroDistinguishers);
            ruleDefinition.setUniqueDistinguisher(dist);
            return dist;
        }
        return null;
    }

    public static Map<RuleDefinition, Pair<String, String>> extractListDistinguishersForBrodhers(RuleDefinition ruleDefinition) {
        if (ruleDefinition != null) {
            Map<RuleDefinition, Pair<String, String>> res = new LinkedHashMap<>();
            List<RuleDefinition> listBrodhers = extractListBrodhers(ruleDefinition, RuleDefinitionUtil.getRealNameOfRuleDefinition(ruleDefinition));
            if (listBrodhers != null) {
                for (RuleDefinition ruleDefinition2 : listBrodhers) {
                    Pair<String, String> disting = extractUniqueDistinguisher(ruleDefinition2);
                    res.put(ruleDefinition2, disting);
                }
            }
            return res;
        }
        return null;
    }

    protected static Pair<String, String> extractAUniqueDistinguisherFromMap(Map<Pair<String, String>, Integer> mapExistingBroDistinguishers) {
        if (mapExistingBroDistinguishers != null) {
            for (Entry<Pair<String, String>, Integer> entry : mapExistingBroDistinguishers.entrySet()) {
                if (entry.getValue() == 1) {
                    return entry.getKey();
                }
            }
        }
        return null;
    }

    protected static List<RuleDefinition> extractListBrodhers(HasParent hasParent, String ruleName) {
        List<RuleDefinition> listBrodhers = new ArrayList<>();
        if (hasParent == null) {
            return listBrodhers;
        }
        Object parent = hasParent.getParentObject();
        if (parent instanceof TemplateDefinition) {
            listBrodhers = TemplateDefinitionUtil.getElementsByName((TemplateDefinition) parent, ruleName);
        } else if (parent instanceof RuleDefinition) {
            listBrodhers = RuleDefinitionUtil.getElementsByName((RuleDefinition) parent, ruleName);
        } else if (parent instanceof ChoiceDefinition) {
            listBrodhers = extractListBrodhers((ChoiceDefinition) parent, ruleName);
        } else if (parent instanceof ContainDefinition) {
            listBrodhers = extractListBrodhers((ContainDefinition) parent, ruleName);
        } else {
            LOG.error("extractListBrodhers : parent is not instance of TemplateDefinition or RuleDefinition or ChoiceDefinition or " +
                    "ContainDefinition");
        }
        return listBrodhers;
    }

    public static DParent generateDistinguisherForRuleDefinition(RuleDefinition ruleDefinition) {
        Pair<String, String> pair = RuleDefinitionDistinguisherUtil.extractUniqueDistinguisher(ruleDefinition);
        if (pair != null && pair.getObject1() != null && pair.getObject2() != null) {
            DParent dElement = DPathExtractor.extractDElementFromDPath(pair.getObject1());
            String valueDPath = pair.getObject2();
            if (valueDPath != null) {
                dElement.getTail().setValue(valueDPath);
                return dElement;
            }
        }
        return null;
    }

}
