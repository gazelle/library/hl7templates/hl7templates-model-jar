package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.validator.routines.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class DecorMarshaller {
	
	
	private static Logger log = LoggerFactory.getLogger(DecorMarshaller.class);
	
	private DecorMarshaller() {}
	
	public static Decor loadDecor(InputStream is) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(Decor.class);
		Unmarshaller u = jc.createUnmarshaller();
		Decor res = (Decor) u.unmarshal(is);
		updateDecorContent(res);
		return res;
	}
	
	public static Decor loadDecor(String path) {
		log.info("--decor path : {}", path);
		InputStream is = null;
		try {
			if (UrlValidator.getInstance().isValid(path)) {
				URL url;
				
					url = new URL(path);
				
				is = url.openStream();
			}
			else {
				is = new FileInputStream(path);
			}
			return loadDecor(is);
		} catch (IOException | JAXBException e) {
			log.error("error to loard url : " + path, e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					log.error("error to close stream related to path : " + path, e);
				}
			}
		}
		return null;
	}
	
	private static void updateDecorContent(Decor res) {
		if (res != null) {
			res.setDatasets(null);
			res.setIds(null);
			res.setIssues(null);
			res.setScenarios(null);
		}
	}

	public static void marshallDecor(Decor decor, OutputStream os) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(Decor.class);
		Marshaller mar = jc.createMarshaller();
		mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		mar.marshal(decor, os);
	}
	
}
