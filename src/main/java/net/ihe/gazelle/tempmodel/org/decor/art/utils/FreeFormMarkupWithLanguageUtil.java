package net.ihe.gazelle.tempmodel.org.decor.art.utils;

import java.io.ByteArrayOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class FreeFormMarkupWithLanguageUtil {
	
	
	private static final Logger log = LoggerFactory.getLogger(FreeFormMarkupWithLanguageUtil.class);
	
	private static  final Pattern PAT = Pattern.compile("<FreeFormMarkupWithLanguage.*?>(.*)<\\/FreeFormMarkupWithLanguage>", Pattern.DOTALL|Pattern.MULTILINE);
	
	private FreeFormMarkupWithLanguageUtil() {}
	
	private static String extractContentAsHTML(FreeFormMarkupWithLanguage vmc) throws JAXBException{
		if (vmc == null || vmc.getContent().isEmpty()){
			return null;
		}
		JAXBContext jc = JAXBContext.newInstance(FreeFormMarkupWithLanguage.class);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		jc.createMarshaller().marshal(vmc, baos);
		String out = baos.toString();
		Matcher mat = PAT.matcher(out);
		if (mat.find()){
			return StringUtils.trimToEmpty(mat.group(1).trim());
		}
		return out;
	}
	
	public static String extractContentAsString(FreeFormMarkupWithLanguage vmc){
		try {
			String html = extractContentAsHTML(vmc);
			if (html != null) {
				return HTMLUtils.extractText(html.getBytes());
			}
		}
		catch(Exception e){
			log.info("error to parse freeFormMarkup", e);
		}
		return null;
	}

}
