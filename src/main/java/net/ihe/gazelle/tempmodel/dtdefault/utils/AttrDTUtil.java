package net.ihe.gazelle.tempmodel.dtdefault.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempmodel.dtdefault.model.AttrDT;
import net.ihe.gazelle.tempmodel.dtdefault.model.AttrDTs;
import net.ihe.gazelle.tempmodel.dtdefault.model.PE;

/**
 * 
 * @author Abderrazek Boufahja
 * the class has not private constructor because it is extended by a testing class
 *
 */
public class AttrDTUtil {
	
	private static Logger log = LoggerFactory.getLogger(AttrDTUtil.class);
	
	private static AttrDTs attrDTs = null;
	static {
		try {
			String pathAttrDTs = System.getProperty("HL7TEMP_RESOURCES_PATH", 
					"/home/aboufahj/workspaceTopcased/hl7templates-resources/") + 
					File.separator + 
					System.getProperty("HL7TEMP_CDACONFFOLDERNAME", "cdabasic") + 
					"/attrDTs.xml";
			attrDTs = AttrDTLoader.loadDT(new FileInputStream(pathAttrDTs));
		} catch (FileNotFoundException e) {
			log.info("attrDTs.xml not found", e);
		} catch (JAXBException e) {
			log.info("attrDTs.xml not well formated", e);
		}
	}
	

	public static String getOriginalDTOfType(String type, String attrName) {
		PE pe = findPEByTypeName(type);
		if (pe != null){
			for (AttrDT attrDT : pe.getAttrDT()) {
				if (attrDT.getAttrName().equals(attrName)){
					return attrDT.getDefaultDT().value();
				}
			}
			if (pe.getRef() != null) {
				return getOriginalDTOfType(pe.getRef(), attrName);
			}
		}
		return null;
	}


	protected static PE findPEByTypeName(String type) {
		if (attrDTs != null) {
			for (PE pe : attrDTs.getPE()) {
				if (pe.getName().equals(type)){
					return pe;
				}
			}
		}
		return null;
	}

}
