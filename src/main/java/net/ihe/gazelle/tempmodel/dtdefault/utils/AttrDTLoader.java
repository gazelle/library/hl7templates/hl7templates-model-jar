package net.ihe.gazelle.tempmodel.dtdefault.utils;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.tempmodel.dtdefault.model.AttrDTs;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public final class AttrDTLoader {
	
	private AttrDTLoader() {}
	
	public static AttrDTs loadDT(InputStream is) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(AttrDTs.class);
		Unmarshaller u = jc.createUnmarshaller();
		return (AttrDTs) u.unmarshal(is);
	}

}
