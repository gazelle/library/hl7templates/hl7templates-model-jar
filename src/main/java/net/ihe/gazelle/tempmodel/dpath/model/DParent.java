package net.ihe.gazelle.tempmodel.dpath.model;

public abstract class DParent {
	
	private String name;
	
	private String value;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public int size(){
		if (this instanceof DAttribute){
			return 1;
		}
		else {
			DElement del = (DElement)this;
			if (del.getFollowingAttributeOrElement() != null){
				return del.getFollowingAttributeOrElement().size() + 1;
			}
			return 1;
		}
	}
	
	public DParent get(int index){
		int i = 0;
		DParent pointer = this;
		while (i <= index){
			if (i == index) {
				return pointer;
			}
			if (pointer instanceof DElement){
				pointer = ((DElement)pointer).getFollowingAttributeOrElement();
			}
			else {
				return null;
			}
			i++;
		}
		return null;
	}
	
	public DParent getTail(){
		if (this instanceof DElement && (((DElement)this).getFollowingAttributeOrElement() != null)){
			return ((DElement)this).getFollowingAttributeOrElement().getTail();
		}
		return this;
	}
	
}
