package net.ihe.gazelle.tempmodel.location.util;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public final class ChoiceDefinitionLocation {
	
	private ChoiceDefinitionLocation() {}
	
	public static String getLocation(ChoiceDefinition choiceDefinition) {
		if (choiceDefinition != null) {
			if (choiceDefinition.getParentObject() != null) {
				if (choiceDefinition.getParentObject() instanceof RuleDefinition) {
					RuleDefinition parent = (RuleDefinition)choiceDefinition.getParentObject();
					String locationParent = RuleDefinitionLocation.getLocation(parent);
					return locationParent + "/choice[" + 
							(RuleDefinitionUtil.getChoices(parent).indexOf(choiceDefinition) + 1) + "]";
				}
				else if(choiceDefinition.getParentObject() instanceof TemplateDefinition) {
					TemplateDefinition parent = (TemplateDefinition)choiceDefinition.getParentObject();
					String locationParent = TemplateDefinitionLocation.getLocation(parent);
					return locationParent + "/choice[" + 
							(TemplateDefinitionUtil.getChoices(parent).indexOf(choiceDefinition) + 1) + "]";
				}
			}
			else {
				return "/choice";
			}
		}
		return "";
	}

}
