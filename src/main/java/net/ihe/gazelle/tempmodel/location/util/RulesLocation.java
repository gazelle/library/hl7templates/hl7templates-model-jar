package net.ihe.gazelle.tempmodel.location.util;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;

public final class RulesLocation {
	
	private static final String RULES2 = "//rules";

	private RulesLocation() {}
	
	public static String getLocation(Rules rules) {
		return RULES2;
	}

}
