package net.ihe.gazelle.tempmodel.location.util;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public final class IncludeDefinitionLocation {
	
	private static final String INCLODE_END = "]";
	private static final String INCLUDE_OPEN = "/include[";

	private IncludeDefinitionLocation() {}
	
	public static String getLocation(IncludeDefinition includeDefinition) {
		if (includeDefinition != null) {
			if (includeDefinition.getParentObject() != null) {
				if (includeDefinition.getParentObject() instanceof RuleDefinition) {
					RuleDefinition parent = (RuleDefinition)includeDefinition.getParentObject();
					String locationParent = RuleDefinitionLocation.getLocation(parent);
					return locationParent + INCLUDE_OPEN + 
							(RuleDefinitionUtil.getIncludes(parent).indexOf(includeDefinition) + 1) + INCLODE_END;
				}
				else if(includeDefinition.getParentObject() instanceof TemplateDefinition) {
					TemplateDefinition parent = (TemplateDefinition)includeDefinition.getParentObject();
					String locationParent = TemplateDefinitionLocation.getLocation(parent);
					return locationParent + INCLUDE_OPEN + 
							(TemplateDefinitionUtil.getIncludes(parent).indexOf(includeDefinition) + 1) + INCLODE_END;
				}
				else if(includeDefinition.getParentObject() instanceof ChoiceDefinition) {
					ChoiceDefinition parent = (ChoiceDefinition)includeDefinition.getParentObject();
					String locationParent = ChoiceDefinitionLocation.getLocation(parent);
					return locationParent + INCLUDE_OPEN + 
							(ChoiceDefinitionUtil.getIncludes(parent).indexOf(includeDefinition) + 1) + INCLODE_END;
				}
			}
			else {
				return "/include";
			}
		}
		return "";
	}

}
