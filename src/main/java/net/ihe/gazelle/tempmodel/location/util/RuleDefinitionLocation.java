package net.ihe.gazelle.tempmodel.location.util;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public final class RuleDefinitionLocation {
	
	private static final String ELEMENT_END = "]";
	private static final String ELEMENT_DESC = "/element[";

	private RuleDefinitionLocation() {}
	
	public static String getLocation(RuleDefinition ruleDefinition) {
		if (ruleDefinition != null) {
			if (ruleDefinition.getParentObject() != null) {
				if (ruleDefinition.getParentObject() instanceof RuleDefinition) {
					RuleDefinition parent = (RuleDefinition)ruleDefinition.getParentObject();
					String locationParent = RuleDefinitionLocation.getLocation(parent);
					return locationParent + ELEMENT_DESC + 
							(RuleDefinitionUtil.getElements(parent).indexOf(ruleDefinition) + 1) + ELEMENT_END;
				}
				else if(ruleDefinition.getParentObject() instanceof ChoiceDefinition) {
					ChoiceDefinition parent = (ChoiceDefinition)ruleDefinition.getParentObject();
					String locationParent = ChoiceDefinitionLocation.getLocation(parent);
					return locationParent + ELEMENT_DESC + 
							(ChoiceDefinitionUtil.getElements(parent).indexOf(ruleDefinition) + 1) + ELEMENT_END;
				}
				else if(ruleDefinition.getParentObject() instanceof TemplateDefinition) {
					TemplateDefinition parent = (TemplateDefinition)ruleDefinition.getParentObject();
					String locationParent = TemplateDefinitionLocation.getLocation(parent);
					return locationParent + ELEMENT_DESC + 
							(TemplateDefinitionUtil.getElements(parent).indexOf(ruleDefinition) + 1) + ELEMENT_END;
				}
			}
			else {
				return "/element";
			}
		}
		return "";
	}

}
