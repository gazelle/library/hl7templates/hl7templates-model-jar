package net.ihe.gazelle.tempmodel.location.util;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public final class AttributeLocation {
	
	private AttributeLocation() {}
	
	public static String getLocation(Attribute attr) {
		if (attr != null) {
			if (attr.getParentObject() instanceof RuleDefinition) {
				RuleDefinition parent = (RuleDefinition)attr.getParentObject();
				String locationParent = RuleDefinitionLocation.getLocation(parent);
				return locationParent + "/attribute[" + 
						(parent.getAttribute().indexOf(attr) + 1) + "]";
			} else if (attr.getParentObject() instanceof TemplateDefinition) {
				TemplateDefinition parent = (TemplateDefinition)attr.getParentObject();
				String locationParent = TemplateDefinitionLocation.getLocation(parent);
				return locationParent + "/attribute[" + 
						(TemplateDefinitionUtil.getAttributes(parent).indexOf(attr) + 1) + "]";
			} else {
				return "/attribute";
			}
		}
		return "";
	}

}
