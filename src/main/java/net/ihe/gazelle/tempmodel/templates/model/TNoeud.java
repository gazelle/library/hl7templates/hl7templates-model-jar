package net.ihe.gazelle.tempmodel.templates.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Used to extract the template identifier
 * @author Abderrazek Boufahja
 *
 */
public class TNoeud {
	
	private List<String> listTemplateId;
	
	private String identifier;
	
	private List<TNoeud> listParents;

	public List<String> getListTemplateId() {
		if (listTemplateId == null) {
			listTemplateId = new ArrayList<>();
		}
		return listTemplateId;
	}

	public void setListTemplateId(List<String> listTemplateId) {
		this.listTemplateId = listTemplateId;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public List<TNoeud> getListParents() {
		if (listParents == null) {
			listParents = new ArrayList<>();
		}
		return listParents;
	}

	public void setListParents(List<TNoeud> listParents) {
		this.listParents = listParents;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((identifier == null) ? 0 : identifier.hashCode());
		result = prime * result
				+ ((listParents == null) ? 0 : listParents.hashCode());
		result = prime * result
				+ ((listTemplateId == null) ? 0 : listTemplateId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TNoeud other = (TNoeud) obj;
		if (identifier == null) {
			if (other.identifier != null) {
				return false;
			}
		} else if (!identifier.equals(other.identifier)) {
			return false;
		}
		if (listParents == null) {
			if (other.listParents != null) {
				return false;
			}
		} else if (!listParents.equals(other.listParents)) {
			return false;
		}
		if (listTemplateId == null) {
			if (other.listTemplateId != null) {
				return false;
			}
		} else if (!listTemplateId.equals(other.listTemplateId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		if (listTemplateId != null) {
			Collections.sort(listTemplateId);
		}
		return "TNoeud [listTemplateId=" + listTemplateId + ", identifier="
				+ identifier + ", listParents=" + listParents + "]";
	}
	
	
	
}
