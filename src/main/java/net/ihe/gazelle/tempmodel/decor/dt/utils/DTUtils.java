package net.ihe.gazelle.tempmodel.decor.dt.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.goc.uml.utils.UMLLoader;
import net.ihe.gazelle.tempmodel.decor.dt.model.Datatype;
import net.ihe.gazelle.tempmodel.decor.dt.model.Datatypes;

/**
 *
 * @author Abderrazek Boufahja
 *
 */
public final class DTUtils {
	
	private DTUtils() {
		// private constructor
	}
	
	
	private static Logger log = LoggerFactory.getLogger(DTUtils.class);
	
	private static Datatypes listDT = null;
	
	private static List<String> listDTString = new ArrayList<>();
	
	static {
		try {
			String pathDatatypes = System.getProperty("HL7TEMP_RESOURCES_PATH",
					"/home/aboufahj/workspaceTopcased/hl7templates-resources/") +
					File.separator +
					System.getProperty("HL7TEMP_CDACONFFOLDERNAME", "cdabasic") +
					"/datatypes.xml";
			listDT = DTLoader.loadDT(new FileInputStream(pathDatatypes));
			for (Datatype dt : listDT.getDatatype()) {
				listDTString.add(dt.getValue());
			}
		} catch (FileNotFoundException e) {
			log.info("datatypes.xml not found", e);
		} catch (JAXBException e) {
			log.info("datatypes.xml not well formed", e);
		}
	}
	
	public static boolean datatypesContainDT(String dt){
		if (dt == null) {
			return false;
		}
		String realDT = dt.split("\\.")[0];
		return listDTString.contains(realDT);
	}
	
	public static String getUMLDatatype(String dataType){
		String dt = getRealDatatype(dataType);
		if (dt != null) {
			dt = dt.replace("_", "");
		}
		return dt;
	}
	
	public static String getRealDatatype(String dt){
		if (dt == null) {
			return null;
		}
		Datatype dtype = findDatatypeByValue(dt);
		if (dtype == null) {
			return null;
		}
		return getRealDatatype(dtype);
	}
	
	private static Datatype findDatatypeByValue(String value){
		for (Datatype dt : listDT.getDatatype()) {
			if (dt.getValue().equals(value)) {
				return dt;
			}
		}
		return null;
	}
	
	public static String getRealDatatype(Datatype datatype){
		if (datatype == null) {
			return null;
		}
		if (datatype.getRef() != null) {
			return datatype.getRef();
		}
		if (datatype.getValue() != null) {
			return datatype.getValue().split("\\.")[0];
		}
		return null;
	}
	
	public static boolean constrainedDTIsAnExtension(String dt, String typeRD) {
		String realDTConstrained = DTUtils.getRealDatatype(dt);
		if (realDTConstrained != null) {
			realDTConstrained = realDTConstrained.replace("_", "");
		}
		else {
			return false;
		}
		return  !realDTConstrained.equals(typeRD) && UMLLoader.verifyTheParentOfType1IsType2(realDTConstrained, typeRD);
	}

	/**
	 * example : CE.EPSOS -&gt; true
	 * CD -&gt; false
	 * @param dt
	 * @return boolean
	 */
	public static boolean ignoreDTExtensionError(String dt) {
		if (dt == null) {
			return false;
		}
		for (Datatype dtorig : listDT.getDatatype()) {
			if (dtorig.getValue().equals(dt)) {
				return dtorig.getIgnore();
			}
		}
		return false;
	}

}
