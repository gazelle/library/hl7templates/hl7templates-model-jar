package net.ihe.gazelle.tempmodel.decor.dt.utils;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.tempmodel.decor.dt.model.Datatypes;

/**
 *
 * @author Abderrazek Boufahja
 *
 */
public final class DTLoader {

	private DTLoader() {}

	public static Datatypes loadDT(InputStream is) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(Datatypes.class);
		Unmarshaller u = jc.createUnmarshaller();
		return (Datatypes) u.unmarshal(is);
	}

}
