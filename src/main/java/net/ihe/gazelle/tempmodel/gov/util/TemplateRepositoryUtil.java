package net.ihe.gazelle.tempmodel.gov.util;

import net.ihe.gazelle.tempmodel.gov.model.TemplateRepository;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BuildingBlockRepository;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Abderrazek Boufahja
 * the class has not private constructor because it is extended by a testing class
 *
 */
public class TemplateRepositoryUtil {
	
	
	private static Logger log = LoggerFactory.getLogger(TemplateRepositoryUtil.class);
	
	private static Map<String, TemplateRepository> mapTemplateRepository = new HashMap<>();
	
	public static TemplateRepository extractTemplateRepositoryIfAlreadyProcessed(Decor decor){
		if (decor == null) {
			return null;
		}
		for (TemplateRepository tr : mapTemplateRepository.values()) {
			if (tr!= null && tr.getCurrentDecor() != null && tr.getCurrentDecor().equals(decor)){
				return tr;
			}
		}
		return null;
	}
	
	protected static TemplateRepository generateTemplateRepositoryFromURL(String ref){
		if (mapTemplateRepository.containsKey(ref)){
			return mapTemplateRepository.get(ref);
		}
		else {
			log.info("Processing BBR : {}", ref);
			Decor decReference = DecorMarshaller.loadDecor(ref);
			TemplateRepository refTemplateRepository = generateTemplateRepository(decReference, ref);
			TemplateRepositoryUtil.mapTemplateRepository.put(ref, refTemplateRepository);
			log.info("Processed BBR : {}", ref);
			return refTemplateRepository;
		}
	}
	
	public static TemplateRepository generateTemplateRepository(Decor dec) {
		return generateTemplateRepository(dec, null);
	}
	
	public static TemplateRepository generateTemplateRepository(Decor dec, String refOriginalURL){
		if (dec == null) {
			return null;
		}
		TemplateRepository res = extractTemplateRepositoryIfAlreadyProcessed(dec);
		if (res != null){
			return res;
		}
		res = new TemplateRepository();
		res.setCurrentDecor(dec);
		if (dec.getProject() != null && dec.getProject().getPrefix() != null && refOriginalURL != null) {
			TemplateRepositoryUtil.mapTemplateRepository.put(refOriginalURL, res);
		}
		
		if (dec.getProject() != null && !dec.getProject().getBuildingBlockRepository().isEmpty()){
			for (BuildingBlockRepository bbr : dec.getProject().getBuildingBlockRepository()) {
				String ref = createUrlBBRExtraction(bbr);
				URL bbrUrl = null;
				try {
					bbrUrl = new URL(ref);
					if (!mapTemplateRepository.containsKey(ref) && !verifyUrlReachable(bbrUrl)){
						log.info("this url is not reachable : {}", ref);
						TemplateRepositoryUtil.mapTemplateRepository.put(ref, null);
					}
					else {
						TemplateRepository refTemplateRepository = generateTemplateRepositoryFromURL(ref);
						res.getListParentRepository().add(refTemplateRepository);
						
					}
				} catch (MalformedURLException e) {
					log.info("this BuildingBlockRepository is not a valid URL : " + ref);
				}
			}
		}
		return res;
	}

	protected static String createUrlBBRExtraction(BuildingBlockRepository bbr) {
		if (bbr == null || bbr.getUrl() == null || bbr.getIdent() == null) {
			return null;
		}
		String res = bbr.getUrl();
		if (!res.endsWith("/")) {
			res = res + "/";
		}
		return res + "RetrieveProject?prefix=" + bbr.getIdent() + "&mode=compiled&language=en-US&format=xml";
	}
	
	public static TemplateRepository getTemplateRepositoryByPrefix(TemplateRepository templateRepository, String prefix) {
		return getTemplateRepositoryByPrefix(templateRepository, prefix, null);
	}
	
	public static TemplateRepository getTemplateRepositoryByPrefix(TemplateRepository templateRepository, String prefix, List<String> listPrefixesParam) {
		List<String> listPrefixes = listPrefixesParam;
		if (listPrefixes == null) {
			listPrefixes = new ArrayList<>();
		}
		listPrefixes.add(templateRepository.getCurrentDecor().getProject().getPrefix());
		if (templateRepository!= null && templateRepository.getListParentRepository() != null) {
			for (TemplateRepository tr : templateRepository.getListParentRepository()) {
				if (TemplateRepositoryUtil.currentDecorContaintsProjectWithPrefix(tr) &&
						tr.getCurrentDecor().getProject().getPrefix().equals(prefix)){
					return tr;
				}
				else{
					if (tr != null && tr.getCurrentDecor() != null && tr.getCurrentDecor().getProject() != null && 
							!listPrefixes.contains(tr.getCurrentDecor().getProject().getPrefix())) { 
						TemplateRepository hmm = getTemplateRepositoryByPrefix(tr, prefix, listPrefixes);
						if (hmm != null){
							return hmm;
						}
					}
				}
			}
		}
		return null;
	}
	
	private static boolean currentDecorContaintsProjectWithPrefix(TemplateRepository templateRepository) {
		return templateRepository!= null && 
				templateRepository.getCurrentDecor() != null && 
				templateRepository.getCurrentDecor().getProject() != null && 
				templateRepository.getCurrentDecor().getProject().getPrefix() != null;
	}
	
	public static boolean verifyUrlReachable(URL url){
		try{
		    final URLConnection connection = url.openConnection();
		    connection.setConnectTimeout(5000);
		    connection.connect();
		    return true;
		} catch(IOException e){
		    return false;
		}
	}
	
	public static TemplateDefinition findTemplateDefinitionById(TemplateRepository templateRepository, String templateId) {
		return findTemplateDefinitionById(templateRepository, templateId, null);
	}

	private static TemplateDefinition findTemplateDefinitionById(TemplateRepository templateRepository, String templateId, 
			List<TemplateRepository> listProcessedTRParam) {
		List<TemplateRepository> listProcessedTR = listProcessedTRParam;
		if (listProcessedTR == null) {
			listProcessedTR = new ArrayList<>();
		}
		if (templateRepository != null && templateId != null && !listProcessedTR.contains(templateRepository)){
			TemplateDefinition td = RulesUtil.getTemplateDefinitionById(templateRepository.getCurrentDecor().getRules(), templateId);
			if (td != null){
				return td;
			}
			listProcessedTR.add(templateRepository);
			for (TemplateRepository refTR : templateRepository.getListParentRepository()) {
				td = findTemplateDefinitionById(refTR, templateId, listProcessedTR);
				if (td != null){
					return td;
				}
			}
			
		}
		return null;
	}
	
	public static void cleanMapTemplateRepository() {
		TemplateRepositoryUtil.mapTemplateRepository.clear();
	}
	
	public static String getReferenceTemplateIdentifierFromTemplateRepository(TemplateRepository tr, String ref) {
		return getReferenceTemplateIdentifierFromTemplateRepository(tr, ref, null);
	}

	private static String getReferenceTemplateIdentifierFromTemplateRepository(TemplateRepository tr, String ref, List<TemplateRepository> listProcessedTRParam) {
		List<TemplateRepository> listProcessedTR = listProcessedTRParam;
		if (listProcessedTR == null) {
			listProcessedTR = new ArrayList<>();
		}
		if (tr != null && tr.getCurrentDecor() != null && tr.getCurrentDecor().getRules() != null && !listProcessedTR.contains(tr)) {
			Rules rules = tr.getCurrentDecor().getRules();
			TemplateDefinition td = RulesUtil.getTemplateDefinitionByIdOrName(rules, ref);
			if (td != null) {
				return td.getId();
			}
			listProcessedTR.add(tr);
			for (TemplateRepository trParent : tr.getListParentRepository()) {
				String res = getReferenceTemplateIdentifierFromTemplateRepository(trParent, ref, listProcessedTR);
				if (res != null) {
					return res;
				}
			}
		}
		return null;
	}
	
	public static TemplateDefinition getTemplateDefinitionByIdentifier(TemplateRepository tr, String identifier) {
		if (tr != null) {
			if (tr.getCurrentDecor() != null && tr.getCurrentDecor().getRules() != null) {
				for (TemplateDefinition td : RulesUtil.getTemplates(tr.getCurrentDecor().getRules())) {
					if (td.getId()!= null && td.getId().equals(identifier)) {
						return td;
					}
				}
			}
			for (TemplateRepository trParent : tr.getListParentRepository()) {
				TemplateDefinition td = getTemplateDefinitionByIdentifier(trParent, identifier);
				if (td != null) {
					return td;
				}
			}
		}
		return null;
	}
	public static void extractListDecorFromTemplateRepository(TemplateRepository tr, List<Decor> ldec) {
		extractListDecorFromTemplateRepository(tr, ldec, null);
	}
	
	public static void extractListDecorFromTemplateRepository(TemplateRepository tr, List<Decor> ldec, List<TemplateRepository> listProcessedTRParam) {
		List<TemplateRepository> listProcessedTR = listProcessedTRParam;
		if (ldec == null) {
			return;
		}
		if (listProcessedTR == null) {
			listProcessedTR = new ArrayList<>();
		}
		if (tr != null && !DecorUtil.listDecorContainsDecorWithTheSamePrefix(ldec, tr.getCurrentDecor())) {
			ldec.add(tr.getCurrentDecor());
		}
		if (tr != null && tr.getListParentRepository() != null && !listProcessedTR.contains(tr)) {
			listProcessedTR.add(tr);
			for (TemplateRepository trp : tr.getListParentRepository()) {
				extractListDecorFromTemplateRepository(trp, ldec, listProcessedTR);
			}
		}
	}
 
}
