package net.ihe.gazelle.tempmodel.gov.model;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class TemplateRepository {
	
	private Decor currentDecor;
	
	private List<TemplateRepository> listParentRepository;

	public List<TemplateRepository> getListParentRepository() {
		if (listParentRepository == null){
			listParentRepository = new ArrayList<>();
		}
		return listParentRepository;
	}

	public void setListParentRepository(
			List<TemplateRepository> listParentRepository) {
		this.listParentRepository = listParentRepository;
	}

	public Decor getCurrentDecor() {
		return currentDecor;
	}

	public void setCurrentDecor(Decor currentDecor) {
		this.currentDecor = currentDecor;
	}
	
}
