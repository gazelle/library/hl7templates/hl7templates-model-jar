package net.ihe.gazelle.tempmodel.choices.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempmodel.choices.model.ChoiceDefault;
import net.ihe.gazelle.tempmodel.choices.model.ChoiceElement;
import net.ihe.gazelle.tempmodel.choices.model.ChoicesDefault;

/**
 *
 * @author Abderrazek Boufahja
 * the class has not private constructor because it is extended by a testing class
 *
 */
public class ChoiceDefaultUtil {
	
	
	private static Logger log = LoggerFactory.getLogger(ChoiceDefaultUtil.class);
	
	private static ChoicesDefault choicesDefault = null;
	
	static {
		try {
			String pathchoiceDefault = System.getProperty("HL7TEMP_RESOURCES_PATH", "/home/aboufahj/workspaceTopcased/hl7templates-resources/") +
					File.separator +
					System.getProperty("HL7TEMP_CDACONFFOLDERNAME", "cdabasic") +
					"/choicesDefault.xml";
			choicesDefault = ChoicesDefaultLoader.loadChoicesDefault(new FileInputStream(pathchoiceDefault));
		} catch (FileNotFoundException e) {
			log.error("file not found" , e);
		} catch (JAXBException e) {
			log.error("file not with valid structure", e);
		}
	}
	
	public static ChoiceDefault findChoiceDefaultByTypeAndSubelements(String type, List<String> listElementsName) {
		if (choicesDefault == null) {
			return null;
		}
		if (type == null || listElementsName == null || listElementsName.isEmpty()) {
			return null;
		}
		List<ChoiceDefault> listConcerned = new ArrayList<>();
		for (ChoiceDefault choiceDefault : choicesDefault.getChoiceDefault()) {
			if (choiceDefault.getType() != null && choiceDefault.getType().equals(type)) {
				listConcerned.add(choiceDefault);
			}
		}
		for (ChoiceDefault choiceDefault : listConcerned) {
			if (listSubElementsContainsAll(choiceDefault, listElementsName)) {
				return choiceDefault;
			}
		}
		return null;
	}

	protected static boolean listSubElementsContainsAll(ChoiceDefault choiceDefault, List<String> listElementsName) {
		boolean res = true;
		for (String string : listElementsName) {
			res = res && listSubElementsContains(choiceDefault, string);
		}
		return res;
	}
	
	protected static boolean listSubElementsContains(ChoiceDefault choiceDefault, String elementName) {
		for (ChoiceElement choiceElement : choiceDefault.getChoiceElement()) {
			if (choiceElement.getName() != null && choiceElement.getName().equals(elementName)) {
				return true;
			}
		}
		return false;
	}

}
