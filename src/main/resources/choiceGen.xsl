<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"
        omit-xml-declaration="yes"/>
    
    <xsl:template match="/">
        <choicesDefault>
        <xsl:apply-templates select="//xs:choice"/>
        </choicesDefault>
    </xsl:template>
    
    <xsl:template match="xs:choice">
        <choiceDefault>
            <xsl:attribute name="type">
                <xsl:value-of select="parent::node()/parent::node()/parent::node()/parent::node()/@name"/>
            </xsl:attribute>
            <xsl:attribute name="min">
                <xsl:choose>
                    <xsl:when test="@minOccurs">
                        <xsl:value-of select="@minOccurs"/>
                    </xsl:when>
                    <xsl:otherwise>1</xsl:otherwise>
                </xsl:choose>
                <xsl:if test="@minOccurs">
                    <xsl:value-of select="@minOccurs"/>
                </xsl:if>
            </xsl:attribute>
            <xsl:attribute name="max" xml:space="default">
                <xsl:choose>
                    <xsl:when test="@maxOccurs">
                        <xsl:if test="@maxOccurs='unbounded'">-1</xsl:if>
                        <xsl:if test="@maxOccurs!='unbounded'"><xsl:value-of select="@maxOccurs"/></xsl:if>
                    </xsl:when>
                    <xsl:otherwise>1</xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:for-each select="xs:element">
                <choiceElement>
                    <xsl:attribute name="name">
                        <xsl:value-of select="@name"/>
                    </xsl:attribute>
                    <xsl:if test="@minOccurs">
                        <xsl:attribute name="min"><xsl:value-of select="@minOccurs"/></xsl:attribute>
                    </xsl:if>
                    <xsl:if test="@maxOccurs">
                        <xsl:attribute name="max"><xsl:value-of select="@maxOccurs"/></xsl:attribute>
                    </xsl:if>
                </choiceElement>
            </xsl:for-each>
        </choiceDefault>
    </xsl:template>
</xsl:stylesheet>