<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"
        omit-xml-declaration="yes"/>
    
    <xsl:template match="/">
        <xsl:apply-templates select="supportedDataTypes"/>
    </xsl:template>
    
    <xsl:template match="supportedDataTypes">
        <xsl:for-each select="//dataType">
            <datatype>
            <xsl:value-of select="@name"/>
            </datatype>
        </xsl:for-each>
        <xsl:for-each select="//flavor">
            <datatype>
                <xsl:value-of select="@name"/>
            </datatype>
        </xsl:for-each>
    </xsl:template>
    
</xsl:stylesheet>