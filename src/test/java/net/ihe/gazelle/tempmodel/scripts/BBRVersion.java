package net.ihe.gazelle.tempmodel.scripts;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

public class BBRVersion {
	
	public static void main(String[] args) throws MalformedURLException, FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("/home/aboufahj/Téléchargements/ccdamu2-20170428T135220-en-US-decor-compiled.xml");
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(dec.getRules());
		List<TemplateDefinition> ltdvtoremove = new ArrayList<>();
		for (TemplateDefinition templateDefinition : ltd) {
			if ("2.1".equals(templateDefinition.getVersionLabel())) {
				ltdvtoremove.add(templateDefinition);
			}
		}
		dec.getRules().getTemplateAssociationOrTemplate().removeAll(ltd);
		ltd.removeAll(ltdvtoremove);
		Collections.sort(ltd, new ComparatorTemplates());
		dec.getRules().getTemplateAssociationOrTemplate().addAll(ltd);
		DecorMarshaller.marshallDecor(dec, new FileOutputStream("/home/aboufahj/Téléchargements/ccdamu2-20170428T135220-en-US-decor-compiled-cleaned.xml"));
	}
	
	public static void main1(String[] args) throws MalformedURLException, FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("/home/aboufahj/Téléchargements/ccda-20161222T145740-en-US-decor-compiled.xml");
		DecorMarshaller.marshallDecor(dec, new FileOutputStream("/home/aboufahj/Téléchargements/ccda-20161222T145740-en-US-decor-compiled-formatted.xml"));
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(dec.getRules());
		List<TemplateDefinition> ltdversion = new ArrayList<>();
		for (TemplateDefinition templateDefinition : ltd) {
			if ("1.1".equals(templateDefinition.getVersionLabel())) {
				ltdversion.add(templateDefinition);
			}
		}
		dec.getRules().getTemplateAssociationOrTemplate().removeAll(ltd);
		dec.getRules().getTemplateAssociationOrTemplate().addAll(ltdversion);
		DecorMarshaller.marshallDecor(dec, new FileOutputStream("/home/aboufahj/Téléchargements/ccda-20161222T145740-en-US-decor-compiled-1.1.xml"));
	}
	
	static class ComparatorTemplates implements Comparator<TemplateDefinition> {

		@Override
		public int compare(TemplateDefinition o1, TemplateDefinition o2) {
			if (o1 == null || o1.getId() == null) return -1;
			if (o2 == null || o2.getId() == null) return 1;
			return o1.getId().compareTo(o2.getId());
		}
		
	}

}


