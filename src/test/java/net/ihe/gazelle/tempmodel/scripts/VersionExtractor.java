package net.ihe.gazelle.tempmodel.scripts;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class VersionExtractor {
	
	public static void main(String[] args) throws JAXBException, IOException {
		String decorpath = "/home/aboufahj/Téléchargements/ccda-20180517T173451-en-US-decor-compiled.xml";
		String version = "2.1";
		String contentExtracted = extractVersion(version, decorpath);
		printDoc(contentExtracted, "rest-2.1.xml");
	}

	private static String extractVersion(String version, String decorpath) throws FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor(decorpath);
		List<TemplateDefinition> listRetained = new ArrayList<TemplateDefinition>();
		for (TemplateDefinition templateDefinition : RulesUtil.getTemplates(dec.getRules())) {
			if (templateDefinition.getVersionLabel() != null && templateDefinition.getVersionLabel().equals(version)) {
				listRetained.add(templateDefinition);
			}
		}
		dec.getRules().getTemplateAssociationOrTemplate().removeAll(RulesUtil.getTemplates(dec.getRules()));
		dec.getRules().getTemplateAssociationOrTemplate().addAll(listRetained);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DecorMarshaller.marshallDecor(dec, baos);
		return baos.toString();
	}
	
	public static void printDoc(String doc, String name) throws IOException {
		FileWriter fw = new FileWriter(new File(name));
		fw.append(doc);
		fw.close();
	}

}
