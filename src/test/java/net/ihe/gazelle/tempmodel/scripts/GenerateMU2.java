package net.ihe.gazelle.tempmodel.scripts;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

public class GenerateMU2 {
	
	public static void main(String[] args) throws MalformedURLException, FileNotFoundException, JAXBException {
		exportMU2VDTInpatientSummary();
		exportMU2VDTAmbulatorySummary();
		exportMU2TransitionOfCareInpatientSummary();
		exportMU2TransitionOfCareAmbulatorySummary();
		exportMU2ClinicalSummary();
		
	}
	
	static void exportMU2VDTInpatientSummary() throws MalformedURLException, FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("/home/aboufahj/sequoi/bbr2/ccdamu2-20170428T135220-en-US-decor-compiled.xml");
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(dec.getRules());
		List<TemplateDefinition> ltdvtoremove = new ArrayList<>();
		for (TemplateDefinition templateDefinition : ltd) {
			if ("2.1".equals(templateDefinition.getVersionLabel())) {
				ltdvtoremove.add(templateDefinition);
			}
			String[] tobeDeleted = new String[] {"1.3.6.1.4.1.12559.11.28.2.1.10.1", "1.3.6.1.4.1.12559.11.28.2.1.10.2", "1.3.6.1.4.1.12559.11.28.2.1.10.3", 
					"1.3.6.1.4.1.12559.11.28.2.1.10.4", "1.3.6.1.4.1.12559.11.28.2.1.10.6"};
			if (templateDefinition.getId() != null && Arrays.asList(tobeDeleted).contains(templateDefinition.getId())) {
				ltdvtoremove.add(templateDefinition);
			}
		}
		sortAndSave(dec, ltd, ltdvtoremove, "/home/aboufahj/sequoi/bbr2/ccdamu2-20170428T135220-en-US-decor-compiled-MU2VDTInpatientSummary.xml");
	}
	
	static void exportMU2VDTAmbulatorySummary() throws MalformedURLException, FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("/home/aboufahj/sequoi/bbr2/ccdamu2-20170428T135220-en-US-decor-compiled.xml");
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(dec.getRules());
		List<TemplateDefinition> ltdvtoremove = new ArrayList<>();
		for (TemplateDefinition templateDefinition : ltd) {
			if ("2.1".equals(templateDefinition.getVersionLabel())) {
				ltdvtoremove.add(templateDefinition);
			}
			String[] tobeDeleted = new String[] {"1.3.6.1.4.1.12559.11.28.2.1.10.1", "1.3.6.1.4.1.12559.11.28.2.1.10.2", "1.3.6.1.4.1.12559.11.28.2.1.10.3", 
					"1.3.6.1.4.1.12559.11.28.2.1.10.4", "1.3.6.1.4.1.12559.11.28.2.1.10.7"};
			if (templateDefinition.getId() != null && Arrays.asList(tobeDeleted).contains(templateDefinition.getId())) {
				ltdvtoremove.add(templateDefinition);
			}
		}
		sortAndSave(dec, ltd, ltdvtoremove, "/home/aboufahj/sequoi/bbr2/ccdamu2-20170428T135220-en-US-decor-compiled-MU2VDTAmbulatorySummary.xml");
	}
	
	static void exportMU2TransitionOfCareInpatientSummary() throws MalformedURLException, FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("/home/aboufahj/sequoi/bbr2/ccdamu2-20170428T135220-en-US-decor-compiled.xml");
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(dec.getRules());
		List<TemplateDefinition> ltdvtoremove = new ArrayList<>();
		for (TemplateDefinition templateDefinition : ltd) {
			if ("2.1".equals(templateDefinition.getVersionLabel())) {
				ltdvtoremove.add(templateDefinition);
			}
			String[] tobeDeleted = new String[] {"1.3.6.1.4.1.12559.11.28.2.1.10.1", "1.3.6.1.4.1.12559.11.28.2.1.10.3", 
					"1.3.6.1.4.1.12559.11.28.2.1.10.6", "1.3.6.1.4.1.12559.11.28.2.1.10.7", "1.3.6.1.4.1.12559.11.28.2.1.10.5"};
			if (templateDefinition.getId() != null && Arrays.asList(tobeDeleted).contains(templateDefinition.getId())) {
				ltdvtoremove.add(templateDefinition);
			}
		}
		sortAndSave(dec, ltd, ltdvtoremove, "/home/aboufahj/sequoi/bbr2/ccdamu2-20170428T135220-en-US-decor-compiled-MU2TransitionOfCareInpatientSummary.xml");
	}
	
	static void exportMU2TransitionOfCareAmbulatorySummary() throws MalformedURLException, FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("/home/aboufahj/sequoi/bbr2/ccdamu2-20170428T135220-en-US-decor-compiled.xml");
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(dec.getRules());
		List<TemplateDefinition> ltdvtoremove = new ArrayList<>();
		for (TemplateDefinition templateDefinition : ltd) {
			if ("2.1".equals(templateDefinition.getVersionLabel())) {
				ltdvtoremove.add(templateDefinition);
			}
			String[] tobeDeleted = new String[] {"1.3.6.1.4.1.12559.11.28.2.1.10.1", "1.3.6.1.4.1.12559.11.28.2.1.10.4", 
					"1.3.6.1.4.1.12559.11.28.2.1.10.6", "1.3.6.1.4.1.12559.11.28.2.1.10.7", "1.3.6.1.4.1.12559.11.28.2.1.10.5"};
			if (templateDefinition.getId() != null && Arrays.asList(tobeDeleted).contains(templateDefinition.getId())) {
				ltdvtoremove.add(templateDefinition);
			}
		}
		sortAndSave(dec, ltd, ltdvtoremove, "/home/aboufahj/sequoi/bbr2/ccdamu2-20170428T135220-en-US-decor-compiled-MU2TransitionOfCareAmbulatorySummary.xml");
	}

	static void exportMU2ClinicalSummary() throws MalformedURLException, FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("/home/aboufahj/sequoi/bbr2/ccdamu2-20170428T135220-en-US-decor-compiled.xml");
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(dec.getRules());
		List<TemplateDefinition> ltdvtoremove = new ArrayList<>();
		for (TemplateDefinition templateDefinition : ltd) {
			if ("2.1".equals(templateDefinition.getVersionLabel())) {
				ltdvtoremove.add(templateDefinition);
			}
			String[] tobeDeleted = new String[] {"1.3.6.1.4.1.12559.11.28.2.1.10.2", "1.3.6.1.4.1.12559.11.28.2.1.10.3", "1.3.6.1.4.1.12559.11.28.2.1.10.4", 
					"1.3.6.1.4.1.12559.11.28.2.1.10.6", "1.3.6.1.4.1.12559.11.28.2.1.10.7", "1.3.6.1.4.1.12559.11.28.2.1.10.5"};
			if (templateDefinition.getId() != null && Arrays.asList(tobeDeleted).contains(templateDefinition.getId())) {
				ltdvtoremove.add(templateDefinition);
			}
		}
		sortAndSave(dec, ltd, ltdvtoremove, "/home/aboufahj/sequoi/bbr2/ccdamu2-20170428T135220-en-US-decor-compiled-MU2ClinicalSummary.xml");
	}
	
	private static void sortAndSave(Decor dec, List<TemplateDefinition> ltd, List<TemplateDefinition> ltdvtoremove, String filePath)
			throws JAXBException, FileNotFoundException {
		dec.getRules().getTemplateAssociationOrTemplate().removeAll(ltd);
		ltd.removeAll(ltdvtoremove);
		Collections.sort(ltd, new ComparatorTemplates());
		dec.getRules().getTemplateAssociationOrTemplate().addAll(ltd);
		DecorMarshaller.marshallDecor(dec, 
				new FileOutputStream(filePath));
	}
	
	static void exportWithoutCCDA21() throws MalformedURLException, FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor("/home/aboufahj/sequoi/bbr2/ccdamu2-20170428T135220-en-US-decor-compiled.xml");
		List<TemplateDefinition> ltd = RulesUtil.getTemplates(dec.getRules());
		List<TemplateDefinition> ltdvtoremove = new ArrayList<>();
		for (TemplateDefinition templateDefinition : ltd) {
			if ("2.1".equals(templateDefinition.getVersionLabel())) {
				ltdvtoremove.add(templateDefinition);
			}
		}
		dec.getRules().getTemplateAssociationOrTemplate().removeAll(ltd);
		ltd.removeAll(ltdvtoremove);
		Collections.sort(ltd, new ComparatorTemplates());
		dec.getRules().getTemplateAssociationOrTemplate().addAll(ltd);
		DecorMarshaller.marshallDecor(dec, new FileOutputStream("/home/aboufahj/sequoi/bbr2/ccdamu2-20170428T135220-en-US-decor-compiled-cleaned.xml"));
	}
	
	static class ComparatorTemplates implements Comparator<TemplateDefinition> {

		@Override
		public int compare(TemplateDefinition o1, TemplateDefinition o2) {
			if (o1 == null || o1.getId() == null) return -1;
			if (o2 == null || o2.getId() == null) return 1;
			return o1.getId().compareTo(o2.getId());
		}
		
	}

}


