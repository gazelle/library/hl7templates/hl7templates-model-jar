package net.ihe.gazelle.tempmodel.test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ContainDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Abderrazek Boufahja
 */
public class ContainDefinitionUtilTest {

    Decor decorTemplates = null;
    ContainDefinition selectedContain = null;

    @Before
    public void setUp() {
        decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
        RuleDefinition participantRole = RuleDefinitionUtil.getElementByName(firstRD, "hl7:participantRole");
        ChoiceDefinition selectedChoice = RuleDefinitionUtil.getChoices(participantRole).get(0);
        selectedContain = ChoiceDefinitionUtil.getContains(selectedChoice).get(0);
    }

    @Test
    public void testReferencedTemplateDefinition() {
        TemplateDefinition aa = ContainDefinitionUtil.referencedTemplateDefinition(selectedContain);
        assertTrue(aa.getName().equals("playingEntity"));
    }

    @Test
    public void testGetParentTemplateDefinition() {
        TemplateDefinition aa = ContainDefinitionUtil.getParentTemplateDefinition(selectedContain);
        assertTrue(aa.getName().equals("CDAParticipantBody"));
    }

    @Test
    public void testReferencedTemplateDefinitionFromParentBBR() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_contain_bbr.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition comp = RuleDefinitionUtil.getElementByName(firstRD, "cda:component");
        RuleDefinition strc = RuleDefinitionUtil.getElementByName(comp, "cda:structuredBody");
        RuleDefinition comp2 = RuleDefinitionUtil.getElementByName(strc, "cda:component");
        ContainDefinition containDefinition = RuleDefinitionUtil.getContains(comp2).get(0);
        TemplateDefinition aa = ContainDefinitionUtil.referencedTemplateDefinitionFromParentBBR(containDefinition);
        assertTrue(aa != null);
    }

    @Test
    public void testGetElementByName() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_contain_bbr.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition comp = RuleDefinitionUtil.getElementByName(firstRD, "cda:component");
        RuleDefinition strc = RuleDefinitionUtil.getElementByName(comp, "cda:structuredBody");
        RuleDefinition comp2 = RuleDefinitionUtil.getElementByName(strc, "cda:component");
        List<ContainDefinition> contains = RuleDefinitionUtil.getContains(comp2);
        ContainDefinition currentContainDefinition = contains.get(0);
        processLocal(currentContainDefinition);
        RuleDefinition res = ContainDefinitionUtil.getElementByName(currentContainDefinition, "cda:component");
        assertEquals(Integer.toString(1), ((ContainDefinition) res.getParentObject()).getMaximumMultiplicity());
        assertEquals(0, ((ContainDefinition) res.getParentObject()).getMinimumMultiplicity().intValue());
        assertEquals("2.16.840.1.113883.10.20.21.2.1", ((ContainDefinition) res.getParentObject()).getRef());
    }

    @Test
    public void testGetElements() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_contain_bbr.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition comp = RuleDefinitionUtil.getElementByName(firstRD, "cda:component");
        RuleDefinition strc = RuleDefinitionUtil.getElementByName(comp, "cda:structuredBody");
        RuleDefinition comp2 = RuleDefinitionUtil.getElementByName(strc, "cda:component");
        List<ContainDefinition> contains = RuleDefinitionUtil.getContains(comp2);
        ContainDefinition currentContainDefinition = contains.get(0);
        processLocal(currentContainDefinition);
        List<RuleDefinition> rdList = ContainDefinitionUtil.getElements(currentContainDefinition);
        assertEquals(1, rdList.size());
        assertEquals(Integer.toString(1), ((ContainDefinition) rdList.get(0).getParentObject()).getMaximumMultiplicity());
        assertEquals(0, ((ContainDefinition) rdList.get(0).getParentObject()).getMinimumMultiplicity().intValue());
        assertEquals("2.16.840.1.113883.10.20.21.2.1", ((ContainDefinition) rdList.get(0).getParentObject()).getRef());
    }


    @Test
    public void testGetTemplateIdElement() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_contain_bbr.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition comp = RuleDefinitionUtil.getElementByName(firstRD, "cda:component");
        RuleDefinition strc = RuleDefinitionUtil.getElementByName(comp, "cda:structuredBody");
        RuleDefinition comp2 = RuleDefinitionUtil.getElementByName(strc, "cda:component");
        List<ContainDefinition> contains = RuleDefinitionUtil.getContains(comp2);
        ContainDefinition currentContainDefinition = contains.get(0);
        processLocal(currentContainDefinition);

        RuleDefinition templateId = ContainDefinitionUtil.getTemplateIdElement(contains.get(0));

        assertNotNull(templateId);
        assertEquals("hl7:templateId", templateId.getName());
        assertEquals(1, templateId.getAttribute().size());
        assertEquals("2.16.840.1.113883.10.20.21.2.1", templateId.getAttribute().get(0).getValue());
        assertEquals(contains.get(0).getRef(), templateId.getAttribute().get(0).getValue());
        assertEquals("root", templateId.getAttribute().get(0).getName());
    }


    private void processLocal(ContainDefinition currentContainDefinition) {
        TemplateDefinition td = ContainDefinitionUtil.referencedTemplateDefinitionFromParentBBR(currentContainDefinition);
        if (currentContainDefinition.getParentObject() instanceof RuleDefinition) {
            RuleDefinition replacementRD = new RuleDefinition();
            replacementRD.setName("cda:component");
            replacementRD.setParentObject(currentContainDefinition);
            RuleDefinition templateId = new RuleDefinition();
            templateId.setName("hl7:templateId");
            templateId.getAttribute().add(new Attribute());
            templateId.getAttribute().get(0).setName("root");
            templateId.getAttribute().get(0).setValue(TemplateDefinitionUtil.extractTemplateId(td));
            replacementRD.getLetOrAssertOrReport().add(templateId);
            currentContainDefinition.getLetOrAssertOrReport().add(replacementRD);
        }
    }
}
