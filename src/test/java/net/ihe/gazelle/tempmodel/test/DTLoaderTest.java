package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.decor.dt.model.Datatype;
import net.ihe.gazelle.tempmodel.decor.dt.model.Datatypes;
import net.ihe.gazelle.tempmodel.decor.dt.utils.DTLoader;

public class DTLoaderTest {

	@Test
	public void testLoadDT() throws FileNotFoundException, JAXBException {
		String pathDatatypes = "src/main/resources/datatypes.xml";
		 Datatypes listDT = DTLoader.loadDT(new FileInputStream(pathDatatypes));
		 List<Datatype> aa = listDT.getDatatype();
		 assertTrue(aa.size()>0);
		 boolean containSDTEXT = false;
		 for (Datatype datatype : aa) {
			if (datatype.getValue().equals("SD.TEXT")) {
				containSDTEXT = true;
				assertTrue(datatype.getRef().equals("StrucDocText"));
			}
		}
		assertTrue(containSDTEXT);
	}
}
