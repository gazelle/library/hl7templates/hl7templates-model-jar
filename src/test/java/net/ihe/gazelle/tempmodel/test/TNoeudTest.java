package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.templates.model.TNoeud;

public class TNoeudTest {

	@Test
	public void testHashCode() {
		TNoeud tn = new TNoeud();
		tn.setIdentifier("1");
		assertTrue(tn.hashCode() == 76880);
	}

	@Test
	public void testEqualsObject() {
		TNoeud tn = new TNoeud();
		tn.setIdentifier("1");
		tn.setListParents(new ArrayList<TNoeud>());
		tn.getListParents().add(new TNoeud());
		tn.getListParents().get(0).setIdentifier("aaa");
		tn.setListTemplateId(new ArrayList<String>());
		tn.getListTemplateId().add("1.2.3");
		TNoeud tn2 = new TNoeud();
		tn2.setIdentifier("1");
		tn2.setListParents(new ArrayList<TNoeud>());
		tn2.getListParents().add(new TNoeud());
		tn2.getListParents().get(0).setIdentifier("aaa");
		tn2.setListTemplateId(new ArrayList<String>());
		tn2.getListTemplateId().add("1.2.3");
		assertTrue(tn.equals(tn2));
	}

	@Test
	public void testToString() {
		TNoeud tn = new TNoeud();
		tn.setIdentifier("1");
		tn.setListParents(new ArrayList<TNoeud>());
		tn.getListParents().add(new TNoeud());
		tn.getListParents().get(0).setIdentifier("aaa");
		tn.setListTemplateId(new ArrayList<String>());
		tn.getListTemplateId().add("1.2.3");
		assertTrue(tn.toString().equals("TNoeud [listTemplateId=[1.2.3], identifier=1, "
				+ "listParents=[TNoeud [listTemplateId=null, identifier=aaa, listParents=null]]]"));
	}

}
