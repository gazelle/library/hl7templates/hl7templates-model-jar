package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AssertRole;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.AssertUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AssertUtilTest {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_assert.xml");
	}

	@Test
	public void testGetParentTemplateDefinition() {
		Assert currentAssert = new Assert();
		currentAssert.setTest("hl7:id/@root='1.2.3'");
		currentAssert.setRole(AssertRole.ERROR);
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		currentAssert.setParentObject(firstRD);
		TemplateDefinition td = AssertUtil.getParentTemplateDefinition(currentAssert);
		assertTrue(td.getId().equals("1.3.6.1.4.1.19376.1.5.3.1.4.5.2"));
	}

	@Test
	public void testGetStringValue() {
		Assert currentAssert = new Assert();
		currentAssert.setTest("hl7:id/@root='1.2.3'");
		currentAssert.setRole(AssertRole.ERROR);
		currentAssert.getContent().add("Hello !");
		assertTrue(AssertUtil.getStringValue(currentAssert).equals("Hello !"));
	}

}
