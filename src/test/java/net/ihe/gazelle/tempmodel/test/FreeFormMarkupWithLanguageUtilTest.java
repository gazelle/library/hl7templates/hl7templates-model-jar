package net.ihe.gazelle.tempmodel.test;

import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.FreeFormMarkupWithLanguageUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class FreeFormMarkupWithLanguageUtilTest {
	
Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_vmc.xml");
	}

	@Test
	public void testExtractContentAsString() throws JAXBException {
		RuleDefinition first = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
		RuleDefinition time = RuleDefinitionUtil.getElements(first).get(0);
		List<FreeFormMarkupWithLanguage> aa = RuleDefinitionUtil.getConstraints(time);
		String content = "";
		for (FreeFormMarkupWithLanguage freeFormMarkupWithLanguage : aa) {
			content += FreeFormMarkupWithLanguageUtil.extractContentAsString(freeFormMarkupWithLanguage);
		}
		Assert.assertTrue(content.equals("testaa"));
	}

}
