package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ihe.gazelle.tempmodel.gov.model.TemplateRepository;
import net.ihe.gazelle.tempmodel.gov.util.TemplateRepositoryUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.templates.model.TNoeud;
import net.ihe.gazelle.tempmodel.templates.util.TNoeudUtil;

public class TNoeudUtilTest extends TNoeudUtil {
	
	private static Logger log = LoggerFactory.getLogger(TNoeudUtilTest.class);

	@Test
	public void testExtractListTemplateIdForIdentificationFromTD1() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid.xml");
		TemplateDefinition first = RulesUtil.getTemplates(dec.getRules()).get(0);
		Set<String> ls = TNoeudUtil.extractListTemplateIdForIdentificationFromTD(first);
		assertTrue(ls.size()==1);
		assertTrue(ls.contains("1.3.6.1.4.1.19376.1.5.3.1.3.16.1"));
	}
	
	@Test
	public void testExtractListTemplateIdForIdentificationFromTD2() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid2.xml");
		TemplateDefinition first = RulesUtil.getTemplates(dec.getRules()).get(0);
		Set<String> ls = TNoeudUtil.extractListTemplateIdForIdentificationFromTD(first);
		assertTrue(ls.size()==1);
		assertTrue(ls.contains("2.2.2.2.2"));
	}
	
	@Test
	public void testExtractListTemplateIdForIdentificationFromTD3() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid3.xml");
		TemplateDefinition first = RulesUtil.getTemplates(dec.getRules()).get(0);
		Set<String> ls = TNoeudUtil.extractListTemplateIdForIdentificationFromTD(first);
		assertTrue(ls.size()==1);
		assertTrue(ls.contains("1.3.6.1.4.1.19376.1.12.1.3.1"));
	}
	
	@Test
	public void testExtractListTemplateIdForIdentificationFromTD2_2() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid2.xml");
		TemplateDefinition second = RulesUtil.getTemplates(dec.getRules()).get(1);
		Set<String> ls = TNoeudUtil.extractListTemplateIdForIdentificationFromTD(second);
		assertTrue(ls.size()==1);
		assertTrue(ls.contains("2.2.2.2.2"));
	}

	@Test
	public void testExtractTNoeudTemplateDefinition() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid2.xml");
		TemplateDefinition second = RulesUtil.getTemplates(dec.getRules()).get(1);
		TNoeud tn = TNoeudUtil.extractTNoeud(second);
		System.out.println(tn.toString());
		assertTrue(tn.toString().equals("TNoeud [listTemplateId=[1.2.3.4.5, 2.2.2.2.2], identifier=2.2.2.2, "
				+ "listParents=[TNoeud [listTemplateId=[1.2.3.4.5], identifier=3.3.3.3, listParents=null]]]"));
	}

	@Test
	public void testExtractTNoeudTemplateDefinitionTemplateRepository() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_tdid2.xml");
		TemplateDefinition second = RulesUtil.getTemplates(dec.getRules()).get(0);
		TemplateRepository tr = TemplateRepositoryUtil.generateTemplateRepository(second.getParentObject().getParentObject());
		TNoeud tn = TNoeudUtil.extractTNoeud(second, tr);
		assertTrue(tn.toString().equals("TNoeud [listTemplateId=[1.2.3.4.5, 2.2.2.2.2], identifier=1.1.1.1, listParents=["
				+ 				"TNoeud [listTemplateId=[1.2.3.4.5, 2.2.2.2.2], identifier=2.2.2.2, listParents=["
				+ 								"TNoeud [listTemplateId=[1.2.3.4.5], identifier=3.3.3.3, listParents=null]]]]]"));
	}

	@Test
	public void testExtractListTemplateIdForIdentification1() {
		TNoeud tn = new TNoeud();
		Set<String> aa = extractListTemplateIdForIdentification(tn);
		assertTrue(aa.isEmpty());
	}
	
	@Test
	public void testExtractListTemplateIdForIdentification2() {
		TNoeud tn = new TNoeud();
		tn.setIdentifier("1.2.3");
		tn.getListTemplateId().add("1.2.3");
		Set<String> aa = extractListTemplateIdForIdentification(tn);
		assertTrue(aa.size() == 1);
		assertTrue(aa.contains("1.2.3"));
	}
	
	@Test
	public void testExtractListTemplateIdForIdentification3() {
		TNoeud tn = new TNoeud();
		tn.setIdentifier("1.2.3");
		tn.getListTemplateId().add("1.2.4");
		tn.getListTemplateId().add("1.2.3.4");
		tn.getListParents().add(new TNoeud());
		Set<String> aa = extractListTemplateIdForIdentification(tn);
		assertTrue(aa.size() == 2);
		assertTrue(aa.contains("1.2.3.4"));
		assertTrue(aa.contains("1.2.4"));
	}
	
	@Test
	public void testExtractListTemplateIdForIdentification4() {
		TNoeud tn = new TNoeud();
		tn.setIdentifier("1.2.3");
		tn.getListTemplateId().add("1.2.4");
		tn.getListTemplateId().add("1.2.3.4");
		tn.getListParents().add(new TNoeud());
		tn.getListParents().get(0).getListTemplateId().add("1.2.3");
		Set<String> aa = extractListTemplateIdForIdentification(tn);
		assertTrue(aa.size() == 2);
		assertTrue(aa.contains("1.2.3.4"));
		assertTrue(aa.contains("1.2.4"));
	}
	
	@Test
	public void testExtractListTemplateIdForIdentification5() {
		TNoeud tn = new TNoeud();
		tn.setIdentifier("1.2.3");
		tn.getListTemplateId().add("1.2.4");
		tn.getListTemplateId().add("1.2.3.4");
		tn.getListParents().add(new TNoeud());
		tn.getListParents().get(0).getListTemplateId().add("1.2.4");
		Set<String> aa = extractListTemplateIdForIdentification(tn);
		assertTrue(aa.size() == 1);
		assertTrue(aa.contains("1.2.3.4"));
	}
	
	@Test
	public void testExtractListTemplateIdForIdentification6() {
		TNoeud tn = new TNoeud();
		tn.setIdentifier("1.2.3");
		tn.getListTemplateId().add("1.2.4");
		tn.getListTemplateId().add("1.2.3.4");
		tn.getListParents().add(new TNoeud());
		tn.getListParents().get(0).getListTemplateId().add("1.2.4");
		tn.getListParents().add(new TNoeud());
		tn.getListParents().get(1).getListTemplateId().add("1.2.3.4");
		Set<String> aa = extractListTemplateIdForIdentification(tn);
		assertTrue(aa.isEmpty());
	}
	
	@Test
	public void testExtractListTemplateIdForIdentification7() {
		TNoeud tn = new TNoeud();
		tn.setIdentifier("1.2.3");
		tn.getListTemplateId().add("1.2.4");
		tn.getListTemplateId().add("1.2.3.4");
		tn.getListParents().add(new TNoeud());
		tn.getListParents().get(0).getListTemplateId().add("1.2.4");
		tn.getListParents().get(0).getListTemplateId().add("1.2.3.4");
		Set<String> aa = extractListTemplateIdForIdentification(tn);
		assertTrue(aa.size() == 2);
		assertTrue(aa.contains("1.2.3.4"));
		assertTrue(aa.contains("1.2.4"));
	}
	
	@Test
	public void testExtractListTemplateIdForIdentification8() {
		TNoeud tn = new TNoeud();
		tn.setIdentifier("1.2.3");
		tn.getListTemplateId().add("1.2.4");
		tn.getListTemplateId().add("1.2.3.4");
		tn.getListParents().add(new TNoeud());
		tn.getListParents().get(0).getListTemplateId().add("1.2.4");
		tn.getListParents().get(0).getListTemplateId().add("1.2.3.4");
		tn.getListParents().get(0).getListParents().add(new TNoeud());
		tn.getListParents().get(0).getListParents().get(0).getListTemplateId().add("1.2.4");
		Set<String> aa = extractListTemplateIdForIdentification(tn);
		assertTrue(aa.size() == 1);
		assertTrue(aa.contains("1.2.3.4"));
	}

}
