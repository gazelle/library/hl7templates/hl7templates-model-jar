package net.ihe.gazelle.tempmodel.test.location;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.location.util.AttributeLocation;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public class AttributeLocationTest {

	@Test
	public void testGetLocation1() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_assert.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		RuleDefinition rdfirst = TemplateDefinitionUtil.getFirstElement(td);
		Attribute attr = rdfirst.getAttribute().get(0);
		String loc = AttributeLocation.getLocation(attr);
		assertTrue(loc.equals("//rules/template[1]/element[1]/attribute[1]"));
		attr = RuleDefinitionUtil.getElements(rdfirst).get(0).getAttribute().get(0);
		assertTrue(attr.getName().equals("root"));
		loc = AttributeLocation.getLocation(attr);
		assertTrue(loc.equals("//rules/template[1]/element[1]/element[1]/attribute[1]"));
		attr = RuleDefinitionUtil.getElements(rdfirst).get(1).getAttribute().get(0);
		loc = AttributeLocation.getLocation(attr);
		assertTrue(loc.equals("//rules/template[1]/element[1]/element[2]/attribute[1]"));
		attr = rdfirst.getAttribute().get(1);
		loc = AttributeLocation.getLocation(attr);
		assertTrue(loc.equals("//rules/template[1]/element[1]/attribute[2]"));
	}
	
	@Test
	public void testGetLocation2() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/template3.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		Attribute attr = TemplateDefinitionUtil.getAttributes(td).get(0);
		String loc = AttributeLocation.getLocation(attr);
		assertTrue(loc.equals("//rules/template[1]/attribute[1]"));
	}

}
