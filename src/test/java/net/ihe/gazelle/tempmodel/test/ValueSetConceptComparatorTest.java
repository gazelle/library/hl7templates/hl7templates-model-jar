package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ValueSetConceptComparator;

public class ValueSetConceptComparatorTest {

	@Test
	public void testCompare1() {
		ValueSetConcept vsc1 = new ValueSetConcept();
		vsc1.setCode("A1");
		vsc1.setCodeSystem("11111");
		ValueSetConcept vsc2 = new ValueSetConcept();
		vsc2.setCode("A2");
		vsc2.setCodeSystem("11111");
		assertTrue((new ValueSetConceptComparator()).compare(vsc1, vsc2)<0);
	}
	
	@Test
	public void testCompare2() {
		assertTrue((new ValueSetConceptComparator()).compare(new ValueSetConcept(), null)>0);
		assertTrue((new ValueSetConceptComparator()).compare(null, new ValueSetConcept())<0);
		assertTrue((new ValueSetConceptComparator()).compare(null, null)<0);
	}
	
	@Test
	public void testCompare3() {
		ValueSetConcept vsc1 = new ValueSetConcept();
		vsc1.setCodeSystem("11111");
		ValueSetConcept vsc2 = new ValueSetConcept();
		vsc2.setCode("A2");
		vsc2.setCodeSystem("11111");
		assertTrue((new ValueSetConceptComparator()).compare(vsc1, vsc2)<0);
	}
	
	@Test
	public void testCompare4() {
		ValueSetConcept vsc1 = new ValueSetConcept();
		vsc1.setCode("A1");
		vsc1.setCodeSystem("11111");
		ValueSetConcept vsc2 = new ValueSetConcept();
		vsc2.setCodeSystem("11111");
		assertTrue((new ValueSetConceptComparator()).compare(vsc1, vsc2)>0);
	}

}
