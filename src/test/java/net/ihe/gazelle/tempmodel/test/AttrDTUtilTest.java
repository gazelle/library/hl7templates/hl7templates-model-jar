package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.dtdefault.model.PE;
import net.ihe.gazelle.tempmodel.dtdefault.utils.AttrDTUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttrDTUtilTest extends AttrDTUtil {

	@Test
	public void testGetOriginalDTOfType() {
		assertTrue(AttrDTUtil.getOriginalDTOfType("II", "root").equals("uid"));
		assertTrue(AttrDTUtil.getOriginalDTOfType("CS", "code").equals("cs"));
	}
	
	@Test
	public void testFindPEByTypeName() throws Exception {
		PE pe = findPEByTypeName("II");
		assertTrue(pe != null);
		assertTrue(pe.getName().equals("II"));
	}

}
