package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ValueSetRefUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ValueSetRefUtilTest {

	@Test
	public void testExtractReferencedValueSetValueSetRefListOfValueSet() {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_terma.xml");
		ValueSetRef vsr = new ValueSetRef();
		vsr.setRef("1.2.3");
		ValueSet aa = ValueSetRefUtil.extractReferencedValueSet(vsr, decorTemplates.getTerminology().getValueSet());
		assertTrue(aa != null);
		assertTrue(aa.getConceptList().getConceptOrInclude().size()==2);
	}

	@Test
	public void testExtractReferencedValueSetStringListOfValueSet() {
		Decor decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_terma.xml");
		ValueSet aa = ValueSetRefUtil.extractReferencedValueSet("1.2.3", decorTemplates.getTerminology().getValueSet());
		assertTrue(aa != null);
		assertTrue(aa.getConceptList().getConceptOrInclude().size()==2);
	}

}
