package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.decor.dt.model.Datatype;
import net.ihe.gazelle.tempmodel.decor.dt.utils.DTUtils;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class DTUtilsTest {
	
	@Test
	public void testDatatypesContainDT() {
		assertTrue(DTUtils.datatypesContainDT("II"));
		assertTrue(DTUtils.datatypesContainDT("II.EPSOS"));
		assertTrue(DTUtils.datatypesContainDT("CD"));
		assertFalse(DTUtils.datatypesContainDT("IA"));
		assertFalse(DTUtils.datatypesContainDT("ClassRoot"));
		assertFalse(DTUtils.datatypesContainDT("uid"));
		assertFalse(DTUtils.datatypesContainDT("POCDMT000040Act"));
		assertFalse(DTUtils.datatypesContainDT(null));
		assertFalse(DTUtils.datatypesContainDT(""));
	}

	@Test
	public void testGetRealDatatype1() {
		String da = DTUtils.getRealDatatype("II");
		assertTrue(da.equals("II"));
		da = DTUtils.getRealDatatype("II.EPSOS");
		assertTrue(da.equals("II"));
		da = DTUtils.getRealDatatype("");
		assertTrue(da == null);
	}
	
	@Test
	public void testGetRealDatatype2() {
		Datatype dt = new Datatype();
		assertTrue(DTUtils.getRealDatatype(dt) == null);
		dt.setRef("test");
		assertTrue(DTUtils.getRealDatatype(dt).equals("test"));
	}
	
	@Test
	public void testIgnoreDTExtensionError() throws Exception {
		assertTrue(DTUtils.ignoreDTExtensionError("CV.EPSOS"));
		assertFalse(DTUtils.ignoreDTExtensionError("CV"));
		assertFalse(DTUtils.ignoreDTExtensionError("II"));
		assertFalse(DTUtils.ignoreDTExtensionError(null));
		assertFalse(DTUtils.ignoreDTExtensionError("AA"));
	}
	
	@Test
	public void testConstrainedDTIsAnExtension() throws Exception {
		assertTrue(DTUtils.constrainedDTIsAnExtension("CD", "ANY"));
		assertFalse(DTUtils.constrainedDTIsAnExtension("IVLTS", "TS"));
		assertTrue(DTUtils.constrainedDTIsAnExtension("IVL_TS", "TS"));
		assertFalse(DTUtils.constrainedDTIsAnExtension("II", "TS"));
		assertFalse(DTUtils.constrainedDTIsAnExtension("II", null));
	}
	
	@Test
	public void testGetUMLDatatype() throws Exception {
		assertTrue(DTUtils.getUMLDatatype("IVL_TS").equals("IVLTS"));
		assertTrue(DTUtils.getUMLDatatype("II").equals("II"));
		assertTrue(DTUtils.getUMLDatatype(null) == null);
	}

}
