package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.decor.dt.model.AttDatatype;
import net.ihe.gazelle.tempmodel.handler.util.AttributeCheckerUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class AttributeCheckerUtilTest extends AttributeCheckerUtil {
	
	Decor decorTemplates = null;
	
	@Before
	public void setUp(){
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/decor_attrdt.xml");
	}

	@Test
	public void testIsUseValueAcceptableForAttribute() {
		Decor decorVocab1 = DecorMarshaller.loadDecor("src/test/resources/decor_vocab1.xml");
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorVocab1.getRules()).get(0));
		RuleDefinition temp = RuleDefinitionUtil.getElementByName(firstRD, "hl7:templateId");
		assertTrue(AttributeCheckerUtil.isUseValueAcceptableForAttribute(temp.getAttribute().get(0), "AAA"));
		assertFalse(AttributeCheckerUtil.isUseValueAcceptableForAttribute(temp.getAttribute().get(0), null));
		assertFalse(AttributeCheckerUtil.isUseValueAcceptableForAttribute(null, "AAA"));
		
	}

	@Test
	public void testIsUseValueAcceptable() {
		assertTrue(isUseValueAcceptable("ABC", "AD"));
		assertTrue(isUseValueAcceptable("H", "AD"));
		assertTrue(isUseValueAcceptable("XXXXXXXX", "AD"));
		assertTrue(isUseValueAcceptable("XXX X XXXX", "AD"));
		assertFalse(isUseValueAcceptable("", "EN"));
		assertFalse(isUseValueAcceptable(null, "AD"));
		assertFalse(isUseValueAcceptable("XXXXXXXX", null));
		assertFalse(isUseValueAcceptable(null, null));
		assertTrue(isUseValueAcceptable("SNDX", "EN"));
		assertTrue(isUseValueAcceptable("HP", "TEL"));
		assertTrue(isUseValueAcceptable("HP", "TEL"));
	}
	
	@Test
	public void testVerifyThatDatatypeIsSupported1() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition code = RuleDefinitionUtil.getElementByName(firstRD, "hl7:code");
		Attribute attr = code.getAttribute().get(0);
		assertTrue(AttributeCheckerUtil.verifyThatDatatypeIsSupported(attr, AttDatatype.CS));
		assertTrue(AttributeCheckerUtil.verifyThatDatatypeIsSupported(attr, AttDatatype.INT));
		assertTrue(AttributeCheckerUtil.verifyThatDatatypeIsSupported(attr, AttDatatype.TS));
	}
	
	@Test
	public void testVerifyThatDatatypeIsSupported2() {
		RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplates.getRules()).get(0));
		RuleDefinition id = RuleDefinitionUtil.getElementByName(firstRD, "hl7:id");
		Attribute attr = id.getAttribute().get(0);
		assertFalse(AttributeCheckerUtil.verifyThatDatatypeIsSupported(attr, AttDatatype.CS));
		assertFalse(AttributeCheckerUtil.verifyThatDatatypeIsSupported(attr, AttDatatype.INT));
		assertFalse(AttributeCheckerUtil.verifyThatDatatypeIsSupported(attr, AttDatatype.TS));
		assertTrue(AttributeCheckerUtil.verifyThatDatatypeIsSupported(attr, AttDatatype.BN));
		assertTrue(AttributeCheckerUtil.verifyThatDatatypeIsSupported(attr, AttDatatype.BL));
	}

}
