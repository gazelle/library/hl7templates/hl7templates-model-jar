package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ItemUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ItemUtilTest {
	
	Decor decorObs = null;
	
	@Before
	public void setUp(){
		decorObs = DecorMarshaller.loadDecor("src/test/resources/decor_obs.xml");
	}

	@Test
	public void testExtractSetItems1() {
		Item item = new Item();
		item.setLabel("test");
		Set<String> aa = ItemUtil.extractSetItems(item);
		assertTrue(aa.size()==1);
		assertTrue(aa.contains("test"));
	}
	
	@Test
	public void testExtractSetItems2() {
		Item item = new Item();
		item.setLabel("test;test2");
		Set<String> aa = ItemUtil.extractSetItems(item);
		assertTrue(aa.size()==2);
		assertTrue(aa.contains("test"));
		assertTrue(aa.contains("test2"));
	}
	
	@Test
	public void testExtractSetItems3() {
		Item item = new Item();
		item.setLabel("test/test2");
		Set<String> aa = ItemUtil.extractSetItems(item);
		assertTrue(aa.size()==2);
		assertTrue(aa.contains("test"));
		assertTrue(aa.contains("test2"));
	}
	
	@Test
	public void testExtractSetItems4() {
		Set<String> aa = ItemUtil.extractSetItems(null);
		assertTrue(aa.isEmpty());
		aa = ItemUtil.extractSetItems(new Item());
		assertTrue(aa.isEmpty());
	}
	
	@Test
	public void testExtractRelatedItems1() throws Exception {
		TemplateDefinition aa = RulesUtil.getTemplates(decorObs.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(aa);
		Attribute attr = rd.getAttribute().get(0);
		Set<String> aaaa = ItemUtil.extractRelatedItems(attr);
		assertTrue(aaaa.size()==2);
		assertTrue(aaaa.contains("zz"));
		assertTrue(aaaa.contains("aa"));
	}
	
	@Test
	public void testExtractRelatedItems2() throws Exception {
		Set<String> a = ItemUtil.extractRelatedItems(null);
		assertTrue(a.isEmpty());
		a = ItemUtil.extractRelatedItems(new Vocabulary());
		assertTrue(a.isEmpty());
	}

}
