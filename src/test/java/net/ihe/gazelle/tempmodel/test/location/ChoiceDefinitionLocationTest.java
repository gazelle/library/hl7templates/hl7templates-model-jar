package net.ihe.gazelle.tempmodel.test.location;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.location.util.ChoiceDefinitionLocation;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public class ChoiceDefinitionLocationTest {

	@Test
	public void testGetLocation1() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		RuleDefinition rdfirst = TemplateDefinitionUtil.getFirstElement(td);
		RuleDefinition pp = RuleDefinitionUtil.getElementByName(rdfirst, "hl7:participantRole");
		ChoiceDefinition cd = RuleDefinitionUtil.getChoices(pp).get(0);
		String loc = ChoiceDefinitionLocation.getLocation(cd);
		assertTrue(loc.equals("//rules/template[1]/element[1]/element[3]/choice[1]"));
	}
	
	@Test
	public void testGetLocation2() throws Exception {
		ChoiceDefinition cd = null;
		assertTrue(ChoiceDefinitionLocation.getLocation(cd).equals(""));
		cd = new ChoiceDefinition();
		assertTrue(ChoiceDefinitionLocation.getLocation(cd).equals("/choice"));
	}

}
