package net.ihe.gazelle.tempmodel.test;

import net.ihe.gazelle.tempmodel.gov.model.TemplateRepository;
import net.ihe.gazelle.tempmodel.gov.util.TemplateRepositoryUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BuildingBlockRepository;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Abderrazek Boufahja
 *
 */
public class TemplateRepositoryUtilTest extends TemplateRepositoryUtil {

    private static final Logger LOG = LoggerFactory.getLogger(TemplateRepositoryUtilTest.class);

	Decor decorTemplates = null;

	private static String URL_DIS = "https://art-decor.ihe-europe.net/decor/services/RetrieveProject?prefix=IHE-PHARM-DIS-&version=&mode=compiled&language=&download=true&format=xml";

	private static String URL_PRE = "https://art-decor.ihe-europe.net/decor/services/RetrieveProject?prefix=IHE-PHARM-PRE-&version=&mode=compiled&language=&download=true&format=xml";

	@Before
	public void setUp() {
		decorTemplates = DecorMarshaller.loadDecor("src/test/resources/template2.xml");
	}

	@Test
	public void testGenerateTemplateRepository() throws MalformedURLException {
		TemplateRepository aa = TemplateRepositoryUtil.generateTemplateRepository(decorTemplates);
        LOG.info("Current Decor prefix: {}", aa.getCurrentDecor().getProject().getPrefix());
        LOG.info("Current Decor parent repository number : {}", aa.getListParentRepository().size());
		assertTrue(aa.getCurrentDecor().getProject().getPrefix().equals("ccda-"));
		if (TemplateRepositoryUtil.verifyUrlReachable(new URL("http://www.google.com"))) {
			// verify if internet reachable
			assertTrue(aa.getListParentRepository().size() == 2);
			//FIXME IHE-PPC- return BPPC- project maybe art-decor issue
			//assertTrue(TemplateRepositoryUtil.getTemplateRepositoryByPrefix(aa, "IHE-PCC-") != null);
			assertTrue(TemplateRepositoryUtil.getTemplateRepositoryByPrefix(aa, "ccd1-") != null);
		}
	}

	@Test
	public void testGenerateTemplateRepository2() throws MalformedURLException, FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor(URL_DIS);
		TemplateRepository aa = TemplateRepositoryUtil.generateTemplateRepository(dec);
        LOG.info("Current Decor prefix: {}", aa.getCurrentDecor().getProject().getPrefix());
        LOG.info("Current Decor parent repository number : {}", aa.getListParentRepository().size());
		assertTrue(aa.getCurrentDecor().getProject().getPrefix().equals("IHE-PHARM-DIS-"));
		if (TemplateRepositoryUtil.verifyUrlReachable(new URL("http://www.google.com"))) {
            // verify if internet reachable
			assertTrue(aa.getListParentRepository().size() == 2);
			assertTrue(TemplateRepositoryUtil.getTemplateRepositoryByPrefix(aa, "IHE-PHARM-PRE-") != null);
			assertTrue(TemplateRepositoryUtil.getTemplateRepositoryByPrefix(aa, "IHE-PHARM-PADV-") != null);
		}
	}

    @Test
	public void testGenerateTemplateRepository3() throws MalformedURLException, FileNotFoundException, JAXBException {
		Decor dec = DecorMarshaller.loadDecor(URL_PRE);
		TemplateRepository aa = TemplateRepositoryUtil.generateTemplateRepository(dec);
        LOG.info("Current Decor prefix: {}", aa.getCurrentDecor().getProject().getPrefix());
        LOG.info("Current Decor parent repository number : {}", aa.getListParentRepository().size());
		assertTrue(aa.getCurrentDecor().getProject().getPrefix().equals("IHE-PHARM-PRE-"));
		if (TemplateRepositoryUtil.verifyUrlReachable(new URL("http://www.google.com"))) {
            // verify if internet reachable
            assertTrue(aa.getListParentRepository().size() == 5);
			assertTrue(TemplateRepositoryUtil.getTemplateRepositoryByPrefix(aa, "ccd1-") != null);
			assertTrue(TemplateRepositoryUtil.getTemplateRepositoryByPrefix(aa, "ad1bbr-") != null);
            assertTrue(TemplateRepositoryUtil.getTemplateRepositoryByPrefix(aa, "MTP-") != null);
            assertTrue(TemplateRepositoryUtil.getTemplateRepositoryByPrefix(aa, "IHE-PHARM-DIS-") != null);
            //FIXME IHE-PPC- return BPPC- project maybe art-decor issue
            //assertTrue(TemplateRepositoryUtil.getTemplateRepositoryByPrefix(aa, "IHE-PCC-") != null);
		}
	}

	@Ignore
    @Test
	public void testGenerateTemplateRepositoryFromURL() throws Exception {
		String url = "http://art-decor.org/decor/services/modules/RetrieveProject.xquery?prefix=hl7de-&mode=compiled&language=en-US&format=xml";
		if (TemplateRepositoryUtil.verifyUrlReachable(new URL(url))) {
			TemplateRepository aa = TemplateRepositoryUtil.generateTemplateRepositoryFromURL(url);
			assertTrue(aa.getCurrentDecor().getProject().getPrefix().equals("hl7de-"));
			assertTrue(TemplateRepositoryUtil.extractTemplateRepositoryIfAlreadyProcessed(aa.getCurrentDecor()) != null);
		}
	}

    @Test
	public void testFindTemplateDefinitionById() throws Exception {
		TemplateRepository aa = TemplateRepositoryUtil.generateTemplateRepository(decorTemplates);
		TemplateDefinition td = TemplateRepositoryUtil.findTemplateDefinitionById(aa, "2.16.840.1.113883.2.4.3.11.60.22.10.143");
		assertTrue(td != null);
		assertTrue(td.getName().equals("epSOSCDAMaterial"));
		if (TemplateRepositoryUtil.verifyUrlReachable(new URL("http://www.google.com"))) {
			td = TemplateRepositoryUtil.findTemplateDefinitionById(aa, "2.16.840.1.113883.10.20.1.40");
			assertTrue(td != null);
			assertTrue(td.getName().equals("Comment"));
		}
	}

	@Ignore
    @Test
	public void testCleanMapTemplateRepository() throws Exception {
		String url = "http://art-decor.org/decor/services/modules/RetrieveProject.xquery?prefix=hl7de-&mode=compiled&language=en-US&format=xml";
		if (TemplateRepositoryUtil.verifyUrlReachable(new URL(url))) {
			TemplateRepository aa = TemplateRepositoryUtil.generateTemplateRepositoryFromURL(url);
			assertTrue(aa.getCurrentDecor().getProject().getPrefix().equals("hl7de-"));
			assertTrue(TemplateRepositoryUtil.extractTemplateRepositoryIfAlreadyProcessed(aa.getCurrentDecor()) != null);
			TemplateRepositoryUtil.cleanMapTemplateRepository();
			assertTrue(TemplateRepositoryUtil.extractTemplateRepositoryIfAlreadyProcessed(aa.getCurrentDecor()) == null);
		}
	}

	@Ignore
    @Test
	public void testGetReferenceTemplateIdentifierFromTemplateRepository1() throws Exception {
		TemplateRepositoryUtil.cleanMapTemplateRepository();
		String url = "http://art-decor.org/decor/services/modules/RetrieveProject.xquery?prefix=hl7de-&mode=compiled&language=en-US&format=xml";
		if (TemplateRepositoryUtil.verifyUrlReachable(new URL(url))) {
			TemplateRepository aa = TemplateRepositoryUtil.generateTemplateRepositoryFromURL(url);
			assertTrue(aa.getCurrentDecor().getProject().getPrefix().equals("hl7de-"));
			String abba = TemplateRepositoryUtil.getReferenceTemplateIdentifierFromTemplateRepository(aa, "1.2.276.0.76.10.4075");
			assertTrue(abba.equals("1.2.276.0.76.10.4075"));
			abba = TemplateRepositoryUtil.getReferenceTemplateIdentifierFromTemplateRepository(aa, "CDAtypeId");
			assertTrue(abba.equals("1.2.276.0.76.10.90003"));
			abba = TemplateRepositoryUtil.getReferenceTemplateIdentifierFromTemplateRepository(aa, "2.16.840.1.113883.10.12.400");
			assertTrue(abba.equals("2.16.840.1.113883.10.12.400"));
			abba = TemplateRepositoryUtil.getReferenceTemplateIdentifierFromTemplateRepository(aa, "CDAauthenticator");
			assertTrue(abba.equals("2.16.840.1.113883.10.12.107"));
		}
	}

	@Ignore
    @Test
	public void testGetTemplateDefinitionByIdentifier() throws Exception {
		TemplateRepositoryUtil.cleanMapTemplateRepository();
		String url = "http://art-decor.org/decor/services/modules/RetrieveProject.xquery?prefix=hl7de-&mode=compiled&language=en-US&format=xml";
		if (TemplateRepositoryUtil.verifyUrlReachable(new URL(url))) {
			TemplateRepository aa = TemplateRepositoryUtil.generateTemplateRepositoryFromURL(url);
			assertTrue(aa.getCurrentDecor().getProject().getPrefix().equals("hl7de-"));
			TemplateDefinition abba = TemplateRepositoryUtil.getTemplateDefinitionByIdentifier(aa, "1.2.276.0.76.10.4075");
			assertTrue(abba.getId().equals("1.2.276.0.76.10.4075"));
			abba = TemplateRepositoryUtil.getTemplateDefinitionByIdentifier(aa, "2.16.840.1.113883.10.12.400");
			assertTrue(abba.getId().equals("2.16.840.1.113883.10.12.400"));
		}
	}

    @Test
    public void testCreateUrlBBRExtractionNullBBR() throws Exception {
		BuildingBlockRepository bbr = null;
		String ee = createUrlBBRExtraction(bbr);
        assertNull(ee);
	}

    @Test
    public void testCreateUrlBBRExtractionBBRNoUrl() throws Exception {
		BuildingBlockRepository bbr = new BuildingBlockRepository();
		String ee = createUrlBBRExtraction(bbr);
        assertNull(ee);
	}

    @Test
    public void testCreateUrlBBRExtractionEndingSlash() throws Exception {
		BuildingBlockRepository bbr = new BuildingBlockRepository();
		bbr.setUrl("http://art-decor.org/decor/services/");
		bbr.setIdent("ccda-");
		String ee = createUrlBBRExtraction(bbr);
        assertEquals("http://art-decor.org/decor/services/RetrieveProject?prefix=ccda-&mode=compiled&language=en-US&format=xml", ee);
	}

    @Test
    public void testCreateUrlBBRExtractionNoEndingSlash() throws Exception {
		BuildingBlockRepository bbr = new BuildingBlockRepository();
		bbr.setUrl("http://art-decor.org/decor/services");
		bbr.setIdent("ccda-");
		String ee = createUrlBBRExtraction(bbr);
        assertEquals("http://art-decor.org/decor/services/RetrieveProject?prefix=ccda-&mode=compiled&language=en-US&format=xml", ee);
	}

}
