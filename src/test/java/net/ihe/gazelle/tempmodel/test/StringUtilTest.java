package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.utils.StringUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class StringUtilTest {
	
	@Test
	public void testGetRealNameOfRuleDefinition() throws Exception {
		String name = "hl7:observation";
		String real = StringUtil.getRealNameOfRuleDefinition(name);
		assertTrue(real.equals("observation"));
		name = "hl7:component[hl7:observationMedia]";
		real = StringUtil.getRealNameOfRuleDefinition(name);
		assertTrue(real.equals("component"));
	}
	
	@Test
	public void testcap1stChar() throws Exception {
		String name = "obs";
		String cap = StringUtil.cap1stChar(name);
		assertTrue(cap.equals("Obs"));
		assertTrue(StringUtil.cap1stChar("").equals(""));
	}
	
	@Test
	public void testIsOID() throws Exception {
		assertTrue(StringUtil.isOID("1.2.3"));
		assertFalse(StringUtil.isOID("abd"));
		assertFalse(StringUtil.isOID(null));
		assertFalse(StringUtil.isOID(""));
	}

}
