package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.NegativeDistinguisherList;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.NegativeDistinguisherVerifier;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class NegativeDistinguisherListTest {
	
	@Before
	public void before() {
		NegativeDistinguisherList.updateNegativeDistinguisherVerifier(null);
		NegativeDistinguisherList.cleanMapNegativeDistinguisher();
	}
	

	@Test
	public void testGetTreatNegativeDistinguisher() {
		Boolean aa = NegativeDistinguisherList.getTreatNegativeDistinguisher();
		assertTrue(aa != null);
	}

	@Test
	public void testSetTreatNegativeDistinguisher() {
		NegativeDistinguisherList.cleanMapNegativeDistinguisher();
		NegativeDistinguisherList.setTreatNegativeDistinguisher(null);
		assertTrue(NegativeDistinguisherList.getTreatNegativeDistinguisher() == null);
		NegativeDistinguisherList.setTreatNegativeDistinguisher(false);
		assertFalse(NegativeDistinguisherList.getTreatNegativeDistinguisher());
		NegativeDistinguisherList.setTreatNegativeDistinguisher(true);
		assertTrue(NegativeDistinguisherList.getTreatNegativeDistinguisher());
	}

	@Test
	public void testDefineRuleDefinitionAsNegativeDistinguisher() {
		NegativeDistinguisherList.cleanMapNegativeDistinguisher();
		RuleDefinition rd = new RuleDefinition();
		rd.setName("aa");
		NegativeDistinguisherList.defineRuleDefinitionAsNegativeDistinguisher(rd, true);
		assertTrue(NegativeDistinguisherList.ruleDefinitionHasNegativeDistinguisher(rd));
		RuleDefinition rd2 = new RuleDefinition();
		rd2.setName("bb");
		NegativeDistinguisherList.defineRuleDefinitionAsNegativeDistinguisher(rd2, false);
		assertFalse(NegativeDistinguisherList.ruleDefinitionHasNegativeDistinguisher(rd2));
		RuleDefinition rd3 = new RuleDefinition();
		rd3.setName("cc");
		assertNull(NegativeDistinguisherList.ruleDefinitionHasNegativeDistinguisher(rd3));
	}

	@Test
	public void testRuleDefinitionHasNegativeDistinguisher() {
		NegativeDistinguisherList.cleanMapNegativeDistinguisher();
		RuleDefinition rd = new RuleDefinition();
		rd.setName("aa");
		NegativeDistinguisherList.defineRuleDefinitionAsNegativeDistinguisher(rd, true);
		assertTrue(NegativeDistinguisherList.ruleDefinitionHasNegativeDistinguisher(rd));
		assertNull(NegativeDistinguisherList.ruleDefinitionHasNegativeDistinguisher(null));
		assertNull(NegativeDistinguisherList.ruleDefinitionHasNegativeDistinguisher(new RuleDefinition()));
	}
	
	@Test
	public void testUpdateNegativeDistinguisherVerifier1() throws Exception {
		NegativeDistinguisherVerifier ndv = new NegativeDistinguisherVerifier() {
			
			@Override
			public boolean acceptNegativeDistinguisher(RuleDefinition ruleDefinition) {
				return true;
			}
		};
		NegativeDistinguisherList.updateNegativeDistinguisherVerifier(ndv);
		RuleDefinition rd = new RuleDefinition();
		NegativeDistinguisherList.defineRuleDefinitionAsNegativeDistinguisher(rd, true);
		assertTrue(NegativeDistinguisherList.ruleDefinitionHasNegativeDistinguisher(rd));
	}
	
	@Test
	public void testUpdateNegativeDistinguisherVerifier2() throws Exception {
		NegativeDistinguisherVerifier ndv = new NegativeDistinguisherVerifier() {
			
			@Override
			public boolean acceptNegativeDistinguisher(RuleDefinition ruleDefinition) {
				return false;
			}
		};
		NegativeDistinguisherList.updateNegativeDistinguisherVerifier(ndv);
		RuleDefinition rd = new RuleDefinition();
		NegativeDistinguisherList.defineRuleDefinitionAsNegativeDistinguisher(rd, true);
		assertFalse(NegativeDistinguisherList.ruleDefinitionHasNegativeDistinguisher(rd));
	}
	
	@Test
	public void testCleanMapNegativeDistinguisher() throws Exception {
		NegativeDistinguisherList.updateNegativeDistinguisherVerifier(null);
		RuleDefinition rd = new RuleDefinition();
		NegativeDistinguisherList.defineRuleDefinitionAsNegativeDistinguisher(rd, true);
		assertTrue(NegativeDistinguisherList.ruleDefinitionHasNegativeDistinguisher(rd));
		NegativeDistinguisherList.cleanMapNegativeDistinguisher();
		assertNull(NegativeDistinguisherList.ruleDefinitionHasNegativeDistinguisher(rd));
	}

}
