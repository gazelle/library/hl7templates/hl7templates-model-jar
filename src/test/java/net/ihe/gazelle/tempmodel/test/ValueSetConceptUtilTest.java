package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VocabType;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ValueSetConceptUtil;

public class ValueSetConceptUtilTest {

	@Test
	public void testListValueSetConceptContainCode() {
		ValueSetConcept vsc1 = new ValueSetConcept();
		vsc1.setCode("A1");
		vsc1.setCodeSystem("11111");
		ValueSetConcept vsc2 = new ValueSetConcept();
		vsc2.setCode("A2");
		vsc2.setCodeSystem("11111");
		List<ValueSetConcept> lvsc = new ArrayList<ValueSetConcept>();
		lvsc.add(vsc1);
		lvsc.add(vsc2);
		assertTrue(ValueSetConceptUtil.listValueSetConceptContainCode("A1", lvsc));
		assertFalse(ValueSetConceptUtil.listValueSetConceptContainCode("BBBB", lvsc));
		assertFalse(ValueSetConceptUtil.listValueSetConceptContainCode(null, lvsc));
		assertFalse(ValueSetConceptUtil.listValueSetConceptContainCode("AA", null));
	}
	
	@Test
	public void testValueSetConceptIsNotAbstract() throws Exception {
		ValueSetConcept vsc1 = new ValueSetConcept();
		vsc1.setCode("A1");
		vsc1.setCodeSystem("11111");
		assertTrue(ValueSetConceptUtil.valueSetConceptIsNotAbstract(vsc1));
		vsc1.setType(VocabType.D);
		assertTrue(ValueSetConceptUtil.valueSetConceptIsNotAbstract(vsc1));
		vsc1.setType(VocabType.A);
		assertFalse(ValueSetConceptUtil.valueSetConceptIsNotAbstract(vsc1));
	}

}
