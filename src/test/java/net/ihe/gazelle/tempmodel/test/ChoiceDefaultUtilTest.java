package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.choices.model.ChoiceDefault;
import net.ihe.gazelle.tempmodel.choices.model.ChoiceElement;
import net.ihe.gazelle.tempmodel.choices.utils.ChoiceDefaultUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ChoiceDefaultUtilTest extends ChoiceDefaultUtil {

	@Test
	public void testFindChoiceDefaultByTypeAndSubelements1() {
		ChoiceDefault dd = ChoiceDefaultUtil.findChoiceDefaultByTypeAndSubelements("POCDMT000040Component2", 
				Arrays.asList(new String[] {"nonXMLBody", "structuredBody"}));
		assertTrue(dd != null);
		dd = ChoiceDefaultUtil.findChoiceDefaultByTypeAndSubelements("POCDMT000040Component4", 
				Arrays.asList(new String[] {"encounter", "observationMedia"}));
		assertTrue(dd != null);
		dd = ChoiceDefaultUtil.findChoiceDefaultByTypeAndSubelements("POCDMT000040Component4", 
				Arrays.asList(new String[] {"encounter", "observationMedia", "toto"}));
		assertTrue(dd == null);
	}
	
	@Test
	public void testFindChoiceDefaultByTypeAndSubelements2() {
		assertTrue(ChoiceDefaultUtil.findChoiceDefaultByTypeAndSubelements(null, null) == null);
		assertTrue(ChoiceDefaultUtil.findChoiceDefaultByTypeAndSubelements("POCDMT000040Component2", null) == null);
		assertTrue(ChoiceDefaultUtil.findChoiceDefaultByTypeAndSubelements(null , Arrays.asList(new String[] {"nonXMLBody", "structuredBody"})) == null);
	}

	@Test
	public void testListSubElementsContainsAll() {
		ChoiceDefault cdef = new ChoiceDefault();
		cdef.setType("test");
		cdef.getChoiceElement().add(new ChoiceElement());
		cdef.getChoiceElement().add(new ChoiceElement());
		cdef.getChoiceElement().get(0).setName("1");
		cdef.getChoiceElement().get(1).setName("2");
		assertTrue(listSubElementsContainsAll(cdef, Arrays.asList(new String[] {"1", "2"})));
		assertFalse(listSubElementsContainsAll(cdef, Arrays.asList(new String[] {"1", "3"})));
	}

	@Test
	public void testListSubElementsContains() {
		ChoiceDefault cdef = new ChoiceDefault();
		cdef.setType("test");
		cdef.getChoiceElement().add(new ChoiceElement());
		cdef.getChoiceElement().add(new ChoiceElement());
		cdef.getChoiceElement().get(0).setName("1");
		cdef.getChoiceElement().get(1).setName("2");
		assertTrue(listSubElementsContains(cdef, "2"));
		assertFalse(listSubElementsContains(cdef, "aa"));
	}
	
}
