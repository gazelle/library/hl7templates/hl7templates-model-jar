package net.ihe.gazelle.tempmodel.test;

import net.ihe.gazelle.goc.uml.utils.PathWithDT;
import net.ihe.gazelle.goc.uml.utils.PathWithDTUtil;
import net.ihe.gazelle.tempmodel.dpath.model.DAttribute;
import net.ihe.gazelle.tempmodel.dpath.model.DElement;
import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefineVariable;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Report;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ContainDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.Pair;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ValueSetConceptUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Abderrazek Boufahja
 */
public class RuleDefinitionUtilTest extends RuleDefinitionUtil {

    Decor decorTemplatesComplex = null;

    RuleDefinition firstRD = null;
    RuleDefinition entr = null;
    RuleDefinition obs = null;
    RuleDefinition targetSiteCode = null;

    Decor decorTemplates = null;

    Decor decorTemplateChoice = null;

    @Before
    public void setUp() {
        decorTemplateChoice = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
        decorTemplates = DecorMarshaller.loadDecor("src/test/resources/template1.xml");
        decorTemplatesComplex = DecorMarshaller.loadDecor("src/test/resources/decor_obs.xml");

        firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplatesComplex.getRules()).get(0));
        entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
        targetSiteCode = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
    }

    @Test
    public void getJavaPathTest1() {
        assertTrue(PathWithDTUtil.extractPathFromListPathWithDT(RuleDefinitionUtil.getJavaPath(firstRD)).equals("POCDMT000040Act"));
        System.out.println(RuleDefinitionUtil.getJavaPath(entr));
        System.out.println(PathWithDTUtil.extractPathFromListPathWithDT(RuleDefinitionUtil.getJavaPath(entr)));
        assertTrue(PathWithDTUtil.extractPathFromListPathWithDT(RuleDefinitionUtil.getJavaPath(entr)).equals("POCDMT000040Act.entryRelationship"));
        assertTrue(PathWithDTUtil.extractPathFromListPathWithDT(RuleDefinitionUtil.getJavaPath(obs)).equals("POCDMT000040Act.entryRelationship" +
                ".observation"));
        assertTrue(PathWithDTUtil.extractPathFromListPathWithDT(RuleDefinitionUtil.getJavaPath(targetSiteCode)).equals("POCDMT000040Act" +
                ".entryRelationship.observation.targetSiteCode"));
    }

    @Test
    public void getJavaPathTest2() {
        RuleDefinition first = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
        // hl7:templateId
        RuleDefinition first_first = RuleDefinitionUtil.getElements(first).get(0);
        assertTrue(PathWithDTUtil.extractPathFromListPathWithDT(RuleDefinitionUtil.getJavaPath(first_first)).equals("POCDMT000040Section" +
                ".templateId"));
        assertTrue(PathWithDTUtil.extractPathFromListPathWithDT(RuleDefinitionUtil.getJavaPath(first)).equals("POCDMT000040Section"));
    }

    @Test
    public void testGetElements() throws Exception {
        RuleDefinition first = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
        assertTrue(RuleDefinitionUtil.getElements(first).size() == 9);
    }

    @Test
    public void testGetParentTemplateDefinition() throws Exception {
        RuleDefinition first = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
        RuleDefinition first_first = RuleDefinitionUtil.getElements(first).get(0);
        assertTrue(RuleDefinitionUtil.getParentTemplateDefinition(first).getId().equals("1.3.6.1.4.1.19376.1.5.3.1.3.16.1"));
        assertTrue(RuleDefinitionUtil.getParentTemplateDefinition(first).equals(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
        assertTrue(RuleDefinitionUtil.getParentTemplateDefinition(first_first).getId().equals("1.3.6.1.4.1.19376.1.5.3.1.3.16.1"));
        assertTrue(RuleDefinitionUtil.getParentTemplateDefinition(first_first).equals(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)));
    }

    @Test
    public void testGetDParentOfRuleDefinition1() throws Exception {
        RuleDefinition first = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
        RuleDefinition first_first = RuleDefinitionUtil.getElements(first).get(0);
        DParent dp = RuleDefinitionUtil.getDParentOfRuleDefinition(first);
        String repr = DPathExtractor.createPathFromDParent(dp);
        assertTrue(repr.equals("/hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']"));
        dp = RuleDefinitionUtil.getDParentOfRuleDefinition(first_first);
        repr = DPathExtractor.createPathFromDParent(dp);
        assertTrue(repr.equals("/hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3" +
                ".1.3.16.1']"));
    }

    @Test
    public void testGetDParentOfRuleDefinition2() throws Exception {
        DParent dp = RuleDefinitionUtil.getDParentOfRuleDefinition(firstRD);
        String repr = DPathExtractor.createPathFromDParent(dp);
        assertTrue(repr.equals("/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2']"));
        dp = RuleDefinitionUtil.getDParentOfRuleDefinition(entr);
        repr = DPathExtractor.createPathFromDParent(dp);
        System.out.println(repr);
        assertTrue(repr.equals("/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5" +
                ".2']/hl7:entryRelationship[hl7:observation/hl7:code/@code='48766-0']"));
        dp = RuleDefinitionUtil.getDParentOfRuleDefinition(obs);
        repr = DPathExtractor.createPathFromDParent(dp);
        assertTrue(repr.equals("/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5" +
                ".2']/hl7:entryRelationship[hl7:observation/hl7:code/@code='48766-0']/hl7:observation"));
        dp = RuleDefinitionUtil.getDParentOfRuleDefinition(targetSiteCode);
        repr = DPathExtractor.createPathFromDParent(dp);
        assertTrue(repr.equals("/hl7:act[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5" +
                ".2']/hl7:entryRelationship[hl7:observation/hl7:code/@code='48766-0']/hl7:observation/hl7:targetSiteCode"));
    }

    @Test
    public void testGetDParentOfRuleDefinition3() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting5.xml");
        RuleDefinition section = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition entr = RuleDefinitionUtil.getElementByName(section, "hl7:entry");
        DParent dp = RuleDefinitionUtil.getDParentOfRuleDefinition(entr);
        String repr = DPathExtractor.createPathFromDParent(dp);
        assertTrue(repr.equals("/hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:entry[@typeCode='COMP']"));
        RuleDefinition act = RuleDefinitionUtil.getElementByName(entr, "hl7:act");
        dp = RuleDefinitionUtil.getDParentOfRuleDefinition(act);
        repr = DPathExtractor.createPathFromDParent(dp);
        assertTrue(repr.equals("/hl7:section[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:entry[@typeCode='COMP']/hl7:act"));
    }

    @Test
    public void testGetDParentOfRuleDefinition4() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_part1.xml");
        RuleDefinition participant = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition awarenessCode = RuleDefinitionUtil.getElementByName(participant, "hl7:awarenessCode");
        DParent dp = RuleDefinitionUtil.getDParentOfRuleDefinition(awarenessCode);
        String repr = DPathExtractor.createPathFromDParent(dp);
        assertTrue(repr.equals("/hl7:participant/hl7:awarenessCode"));
        dp = RuleDefinitionUtil.getDParentOfRuleDefinition(participant);
        repr = DPathExtractor.createPathFromDParent(dp);
        assertTrue(repr.equals("/hl7:participant"));
    }

    @Test
    public void testGetDParentOfRuleDefinition5() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_part2.xml");
        RuleDefinition participant = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition awarenessCode = RuleDefinitionUtil.getElementByName(participant, "hl7:awarenessCode");
        DParent dp = RuleDefinitionUtil.getDParentOfRuleDefinition(awarenessCode);
        String repr = DPathExtractor.createPathFromDParent(dp);
        assertTrue(repr.equals("/hl7:participant[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']/hl7:awarenessCode"));
        dp = RuleDefinitionUtil.getDParentOfRuleDefinition(participant);
        repr = DPathExtractor.createPathFromDParent(dp);
        assertTrue(repr.equals("/hl7:participant[hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']"));
        assertTrue(((DElement) dp).getExtendedType().equals("POCDMT000040Participant1"));
    }

    @Test
    public void testGetDParentOfRuleDefinition7() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_contain_bbr.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition comp = RuleDefinitionUtil.getElementByName(firstRD, "cda:component");
        RuleDefinition strc = RuleDefinitionUtil.getElementByName(comp, "cda:structuredBody");
        RuleDefinition comp2 = RuleDefinitionUtil.getElementByName(strc, "cda:component");
        ContainDefinition containDefinition = RuleDefinitionUtil.getContains(comp2).get(0);
        processLocal(containDefinition);

        RuleDefinition templateId = ContainDefinitionUtil.getTemplateIdElement(containDefinition);
        templateId.setParentObject(containDefinition);
        DParent dp = RuleDefinitionUtil.getDParentOfRuleDefinition(templateId);
        String repr = DPathExtractor.createPathFromDParent(dp);
        assertEquals("/hl7:ClinicalDocument[hl7:templateId/@root='2.16.840.1.113883.10.20.22.1" +
                ".9']/hl7:component/hl7:structuredBody/hl7:component[hl7:section/hl7:templateId/@root='2.16.840.1.113883.10.20.21.2" +
                ".1']/hl7:templateId", repr);
    }

    @Test
    public void testExtractValueDPathFromRuleDefinition1() throws Exception {
        DParent dparent = DPathExtractor.extractDElementFromDPath("/hl7:code/@code");
        String value = RuleDefinitionUtil.extractValueDPathFromRuleDefinition(dparent, obs);
        assertTrue(value.equals("48766-0"));
        dparent = DPathExtractor.extractDElementFromDPath("/hl7:observation/hl7:code/@code");
        value = RuleDefinitionUtil.extractValueDPathFromRuleDefinition(dparent, entr);
        assertTrue(value.equals("48766-0"));
        dparent = DPathExtractor.extractDElementFromDPath("/hl7:observation/hl7:code/@codeSystem");
        value = RuleDefinitionUtil.extractValueDPathFromRuleDefinition(dparent, entr);
        assertTrue(value == null);
    }

    @Test
    public void testExtractValueDPathFromRuleDefinition2() throws Exception {
        RuleDefinition sectionElement = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
        RuleDefinition templateId1 = RuleDefinitionUtil.getElementsByName(sectionElement, "hl7:templateId").get(0);
        RuleDefinition templateId2 = RuleDefinitionUtil.getElementsByName(sectionElement, "hl7:templateId").get(1);
        DParent dparent = DPathExtractor.extractDElementFromDPath("/hl7:templateId/@root");
        String value = RuleDefinitionUtil.extractValueDPathFromRuleDefinition(dparent, sectionElement);
        assertTrue(value.equals("1.3.6.1.4.1.19376.1.5.3.1.3.16.1"));
        dparent = DPathExtractor.extractDElementFromDPath("/@root");
        value = RuleDefinitionUtil.extractValueDPathFromRuleDefinition(dparent, templateId1);
        assertTrue(value.equals("1.3.6.1.4.1.19376.1.5.3.1.3.16.1"));
        value = RuleDefinitionUtil.extractValueDPathFromRuleDefinition(dparent, templateId2);
        assertTrue(value.equals("1.3.6.1.4.1.19376.1.5.3.1.3.16"));
    }

    @Test
    public void testGetRealNameOfRuleDefinition() throws Exception {
        RuleDefinition sectionElement = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
        RuleDefinition templateId1 = RuleDefinitionUtil.getElementsByName(sectionElement, "hl7:templateId").get(0);
        assertTrue(RuleDefinitionUtil.getRealNameOfRuleDefinition(sectionElement).equals("section"));
        assertTrue(RuleDefinitionUtil.getRealNameOfRuleDefinition(templateId1).equals("templateId"));
    }

    @Test
    public void testgetAttributeByName1() throws Exception {
        RuleDefinition sectionElement = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
        RuleDefinition templateId1 = RuleDefinitionUtil.getElementsByName(sectionElement, "hl7:templateId").get(0);
        Attribute attr = RuleDefinitionUtil.getAttributeByName(templateId1, "root");
        assertTrue(attr != null);
        assertTrue(attr.getName().equals("root"));
        attr = RuleDefinitionUtil.getAttributeByName(sectionElement, "classCode");
        assertTrue(attr != null);
        assertTrue(attr.getName().equals("classCode"));
    }

    @Test
    public void testgetAttributeByName2() throws Exception {
        Attribute attr = RuleDefinitionUtil.getAttributeByName(firstRD, "classCode");
        assertTrue(attr != null);
        assertTrue(attr.getName().equals("classCode"));
        attr = RuleDefinitionUtil.getAttributeByName(firstRD, "moodCode");
        assertTrue(attr != null);
        assertTrue(attr.getName().equals("moodCode"));
        attr = RuleDefinitionUtil.getAttributeByName(obs, "classCode");
        assertTrue(attr != null);
        assertTrue(attr.getClassCode() != null);
    }

    @Test
    public void testGetElementByName() throws Exception {
        assertTrue(entr != null && entr.getName().equals("hl7:entryRelationship"));
    }

    @Test
    public void testGetElementsByName() throws Exception {
        RuleDefinition sectionElement = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
        List<RuleDefinition> templateIds = RuleDefinitionUtil.getElementsByName(sectionElement, "hl7:templateId");
        assertTrue(templateIds.size() == 3);
    }

    @Test
    public void testExtractValueDistinguisherFromRuleDefinition() throws Exception {
        RuleDefinition sectionElement = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
        RuleDefinition templateId1 = RuleDefinitionUtil.getElementsByName(sectionElement, "hl7:templateId").get(0);
        List<Pair<String, String>> val = RuleDefinitionUtil.extractValuesDistinguishersFromRuleDefinition(templateId1);
        assertTrue(val.get(0).getObject2().equals("1.3.6.1.4.1.19376.1.5.3.1.3.16.1"));
    }

    @Test
    public void testGetIncludes() throws Exception {
        RuleDefinition sectionElement = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
        List<IncludeDefinition> incs = RuleDefinitionUtil.getIncludes(sectionElement);
        assertTrue(incs.size() == 1);
    }

    @Test
    public void testgetListAllowedSubElements() throws Exception {
        RuleDefinition sectionElement = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
        assertTrue(RuleDefinitionUtil.getListAllowedSubElements(sectionElement).size() == 6);
        RuleDefinition id = RuleDefinitionUtil.getElementByName(sectionElement, "hl7:id");
        assertTrue(RuleDefinitionUtil.getListAllowedSubElements(id).size() == 0);
    }

    @Test
    public void testgetAbsoluteSubElements1() throws Exception {
        RuleDefinition sectionElement = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplateChoice.getRules()).get(0)).get(0);
        RuleDefinition rd = RuleDefinitionUtil.getElementByName(sectionElement, "hl7:participantRole");
        assertTrue(RuleDefinitionUtil.getAbsoluteSubElements(rd).size() == 6);
        RuleDefinition id = RuleDefinitionUtil.getElementByName(sectionElement, "hl7:time");
        assertTrue(RuleDefinitionUtil.getAbsoluteSubElements(id).size() == 0);
    }

    @Test
    public void testgetAbsoluteSubElements2() throws Exception {
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplateChoice.getRules()).get(0));
        RuleDefinition participantRole = RuleDefinitionUtil.getElementByName(firstRD, "hl7:participantRole");
        assertTrue(RuleDefinitionUtil.getAbsoluteSubElements(participantRole).size() == 6);
    }

    @Test
    public void testGetListCompleteSubElements() throws Exception {
        RuleDefinition sectionElement = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
        System.out.println(RuleDefinitionUtil.getListCompleteSubElements(sectionElement).size());
        assertTrue(RuleDefinitionUtil.getListCompleteSubElements(sectionElement).size() == 14);
    }

    @Test
    public void testRuleDefinitionIsCompletelyClosed() throws Exception {
        RuleDefinition sectionElement = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
        assertFalse(RuleDefinitionUtil.ruleDefinitionIsCompletelyClosed(sectionElement));
        RuleDefinition entry1 = RuleDefinitionUtil.getElementsByName(sectionElement, "hl7:entry").get(0);
        assertTrue(RuleDefinitionUtil.ruleDefinitionIsCompletelyClosed(entry1));
        assertFalse(RuleDefinitionUtil.ruleDefinitionIsCompletelyClosed(RuleDefinitionUtil.getElements(entry1).get(0)));
    }

    @Test
    public void testGetContains() throws Exception {
        RuleDefinition sectionElement = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplates.getRules()).get(0)).get(0);
        RuleDefinition entry1 = RuleDefinitionUtil.getElementsByName(sectionElement, "hl7:entry").get(0);
        assertTrue(RuleDefinitionUtil.getContains(entry1).size() == 1);
    }

    @Test
    public void testGetJavaPathA() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition observation = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition auth = RuleDefinitionUtil.getElementByName(observation, "cda:author");
        RuleDefinition time = RuleDefinitionUtil.getElementByName(auth, "cda:time");
        List<PathWithDT> aa = RuleDefinitionUtil.getJavaPath(time);
        System.out.println(aa);
        System.out.println(PathWithDTUtil.extractTypedPathFromListPathWithDT(aa));
        assertTrue(PathWithDTUtil.extractTypedPathFromListPathWithDT(aa).equals("PIVLTS"));
    }

    @Test
    public void testGetConstrainedUMLTypeNameA() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_integ.xml");
        RuleDefinition supply = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition effectiveTime = RuleDefinitionUtil.getElementByName(supply, "cda:effectiveTime");
        assertTrue(RuleDefinitionUtil.getConstrainedUMLTypeName(effectiveTime).equals("IVLTS"));
        assertTrue(RuleDefinitionUtil.getConstrainedUMLTypeName(supply).equals("POCDMT000040Supply"));
    }

    @Test
    public void testGetUMLTypeNameA() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_integ.xml");
        RuleDefinition supply = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition effectiveTime = RuleDefinitionUtil.getElementByName(supply, "cda:effectiveTime");
        assertTrue(RuleDefinitionUtil.getUMLTypeName(effectiveTime).equals("SXCMTS"));
        assertTrue(RuleDefinitionUtil.getUMLTypeName(supply).equals("POCDMT000040Supply"));
    }

    @Test
    public void testDTProposedISARealExtension() throws Exception {
        assertTrue(dtProposedISARealExtension("CD", "CS"));
        assertTrue(dtProposedISARealExtension("TS", "IVLTS"));
        assertTrue(dtProposedISARealExtension("SXCMTS", "IVLTS"));
        assertFalse(dtProposedISARealExtension("II", "CD"));
        assertFalse(dtProposedISARealExtension(null, "CD"));
        assertFalse(dtProposedISARealExtension("II", null));
    }

    @Test
    public void testGetExtendedUMLTypeNameOrNull() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_integ.xml");
        RuleDefinition supply = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition effectiveTime = RuleDefinitionUtil.getElementByName(supply, "cda:effectiveTime");
        String res = RuleDefinitionUtil.getExtendedUMLTypeNameOrNull(effectiveTime,
                RuleDefinitionUtil.getJavaPath(supply));
        assertTrue(res.equals("IVLTS"));
        res = RuleDefinitionUtil.getExtendedUMLTypeNameOrNull(effectiveTime, null);
        assertTrue(res == null);
        effectiveTime.setDatatype(null);
        res = RuleDefinitionUtil.getExtendedUMLTypeNameOrNull(effectiveTime, RuleDefinitionUtil.getJavaPath(supply));
        assertTrue(res == null);
    }

    @Test
    public void testExtractRuleDefinitionOrAttributeFromRuleDefinition1() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_integ.xml");
        RuleDefinition supply = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DParent delement = DPathExtractor.extractDElementFromDPath("/cda:templateId");
        Object aa = RuleDefinitionUtil.extractRuleDefinitionOrAttributeFromRuleDefinition(delement, supply);
        assertTrue(aa instanceof RuleDefinition);
        RuleDefinition rd = (RuleDefinition) aa;
        assertTrue(rd == RuleDefinitionUtil.getElementByName(supply, "cda:templateId"));
    }

    @Test
    public void testExtractRuleDefinitionOrAttributeFromRuleDefinition2() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_integ.xml");
        RuleDefinition supply = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DParent delement = DPathExtractor.extractDElementFromDPath("/cda:templateId/@root");
        Object aa = RuleDefinitionUtil.extractRuleDefinitionOrAttributeFromRuleDefinition(delement, supply);
        assertTrue(aa instanceof Attribute);
        Attribute attr = (Attribute) aa;
        assertTrue(attr == RuleDefinitionUtil.getElementByName(supply, "cda:templateId").getAttribute().get(0));
    }

    @Test
    public void testExtractRuleDefinitionOrAttributeFromRuleDefinition3() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DParent delement = DPathExtractor.extractDElementFromDPath("/cda:templateId[@root='2.16.840.1.113883.10.20.6.2.12']");
        Object aa = RuleDefinitionUtil.extractRuleDefinitionOrAttributeFromRuleDefinition(delement, obs);
        assertTrue(aa instanceof RuleDefinition);
        RuleDefinition rd = (RuleDefinition) aa;
        assertTrue(rd == RuleDefinitionUtil.getElementByName(obs, "cda:templateId"));
    }

    @Test
    public void testExtractRuleDefinitionOrAttributeFromRuleDefinition4() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DParent delement = DPathExtractor.extractDElementFromDPath("/cda:templateId[@root='aaaaaaaaa']");
        Object aa = RuleDefinitionUtil.extractRuleDefinitionOrAttributeFromRuleDefinition(delement, obs);
        assertTrue(aa == null);
    }

    @Test
    public void testExtractRuleDefinitionOrAttributeFromRuleDefinition5() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DParent delement = DPathExtractor.extractDElementFromDPath("/cda:author/cda:time");
        Object aa = RuleDefinitionUtil.extractRuleDefinitionOrAttributeFromRuleDefinition(delement, obs);
        assertTrue(aa instanceof RuleDefinition);
        assertTrue(((RuleDefinition) aa).getName().equals("cda:time"));
    }

    @Test
    public void testExtractRuleDefinitionOrAttributeFromRuleDefinition6() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DParent delement = DPathExtractor.extractDElementFromDPath("/hl7:entryRelationship[@inversionInd='true']/@seperatableInd");
        Object aa = RuleDefinitionUtil.extractRuleDefinitionOrAttributeFromRuleDefinition(delement, obs);
        assertTrue(aa instanceof Attribute);
        assertTrue(((Attribute) aa).isProhibited().equals(true));
    }

    @Test
    public void testExtractRuleDefinitionOrAttributeFromRuleDefinition7() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DParent delement = DPathExtractor.extractDElementFromDPath("/hl7:entryRelationship[@inversionInd='false']/@seperatableInd");
        Object aa = RuleDefinitionUtil.extractRuleDefinitionOrAttributeFromRuleDefinition(delement, obs);
        assertTrue(aa == null);
    }

    @Test
    public void testExtractRuleDefinitionOrAttributeFromRuleDefinition8() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DParent delement = DPathExtractor.extractDElementFromDPath("/hl7:entryRelationship/@inversionInd/@seperatableInd");
        assertTrue(delement == null);
        Object aa = RuleDefinitionUtil.extractRuleDefinitionOrAttributeFromRuleDefinition(delement, obs);
        assertTrue(aa == null);
    }

    @Test
    public void testVerifyIfDElementConvertible1() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DElement del = new DElement();
        del.setName("observation");
        del.setDistinguisherAttributeOrElement(new DElement());
        ((DElement) del.getDistinguisherAttributeOrElement()).setName("templateId");
        assertTrue(verifyIfDElementConvertible(del, obs));
    }

    @Test
    public void testVerifyIfDElementConvertible2() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DElement del = new DElement();
        del.setName("act");
        del.setDistinguisherAttributeOrElement(new DElement());
        ((DElement) del.getDistinguisherAttributeOrElement()).setName("templateId");
        assertFalse(verifyIfDElementConvertible(del, obs));
    }

    @Test
    public void testVerifyIfDElementConvertible3() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DElement del = new DElement();
        del.setName("observation");
        del.setDistinguisherAttributeOrElement(new DElement());
        ((DElement) del.getDistinguisherAttributeOrElement()).setName("xxxx");
        assertFalse(verifyIfDElementConvertible(del, obs));
    }

    @Test
    public void testVerifyIfDElementConvertible4() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DElement del = new DElement();
        del.setName("observation");
        del.setDistinguisherAttributeOrElement(new DAttribute());
        ((DAttribute) del.getDistinguisherAttributeOrElement()).setName("classCode");
        ((DAttribute) del.getDistinguisherAttributeOrElement()).setValue("OBS");
        assertTrue(verifyIfDElementConvertible(del, obs));
    }

    @Test
    public void testIncludeDParentInRuleDefinition1() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DElement del = new DElement();
        del.setName("id");
        RuleDefinitionUtil.includeDParentInRuleDefinition(del, obs);
        RuleDefinition id = RuleDefinitionUtil.getElementByName(obs, "id");
        assertTrue(id != null);
        DAttribute datr = new DAttribute();
        datr.setName("root");
        datr.setValue("aaaa");
        RuleDefinitionUtil.includeDParentInRuleDefinition(datr, id);
        assertTrue(id.getAttribute().size() == 1);
        assertTrue(id.getAttribute().get(0).getValue().equals("aaaa"));
    }

    @Test
    public void testIncludeDParentInRuleDefinition2() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DElement del = new DElement();
        del.setName("id");
        DAttribute datr = new DAttribute();
        datr.setName("root");
        datr.setValue("aaaa");
        del.setFollowingAttributeOrElement(datr);
        RuleDefinitionUtil.includeDParentInRuleDefinition(del, obs);
        RuleDefinition id = RuleDefinitionUtil.getElementByName(obs, "id");
        assertTrue(id.getAttribute().size() == 1);
        assertTrue(id.getAttribute().get(0).getValue().equals("aaaa"));
    }

    @Test
    public void testIncludeDParentInRuleDefinition3() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DElement del = new DElement();
        RuleDefinitionUtil.includeDParentInRuleDefinition(null, obs);
        RuleDefinitionUtil.includeDParentInRuleDefinition(del, obs);
    }

    @Test
    public void testIncludingDParentInRuleDefinitionIsPossible1() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DElement del = new DElement();
        del.setName("cda:templateId");
        assertFalse(RuleDefinitionUtil.includingDParentInRuleDefinitionIsPossible(del, obs));
    }

    @Test
    public void testIncludingDParentInRuleDefinitionIsPossible2() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DElement del = new DElement();
        assertFalse(RuleDefinitionUtil.includingDParentInRuleDefinitionIsPossible(del, obs));
        assertFalse(RuleDefinitionUtil.includingDParentInRuleDefinitionIsPossible(null, obs));
        assertFalse(RuleDefinitionUtil.includingDParentInRuleDefinitionIsPossible(del, null));
        assertFalse(RuleDefinitionUtil.includingDParentInRuleDefinitionIsPossible(null, null));
    }

    @Test
    public void testIncludingDParentInRuleDefinitionIsPossible3() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs3.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        DAttribute dattr = new DAttribute();
        assertFalse(RuleDefinitionUtil.includingDParentInRuleDefinitionIsPossible(dattr, obs));
        dattr.setName("classCode");
        dattr.setValue("OBS");
        assertFalse(RuleDefinitionUtil.includingDParentInRuleDefinitionIsPossible(dattr, obs));
        dattr.setName("aaaa");
        dattr.setValue(null);
        assertTrue(RuleDefinitionUtil.includingDParentInRuleDefinitionIsPossible(dattr, obs));
        dattr.setName("classCode");
        dattr.setValue("AAA");
        assertFalse(RuleDefinitionUtil.includingDParentInRuleDefinitionIsPossible(dattr, obs));
    }

    @Test
    public void testExtractListRelatedException1() {
        Decor decorValueSets = DecorMarshaller.loadDecor("src/test/resources/decor_valueset.xml");
        RuleDefinition obs = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorValueSets.getRules()).get(0));
        Set<ValueSetConcept> aa = RuleDefinitionUtil.extractListRelatedException(obs);
        assertTrue(aa == null || aa.size() == 0);
        RuleDefinition val = RuleDefinitionUtil.getElementByName(obs, "hl7:value");
        aa = RuleDefinitionUtil.extractListRelatedException(val);
        assertTrue(aa == null || aa.size() == 0);
    }

    @Test
    public void testExtractListRelatedException2() {
        Decor decorValueSets = DecorMarshaller.loadDecor("src/test/resources/decor_exception2.xml");
        TemplateDefinition td = RulesUtil.getTemplates(decorValueSets.getRules()).get(0);
        RuleDefinition selectedRuleDefinition = TemplateDefinitionUtil.getFirstElement(td);
        selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:participantRole");
        selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:scopingEntity");
        selectedRuleDefinition = RuleDefinitionUtil.getElementByName(selectedRuleDefinition, "hl7:code");
        Set<ValueSetConcept> aa = RuleDefinitionUtil.extractListRelatedException(selectedRuleDefinition);
        assertTrue(aa.size() == 3);
        assertTrue(ValueSetConceptUtil.listValueSetConceptContainCode("UNK", aa));
    }

    @Test
    public void testVerifyIfRDIsACodeDT() {
        Decor decorValueSets = DecorMarshaller.loadDecor("src/test/resources/decor_valueset.xml");
        TemplateDefinition td = RulesUtil.getTemplates(decorValueSets.getRules()).get(0);
        RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
        RuleDefinition status = RuleDefinitionUtil.getElementByName(first, "hl7:statusCode");
        RuleDefinition code = RuleDefinitionUtil.getElementByName(first, "hl7:code");
        RuleDefinition repeatNumber = RuleDefinitionUtil.getElementByName(first, "hl7:repeatNumber");
        RuleDefinition value = RuleDefinitionUtil.getElementByName(first, "hl7:value");
        assertTrue(RuleDefinitionUtil.verifyIfRDIsACodeDT(status));
        assertTrue(RuleDefinitionUtil.verifyIfRDIsACodeDT(code));
        assertTrue(RuleDefinitionUtil.verifyIfRDIsACodeDT(value));
        assertFalse(RuleDefinitionUtil.verifyIfRDIsACodeDT(first));
        assertFalse(RuleDefinitionUtil.verifyIfRDIsACodeDT(repeatNumber));
        assertFalse(RuleDefinitionUtil.verifyIfRDIsACodeDT(null));
    }

    @Test
    public void testGetLets() {
        Decor decorValueSets = DecorMarshaller.loadDecor("src/test/resources/decor_lets.xml");
        TemplateDefinition td = RulesUtil.getTemplates(decorValueSets.getRules()).get(0);
        RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
        List<Let> aa = RuleDefinitionUtil.getLets(first);
        assertTrue(aa.size() == 2);
    }

    @Test
    public void testGetAsserts() {
        Decor decorValueSets = DecorMarshaller.loadDecor("src/test/resources/decor_lets.xml");
        TemplateDefinition td = RulesUtil.getTemplates(decorValueSets.getRules()).get(0);
        RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
        List<Assert> aa = RuleDefinitionUtil.getAsserts(first);
        assertTrue(aa.size() == 1);
    }

    @Test
    public void testGetReportts() {
        Decor decorValueSets = DecorMarshaller.loadDecor("src/test/resources/decor_lets.xml");
        TemplateDefinition td = RulesUtil.getTemplates(decorValueSets.getRules()).get(0);
        RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
        List<Report> aa = RuleDefinitionUtil.getReports(first);
        assertTrue(aa.size() == 1);
    }

    @Test
    public void testGetDefineVariables() throws Exception {
        Decor decorValueSets = DecorMarshaller.loadDecor("src/test/resources/decor_lets.xml");
        TemplateDefinition td = RulesUtil.getTemplates(decorValueSets.getRules()).get(0);
        RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
        List<DefineVariable> aa = RuleDefinitionUtil.getDefineVariables(first);
        assertTrue(aa.size() == 2);
    }

    @Test
    public void testGetParentTemplateDefinition2() throws Exception {
        RuleDefinition sectionElement = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplateChoice.getRules()).get(0)).get(0);
        RuleDefinition rd = RuleDefinitionUtil.getElementByName(sectionElement, "hl7:participantRole");
        RuleDefinition playingDevice = ChoiceDefinitionUtil.getElementsByName(RuleDefinitionUtil.getChoices(rd).get(0), "hl7:playingDevice").get(0);
        assertTrue(RuleDefinitionUtil.getParentTemplateDefinition(playingDevice) == RulesUtil.getTemplates(decorTemplateChoice.getRules()).get(0));
    }

    @Test
    public void testGetDParentOfRuleDefinition6() throws Exception {
        RuleDefinition sectionElement = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplateChoice.getRules()).get(0)).get(0);
        RuleDefinition rd = RuleDefinitionUtil.getElementByName(sectionElement, "hl7:participantRole");
        RuleDefinition playingDevice = ChoiceDefinitionUtil.getElementsByName(RuleDefinitionUtil.getChoices(rd).get(0), "hl7:playingDevice").get(0);
        DParent dp = RuleDefinitionUtil.getDParentOfRuleDefinition(playingDevice);
        assertTrue(dp instanceof DElement);
        assertTrue(((DElement) dp).getName().equals("participant"));
        assertTrue(((DElement) dp).getFollowingAttributeOrElement().getName().equals("participantRole"));
        assertTrue(((DElement) dp).getFollowingAttributeOrElement() instanceof DElement);
        assertTrue(((DElement) ((DElement) dp).getFollowingAttributeOrElement()).getFollowingAttributeOrElement().getName().equals("playingDevice"));
    }

    @Test
    public void testGetDParentOfTheParent() throws Exception {
        RuleDefinition sectionElement = TemplateDefinitionUtil.getElements(RulesUtil.getTemplates(decorTemplateChoice.getRules()).get(0)).get(0);
        RuleDefinition rd = RuleDefinitionUtil.getElementByName(sectionElement, "hl7:participantRole");
        RuleDefinition playingDevice = ChoiceDefinitionUtil.getElementsByName(RuleDefinitionUtil.getChoices(rd).get(0), "hl7:playingDevice").get(0);
        DParent dp = RuleDefinitionUtil.getDParentOfTheParent(playingDevice);
        assertTrue(dp instanceof DElement);
        assertTrue(((DElement) dp).getName().equals("participant"));
        assertTrue(((DElement) dp).getFollowingAttributeOrElement().getName().equals("participantRole"));
        assertTrue(((DElement) dp).getFollowingAttributeOrElement() instanceof DElement);
        assertTrue(((DElement) ((DElement) dp).getFollowingAttributeOrElement()).getFollowingAttributeOrElement() == null);
    }

    @Test
    public void testGetAttributesByNameNull() throws Exception {
        assertTrue(RuleDefinitionUtil.getAttributesByName(null, "aa").isEmpty());
    }

    private void processLocal(ContainDefinition currentContainDefinition) {
        TemplateDefinition td = ContainDefinitionUtil.referencedTemplateDefinitionFromParentBBR(currentContainDefinition);
        if (currentContainDefinition.getParentObject() instanceof RuleDefinition) {
            RuleDefinition replacementRD = new RuleDefinition();
            replacementRD.setName("cda:component");
            replacementRD.setParentObject(currentContainDefinition);
            RuleDefinition templateId = new RuleDefinition();
            templateId.setName("hl7:templateId");
            templateId.getAttribute().add(new Attribute());
            templateId.getAttribute().get(0).setName("root");
            templateId.getAttribute().get(0).setValue(TemplateDefinitionUtil.extractTemplateId(td));
            replacementRD.getLetOrAssertOrReport().add(templateId);
            currentContainDefinition.getLetOrAssertOrReport().add(replacementRD);
        }
    }

}
