package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.handler.util.PropertyProblemUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Property;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public class PropertyProblemUtilTest {

	@Test
	public void testVerifyIfPropertyContainValidCombination() {
		Property prop = new Property();
		prop.setUnit("kg");
		prop.setMinInclude("0");
		assertTrue(PropertyProblemUtil.verifyIfPropertyContainValidCombination(prop));
		prop.setCurrency("euro");
		assertFalse(PropertyProblemUtil.verifyIfPropertyContainValidCombination(prop));
	}

	@Test
	public void testVerifyIfPropertyDeclaredForTheRightElement1() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs4.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
		RuleDefinition firstEntry = RuleDefinitionUtil.getElementsByName(first, "cda:value").get(0);
		Property prop = firstEntry.getProperty().get(0);
		assertTrue(PropertyProblemUtil.verifyIfPropertyDeclaredForTheRightElement(prop));
	}
	
	@Test
	public void testVerifyIfPropertyDeclaredForTheRightElement2() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_obs4.xml");
		TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
		RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
		RuleDefinition firstEntry = RuleDefinitionUtil.getElementsByName(first, "cda:templateId").get(1);
		Property prop = firstEntry.getProperty().get(0);
		assertFalse(PropertyProblemUtil.verifyIfPropertyDeclaredForTheRightElement(prop));
	}
	
	@Test
	public void testVerifyIfPropertyDeclaredForTheRightElement3() {
		assertFalse(PropertyProblemUtil.verifyIfPropertyDeclaredForTheRightElement(new Property()));
		assertFalse(PropertyProblemUtil.verifyIfPropertyDeclaredForTheRightElement(null));
	}

}
