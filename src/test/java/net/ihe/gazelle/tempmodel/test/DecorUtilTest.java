package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class DecorUtilTest {
	
	Decor decorValueSets = null;
	
	@Before
	public void setUp(){
		decorValueSets = DecorMarshaller.loadDecor("src/test/resources/decor_valueset.xml");
	}

	@Test
	public void testContainValueSet() {
		assertTrue(DecorUtil.containValueSet("ObservationMethod", decorValueSets));
		assertTrue(DecorUtil.containValueSet("epSOSPregnancyInformation", decorValueSets));
		assertFalse(DecorUtil.containValueSet("AAAAAAA", decorValueSets));
		assertFalse(DecorUtil.containValueSet("1.3.43", decorValueSets));
	}

	@Test
	public void testGetValueSetOid() {
		assertTrue(DecorUtil.getValueSetOid("ObservationMethod", decorValueSets).equals("2.16.840.1.113883.1.11.14079"));
		assertTrue(DecorUtil.getValueSetOid("1.3.6.1.4.1.12559.11.10.1.3.1.42.9", decorValueSets).equals("1.3.6.1.4.1.12559.11.10.1.3.1.42.9"));
		assertTrue(DecorUtil.getValueSetOid("epSOSPregnancyInformation", decorValueSets).equals("1.3.6.1.4.1.12559.11.10.1.3.1.42.9"));
		assertNull(DecorUtil.getValueSetOid("AAAAAAA", decorValueSets));
		assertNull(DecorUtil.getValueSetOid("1.3.43", decorValueSets));
	}
	
	@Test
	public void testGetDecorFromRD() throws Exception {
		TemplateDefinition td = RulesUtil.getTemplates(decorValueSets.getRules()).get(0);
		RuleDefinition rd = TemplateDefinitionUtil.getFirstElement(td);
		assertTrue(DecorUtil.getDecorFromRD(rd) == decorValueSets);
	}
	
	@Test
	public void testExtractPrefixFromDecor() throws Exception {
		String aa = DecorUtil.extractPrefixFromDecor(decorValueSets);
		assertTrue(aa.equals("ccda"));
		assertTrue(DecorUtil.extractPrefixFromDecor(null) == "");
	}
	
	@Test
	public void testCleanupForVersionLabel1() {
		Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_labeled1.xml");
		DecorUtil.cleanupForVersionLabel(dec, "1.1");
		assertTrue(RulesUtil.getTemplates(dec.getRules()).size() == 1);
	}
	
	@Test
	public void testCleanupForVersionLabel2() {
		Decor dec2 = DecorMarshaller.loadDecor("src/test/resources/decor_labeled1.xml");
		DecorUtil.cleanupForVersionLabel(dec2, "1.0");
		assertTrue(RulesUtil.getTemplates(dec2.getRules()).size() == 1);
	}
	
	@Test
	public void testCleanupForVersionLabel3() {
		Decor dec3 = DecorMarshaller.loadDecor("src/test/resources/decor_labeled1.xml");
		DecorUtil.cleanupForVersionLabel(dec3, "2.0");
		assertTrue(RulesUtil.getTemplates(dec3.getRules()).size() == 2);
	}
	
	@Test
	public void testCleanupForVersionLabel4() {
		Decor dec3 = DecorMarshaller.loadDecor("src/test/resources/decor_labeled1.xml");
		DecorUtil.cleanupForVersionLabel(dec3, "  ");
		assertTrue(RulesUtil.getTemplates(dec3.getRules()).size() == 2);
	}
	
	@Test
	public void testCleanupForVersionLabel5() {
		Decor dec3 = DecorMarshaller.loadDecor("src/test/resources/decor_labeled2.xml");
		DecorUtil.cleanupForVersionLabel(dec3, "1.0");
		assertTrue(RulesUtil.getTemplates(dec3.getRules()).size() == 2);
	}

}
