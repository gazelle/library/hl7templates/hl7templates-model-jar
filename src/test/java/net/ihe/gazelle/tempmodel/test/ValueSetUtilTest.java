package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ValueSetUtil;

public class ValueSetUtilTest {

	@Test
	public void testExtractListCompatibleValueSet() {
		String id = "1";
		List<ValueSet> listOriginal = new ArrayList<>();
		ValueSet vs1 = new ValueSet();
		vs1.setId("1");
		listOriginal.add(vs1);
		ValueSet vs2 = new ValueSet();
		vs2.setId("2");
		listOriginal.add(vs2);
		List<ValueSet> vs = ValueSetUtil.extractListCompatibleValueSet(id, listOriginal);
		assertTrue(vs.size() == 1);
		
	}

}
