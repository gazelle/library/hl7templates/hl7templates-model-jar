package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import net.ihe.gazelle.tempmodel.decor.prop.model.PropertyDescEnum;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Property;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class PropertyDescEnumTest {

	@Test
	public void testGetPropertyDescriber() {
		assertTrue(PropertyDescEnum.PQ_MO.getPropertyDescriber() != null);
	}

	@Test
	public void testExtractPropertyDescEnum1() {
		Property prop = new Property();
		prop.setCurrency("zzz");
		assertTrue(PropertyDescEnum.extractPropertyDescEnum(prop) == PropertyDescEnum.PQ_MO);
	}
	
	@Test
	public void testExtractPropertyDescEnum2() {
		Property prop = new Property();
		prop.setUnit("zzz");
		prop.setMaxInclude("1");
		assertTrue(PropertyDescEnum.extractPropertyDescEnum(prop) == PropertyDescEnum.PQ_UNIT);
	}
	
	@Test
	public void testExtractPropertyDescEnum3() {
		Property prop = new Property();
		prop.setValue("zzz");
		assertTrue(PropertyDescEnum.extractPropertyDescEnum(prop) == PropertyDescEnum.VAL);
	}
	
	@Test
	public void testExtractPropertyDescEnum4() {
		Property prop = new Property();
		prop.setCurrency("zzz");
		prop.setMaxLength(2);
		assertTrue(PropertyDescEnum.extractPropertyDescEnum(prop) == null);
	}
	
	@Test
	public void testContainsDT1() throws Exception {
		assertTrue(PropertyDescEnum.PQ_UNIT.containsDT("PQ"));
		assertTrue(PropertyDescEnum.PQ_UNIT.containsDT("SXCMPPDPQ"));
		assertTrue(PropertyDescEnum.PQ_UNIT.containsDT("IVXBPPDPQ"));
		assertTrue(PropertyDescEnum.PQ_UNIT.containsDT("PPDPQ"));
		assertTrue(PropertyDescEnum.PQ_UNIT.containsDT("HXITPQ"));
		assertTrue(PropertyDescEnum.PQ_UNIT.containsDT("IVLPQ"));
		assertTrue(PropertyDescEnum.PQ_UNIT.containsDT("IVLPPDPQ"));
		assertTrue(PropertyDescEnum.PQ_UNIT.containsDT("SXCMPQ"));
		assertFalse(PropertyDescEnum.PQ_UNIT.containsDT("AA"));
		assertFalse(PropertyDescEnum.PQ_UNIT.containsDT("AD"));
		assertFalse(PropertyDescEnum.PQ_UNIT.containsDT("II"));
		assertFalse(PropertyDescEnum.PQ_UNIT.containsDT("ANY"));
		assertFalse(PropertyDescEnum.PQ_UNIT.containsDT(""));
	}
	
	@Test
	public void testContainsDT2() throws Exception {
		assertTrue(PropertyDescEnum.PQ_MO.containsDT("MO"));
		assertTrue(PropertyDescEnum.PQ_MO.containsDT("SXCMMO"));
		assertTrue(PropertyDescEnum.PQ_MO.containsDT("IVLMO"));
		assertTrue(PropertyDescEnum.PQ_MO.containsDT("IVXBMO"));
		assertFalse(PropertyDescEnum.PQ_MO.containsDT("AA"));
		assertFalse(PropertyDescEnum.PQ_MO.containsDT("AD"));
		assertFalse(PropertyDescEnum.PQ_MO.containsDT("II"));
		assertFalse(PropertyDescEnum.PQ_MO.containsDT("ANY"));
		assertFalse(PropertyDescEnum.PQ_MO.containsDT(""));
	}
	
	@Test
	public void testContainsDT3() throws Exception {
		assertTrue(PropertyDescEnum.STR.containsDT("AD"));
		assertTrue(PropertyDescEnum.STR.containsDT("BIN"));
		assertTrue(PropertyDescEnum.STR.containsDT("EN"));
		assertTrue(PropertyDescEnum.STR.containsDT("AdxpDelimiter"));
		assertTrue(PropertyDescEnum.STR.containsDT("AdxpCensusTract"));
		assertTrue(PropertyDescEnum.STR.containsDT("ENXP"));
		assertTrue(PropertyDescEnum.STR.containsDT("ED"));
		assertTrue(PropertyDescEnum.STR.containsDT("TN"));
		assertTrue(PropertyDescEnum.STR.containsDT("Thumbnail"));
		assertTrue(PropertyDescEnum.STR.containsDT("AdxpBuildingNumberSuffix"));
		assertTrue(PropertyDescEnum.STR.containsDT("EnPrefix"));
		assertTrue(PropertyDescEnum.STR.containsDT("AdxpUnitID"));
		assertTrue(PropertyDescEnum.STR.containsDT("AdxpStreetAddressLine"));
		assertTrue(PropertyDescEnum.STR.containsDT("AdxpHouseNumber"));
		assertTrue(PropertyDescEnum.STR.containsDT("EnDelimiter"));
		assertFalse(PropertyDescEnum.STR.containsDT("AA"));
		assertFalse(PropertyDescEnum.STR.containsDT("TEL"));
		assertFalse(PropertyDescEnum.STR.containsDT("II"));
		assertFalse(PropertyDescEnum.STR.containsDT("ANY"));
		assertFalse(PropertyDescEnum.STR.containsDT(""));
	}
	
	@Test
	public void testContainsDT4() throws Exception {
		assertTrue(PropertyDescEnum.VAL.containsDT("BL"));
		assertTrue(PropertyDescEnum.VAL.containsDT("BN"));
		assertTrue(PropertyDescEnum.VAL.containsDT("CR"));
		assertTrue(PropertyDescEnum.VAL.containsDT("PQ"));
		assertTrue(PropertyDescEnum.VAL.containsDT("PQR"));
		assertTrue(PropertyDescEnum.VAL.containsDT("INT"));
		assertTrue(PropertyDescEnum.VAL.containsDT("MO"));
		assertTrue(PropertyDescEnum.VAL.containsDT("REAL"));
		assertTrue(PropertyDescEnum.VAL.containsDT("URL"));
		assertTrue(PropertyDescEnum.VAL.containsDT("TS"));
		assertTrue(PropertyDescEnum.VAL.containsDT("SXCMPPDPQ"));
		assertTrue(PropertyDescEnum.VAL.containsDT("SXCMPPDTS"));
		assertTrue(PropertyDescEnum.VAL.containsDT("HXITPQ"));
		assertTrue(PropertyDescEnum.VAL.containsDT("BXITIVLPQ"));
		assertTrue(PropertyDescEnum.VAL.containsDT("SXCMMO"));
		assertTrue(PropertyDescEnum.VAL.containsDT("IVXBTS"));
		assertTrue(PropertyDescEnum.VAL.containsDT("IVXBPPDPQ"));
		assertTrue(PropertyDescEnum.VAL.containsDT("IVXBPPDTS"));
		assertTrue(PropertyDescEnum.VAL.containsDT("IVLPQ"));
		assertFalse(PropertyDescEnum.VAL.containsDT("AA"));
		assertFalse(PropertyDescEnum.VAL.containsDT("II"));
		assertFalse(PropertyDescEnum.VAL.containsDT("ANY"));
		assertFalse(PropertyDescEnum.VAL.containsDT(""));
	}

}
