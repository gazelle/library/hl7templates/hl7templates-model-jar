package net.ihe.gazelle.tempmodel.test;

import net.ihe.gazelle.tempmodel.dpath.model.DParent;
import net.ihe.gazelle.tempmodel.dpath.utils.DPathExtractor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ContainDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DecorMarshaller;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.Pair;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionDistinguisherUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Abderrazek Boufahja
 */
public class RuleDefinitionDistinguisherUtilTest extends RuleDefinitionDistinguisherUtil {

    Decor decorTemplatesComplex = null;
    RuleDefinition firstRD = null;
    RuleDefinition entr = null;
    RuleDefinition obs = null;
    RuleDefinition targetSiteCode = null;

    Decor decorTemplates = null;

    Decor decorTemplateChoice = null;

    @Before
    public void setUp() {
        decorTemplateChoice = DecorMarshaller.loadDecor("src/test/resources/decor_choice.xml");
        decorTemplates = DecorMarshaller.loadDecor("src/test/resources/template1.xml");
        decorTemplatesComplex = DecorMarshaller.loadDecor("src/test/resources/decor_obs.xml");

        firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(decorTemplatesComplex.getRules()).get(0));
        entr = RuleDefinitionUtil.getElementByName(firstRD, "hl7:entryRelationship");
        obs = RuleDefinitionUtil.getElementByName(entr, "hl7:observation");
        targetSiteCode = RuleDefinitionUtil.getElementByName(obs, "hl7:targetSiteCode");
    }

    @Test
    public void testExtractUniqueDistinguisher1() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting5.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
        RuleDefinition firstEntry = RuleDefinitionUtil.getElementByName(first, "hl7:entry");
        Pair<String, String> aa = RuleDefinitionDistinguisherUtil.extractUniqueDistinguisher(firstEntry);
        assertTrue(aa != null);
        assertTrue(aa.getObject1().equals("/@typeCode"));
        assertTrue(aa.getObject2().equals("COMP"));
    }

    @Test
    public void testExtractUniqueDistinguisher2() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting5.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
        RuleDefinition firstEntry = RuleDefinitionUtil.getElementsByName(first, "hl7:entry").get(1);
        Pair<String, String> aa = RuleDefinitionDistinguisherUtil.extractUniqueDistinguisher(firstEntry);
        System.out.println(aa);
        assertTrue(aa == null);
    }

    @Test
    public void testExtractUniqueDistinguisher3() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting5.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
        RuleDefinition firstEntry = RuleDefinitionUtil.getElementsByName(first, "hl7:entry").get(2);
        Pair<String, String> aa = RuleDefinitionDistinguisherUtil.extractUniqueDistinguisher(firstEntry);
        System.out.println(aa);
        assertTrue(aa != null);
        assertTrue(aa.getObject1().equals("/hl7:observation/hl7:templateId/@root"));
        assertTrue(aa.getObject2().equals("5.5.5"));
    }

    @Test
    public void testExtractUniqueDistinguisher4() throws FileNotFoundException, JAXBException {
        Pair<String, String> aa = RuleDefinitionDistinguisherUtil.extractUniqueDistinguisher(null);
        assertTrue(aa == null);
    }

    @Test
    public void testExtractUniqueDistinguisher5() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting5.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
        Pair<String, String> aa = RuleDefinitionDistinguisherUtil.extractUniqueDistinguisher(first);
        System.out.println(aa);
        assertTrue(aa != null);
        assertTrue(aa.getObject1().equals("/hl7:templateId/@root"));
        assertTrue(aa.getObject2().equals("1.3.6.1.4.1.19376.1.5.3.1.3.16.1"));
    }

    @Test
    public void testExtractUniqueDistinguisher6() throws FileNotFoundException, JAXBException {
        Pair<String, String> aa = RuleDefinitionDistinguisherUtil.extractUniqueDistinguisher(targetSiteCode);
        assertTrue(aa == null);
    }


    @Test
    public void testExtractAUniqueDistinguisherFromMap1() {
        Map<Pair<String, String>, Integer> mapExistingBroDistinguishers = new HashMap<Pair<String, String>, Integer>();
        Pair<String, String> aa = extractAUniqueDistinguisherFromMap(mapExistingBroDistinguishers);
        assertNull(aa);
    }

    @Test
    public void testExtractAUniqueDistinguisherFromMap2() {
        Map<Pair<String, String>, Integer> mapExistingBroDistinguishers = new HashMap<Pair<String, String>, Integer>();
        mapExistingBroDistinguishers.put(new Pair<String, String>("aa", "bb"), 1);
        mapExistingBroDistinguishers.put(new Pair<String, String>("cc", "ss"), 2);
        Pair<String, String> aa = extractAUniqueDistinguisherFromMap(mapExistingBroDistinguishers);
        assertNotNull(aa);
        assertTrue(aa.getObject1().equals("aa"));
        assertTrue(aa.getObject2().equals("bb"));
    }

    @Test
    public void testExtractAUniqueDistinguisherFromMap3() {
        Map<Pair<String, String>, Integer> mapExistingBroDistinguishers = new HashMap<Pair<String, String>, Integer>();
        mapExistingBroDistinguishers.put(new Pair<String, String>("aa", "bb"), 3);
        mapExistingBroDistinguishers.put(new Pair<String, String>("cc", "ss"), 2);
        Pair<String, String> aa = extractAUniqueDistinguisherFromMap(mapExistingBroDistinguishers);
        assertNull(aa);
    }

    @Test
    public void testExtractListBrodhers1() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting5.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
        RuleDefinition firstEntry = RuleDefinitionUtil.getElementsByName(first, "hl7:entry").get(0);
        List<RuleDefinition> aa = extractListBrodhers(firstEntry, "hl7:entry");
        assertTrue(aa.size() == 3);
    }

    @Test
    public void testExtractListBrodhers2() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting5.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
        RuleDefinition firstEntry = RuleDefinitionUtil.getElementsByName(first, "hl7:templateId").get(0);
        List<RuleDefinition> aa = extractListBrodhers(firstEntry, "hl7:templateId");
        assertTrue(aa.size() == 2);
    }

    @Test
    public void testExtractListBrodhers3() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting5.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
        List<RuleDefinition> aa = extractListBrodhers(first, "hl7:section");
        assertTrue(aa.size() == 1);
    }

    @Test
    public void testExtractListBrodhers4() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting5.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
        List<RuleDefinition> aa = extractListBrodhers(first, null);
        assertTrue(aa == null || aa.size() == 0);
        aa = extractListBrodhers(first, "aaaaaaaaaaa");
        assertTrue(aa == null || aa.size() == 0);
        aa = extractListBrodhers(null, "hl7:section");
        assertTrue(aa == null || aa.size() == 0);
        aa = extractListBrodhers(null, null);
        assertTrue(aa == null || aa.size() == 0);
    }

    @Test
    public void testExtractListBrodhers5() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_contain_bbr.xml");
        RuleDefinition firstRD = TemplateDefinitionUtil.getFirstElement(RulesUtil.getTemplates(dec.getRules()).get(0));
        RuleDefinition comp = RuleDefinitionUtil.getElementByName(firstRD, "cda:component");
        RuleDefinition strc = RuleDefinitionUtil.getElementByName(comp, "cda:structuredBody");
        RuleDefinition comp2 = RuleDefinitionUtil.getElementByName(strc, "cda:component");
        ContainDefinition containDefinition = RuleDefinitionUtil.getContains(comp2).get(0);
        processLocal(containDefinition);

        RuleDefinition templateId = ContainDefinitionUtil.getTemplateIdElement(containDefinition);
        templateId.setParentObject(containDefinition);
        List<RuleDefinition> aa = extractListBrodhers(templateId, "cda:component");
        assertEquals(0, aa.size());
        aa = extractListBrodhers(comp2, "cda:component");
        assertEquals(2, aa.size());
    }

    @Test
    public void testGenerateDistinguisherForRuleDefinition() throws Exception {
        DParent dist = generateDistinguisherForRuleDefinition(firstRD);
        assertTrue(DPathExtractor.createPathFromDParent(dist).equals("/hl7:templateId/@root='1.3.6.1.4.1.19376.1.5.3.1.4.5.2'"));
        dist = generateDistinguisherForRuleDefinition(obs);
        assertTrue(DPathExtractor.createPathFromDParent(dist).equals("/hl7:code/@code='48766-0'"));
        dist = generateDistinguisherForRuleDefinition(targetSiteCode);
        assertTrue(DPathExtractor.createPathFromDParent(dist).equals(""));
    }

    @Test
    public void testExtractListDistinguishersForBrodhers() {
        Decor dec = DecorMarshaller.loadDecor("src/test/resources/decor_disting5.xml");
        TemplateDefinition td = RulesUtil.getTemplates(dec.getRules()).get(0);
        RuleDefinition first = TemplateDefinitionUtil.getFirstElement(td);
        RuleDefinition firstEntry = RuleDefinitionUtil.getElementsByName(first, "hl7:templateId").get(0);
        Map<RuleDefinition, Pair<String, String>> aa = RuleDefinitionDistinguisherUtil.extractListDistinguishersForBrodhers(firstEntry);
        assertTrue(aa.size() == 2);
        assertTrue(aa.containsKey(firstEntry));
        assertTrue(aa.keySet().containsAll(RuleDefinitionDistinguisherUtil.extractListBrodhers(firstEntry, "hl7:templateId")));
        System.out.println(aa);
        assertTrue(aa.containsValue(null));
        assertTrue(aa.containsValue(new Pair<String, String>("/@root", "1.3.6.1.4.1.19376.1.5.3.1.3.16.1")));
    }

    private void processLocal(ContainDefinition currentContainDefinition) {
        TemplateDefinition td = ContainDefinitionUtil.referencedTemplateDefinitionFromParentBBR(currentContainDefinition);
        if (currentContainDefinition.getParentObject() instanceof RuleDefinition) {
            RuleDefinition replacementRD = new RuleDefinition();
            replacementRD.setName("cda:component");
            replacementRD.setParentObject(currentContainDefinition);
            RuleDefinition templateId = new RuleDefinition();
            templateId.setName("hl7:templateId");
            templateId.getAttribute().add(new Attribute());
            templateId.getAttribute().get(0).setName("root");
            templateId.getAttribute().get(0).setValue(TemplateDefinitionUtil.extractTemplateId(td));
            replacementRD.getLetOrAssertOrReport().add(templateId);
            currentContainDefinition.getLetOrAssertOrReport().add(replacementRD);
        }
    }
}
