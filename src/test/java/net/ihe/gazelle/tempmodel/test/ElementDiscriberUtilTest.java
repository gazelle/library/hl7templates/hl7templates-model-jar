package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.distinguisher.CDAElementsDistinguisher;
import net.ihe.gazelle.tempmodel.distinguisher.ElementDiscriber;
import net.ihe.gazelle.tempmodel.distinguisher.utils.DistinguisherUtil;
import net.ihe.gazelle.tempmodel.distinguisher.utils.ElementDiscriberUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class ElementDiscriberUtilTest {
	
private static CDAElementsDistinguisher cdaDistinguisers = null;
	
	@Before
	public void before(){
		try {
			cdaDistinguisers = DistinguisherUtil.loadCDAElementsDistinguisher(
					new FileInputStream("src/main/resources/distinguishers.xml"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testExtractElementDiscriberById() {
		ElementDiscriber ed = ElementDiscriberUtil.extractElementDiscriberById("POCDMT000040Consumable", cdaDistinguisers);
		assertNotNull(ed);
		assertTrue(ed.getIdentifier().equals("POCDMT000040Consumable"));
		assertNull( ElementDiscriberUtil.extractElementDiscriberById(null, null));
	}

}
