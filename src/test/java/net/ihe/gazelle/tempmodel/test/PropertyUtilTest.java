package net.ihe.gazelle.tempmodel.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

import net.ihe.gazelle.tempmodel.org.decor.art.model.Property;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.PropertyUtil;

/**
 * 
 * @author Abderrazek Boufahja
 *
 */
public class PropertyUtilTest {

	@Test
	public void testExtractDigitNumber() {
		Integer extr = PropertyUtil.extractDigitNumber("2!");
		assertTrue(extr == 2);
		extr = PropertyUtil.extractDigitNumber("12!");
		assertTrue(extr == 12);
		extr = PropertyUtil.extractDigitNumber("3");
		assertTrue(extr == 3);
		Assert.assertNull(PropertyUtil.extractDigitNumber(null));
	}

	@Test
	public void testIsUnitAttributePresent() {
		Property prop = new Property();
		prop.setUnit("cm");
		assertTrue(PropertyUtil.isUnitAttributePresent(prop));
		prop.setUnit(null);
		assertFalse(PropertyUtil.isUnitAttributePresent(prop));
		Assert.assertFalse(PropertyUtil.isUnitAttributePresent(null));
		Assert.assertFalse(PropertyUtil.isUnitAttributePresent(new Property()));
	}

	@Test
	public void testIsMinIncludeAttributePresent() {
		Property prop = new Property();
		prop.setMinInclude("22.1");
		assertTrue(PropertyUtil.isMinIncludeAttributePresent(prop));
		prop.setMinInclude(null);
		assertFalse(PropertyUtil.isMinIncludeAttributePresent(prop));
		Assert.assertFalse(PropertyUtil.isMinIncludeAttributePresent(null));
		Assert.assertFalse(PropertyUtil.isUnitAttributePresent(new Property()));
	}

	@Test
	public void testIsMaxIncludeAttributePresent() {
		Property prop = new Property();
		prop.setMaxInclude("22.1");
		assertTrue(PropertyUtil.isMaxIncludeAttributePresent(prop));
		prop.setMaxInclude(null);
		assertFalse(PropertyUtil.isMaxIncludeAttributePresent(prop));
		Assert.assertFalse(PropertyUtil.isMaxIncludeAttributePresent(null));
		Assert.assertFalse(PropertyUtil.isMaxIncludeAttributePresent(new Property()));
	}

	@Test
	public void testIsFractionDigitsAttributePresent() {
		Property prop = new Property();
		prop.setFractionDigits("22");
		assertTrue(PropertyUtil.isFractionDigitsAttributePresent(prop));
		prop.setFractionDigits(null);
		assertFalse(PropertyUtil.isFractionDigitsAttributePresent(prop));
		prop.setFractionDigits("22!");
		assertTrue(PropertyUtil.isFractionDigitsAttributePresent(prop));
		prop.setFractionDigits("0!");
		assertTrue(PropertyUtil.isFractionDigitsAttributePresent(prop));
		prop.setFractionDigits("0");
		assertFalse(PropertyUtil.isFractionDigitsAttributePresent(prop));
		Assert.assertFalse(PropertyUtil.isFractionDigitsAttributePresent(null));
		Assert.assertFalse(PropertyUtil.isFractionDigitsAttributePresent(new Property()));
	}

	@Test
	public void testIsCurrencyAttributePresent() {
		Property prop = new Property();
		prop.setCurrency("eu");
		assertTrue(PropertyUtil.isCurrencyAttributePresent(prop));
		prop.setCurrency(null);
		assertFalse(PropertyUtil.isCurrencyAttributePresent(prop));
		Assert.assertFalse(PropertyUtil.isCurrencyAttributePresent(null));
		Assert.assertFalse(PropertyUtil.isCurrencyAttributePresent(new Property()));
	}

	@Test
	public void testIsMinLengthAttributePresent() {
		Property prop = new Property();
		prop.setMinLength(1);
		assertTrue(PropertyUtil.isMinLengthAttributePresent(prop));
		prop.setMinLength(null);
		assertFalse(PropertyUtil.isMinLengthAttributePresent(prop));
		Assert.assertFalse(PropertyUtil.isMinLengthAttributePresent(null));
		Assert.assertFalse(PropertyUtil.isMinLengthAttributePresent(new Property()));
	}

	@Test
	public void testIsMaxLengthAttributePresent() {
		Property prop = new Property();
		prop.setMaxLength(1);
		assertTrue(PropertyUtil.isMaxLengthAttributePresent(prop));
		prop.setMaxLength(null);
		assertFalse(PropertyUtil.isMaxLengthAttributePresent(prop));
		Assert.assertFalse(PropertyUtil.isMaxLengthAttributePresent(null));
		Assert.assertFalse(PropertyUtil.isMaxLengthAttributePresent(new Property()));
	}

	@Test
	public void testIsValueAttributePresent() {
		Property prop = new Property();
		prop.setValue("e");
		assertTrue(PropertyUtil.isValueAttributePresent(prop));
		prop.setValue(null);
		assertFalse(PropertyUtil.isValueAttributePresent(prop));
		Assert.assertFalse(PropertyUtil.isValueAttributePresent(null));
		Assert.assertFalse(PropertyUtil.isValueAttributePresent(new Property()));
	}

}
